//
//  UIImage+Utils.h
//  Office
//
//  Created by Abdullah Waseer on 4/3/13.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

//extract a portion of an UIImage instance
-(UIImage *) cutout: (CGRect) coords;
//create a stretchable rendition of an UIImage instance, protecting edges as specified in cornerCaps
-(UIImage *) stretchImageWithCapInsets: (UIEdgeInsets) cornerCaps toSize: (CGSize) size;

@end

