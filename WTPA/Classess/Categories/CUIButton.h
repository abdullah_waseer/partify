//
//  CUIButton.h
//  HEO
//
//  Created by Smonte on 4/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CUIButton : UIButton {

	NSIndexPath *indexPath;
}

@property (nonatomic, retain) NSIndexPath *indexPath;

@end
