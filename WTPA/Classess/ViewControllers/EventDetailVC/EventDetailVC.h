//
//  EventDetailVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 01/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailVC : UIViewController

@property(strong,nonatomic) NSString *venueId;
@property(strong,nonatomic) NSString *eventSource;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andEventDetail:(NSMutableDictionary *) eventDet;
@end
