//
//  EventDetailVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 01/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "EventDetailVC.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "FaceBookManager.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "AJNotificationView.h"
#import "AppDelegate.h"
#import "VenueDetailVC.h"
#import "FeedbackVC.h"


@interface EventDetailVC ()<NetworkManagerDelegate,EKEventEditViewDelegate>

@property (nonatomic,retain) IBOutlet UIButton *faceboookBtn;
@property (nonatomic,retain) IBOutlet UIButton *TwitterBtn;
@property (nonatomic,retain) IBOutlet UIButton *iAmInBtn;
@property (nonatomic,retain) IBOutlet UIButton *calenderButton;
@property (nonatomic,retain) IBOutlet UIButton *contactsbutton;

@property (nonatomic,retain) IBOutlet UILabel *eventName;
@property (nonatomic,retain) IBOutlet UILabel *eventDate;
@property (nonatomic,retain) IBOutlet UILabel *eventVenue;
@property (nonatomic,retain) IBOutlet UILabel *eventStreet;
@property (retain, nonatomic) IBOutlet UIButton *feedbackBtn;
@property (nonatomic,retain) IBOutlet UILabel *eventCountry;
@property (nonatomic,retain) IBOutlet UILabel *eventCity;

@property (nonatomic,retain) IBOutlet UILabel *description;
@property (nonatomic,retain) IBOutlet UILabel *venueAddressLabel;
@property (nonatomic,retain) IBOutlet UILabel *shareThisEvent;

@property (nonatomic,retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic,retain) IBOutlet UITextView *eventdescription;

@property (nonatomic,retain) NSMutableDictionary *eventDeatail;

@property (nonatomic,retain) NetworkManager * networkManager;

@property (nonatomic, retain) EKEventStore *eventStore;
@property (nonatomic, retain) EKCalendar *defaultCalendar;
@property (nonatomic, assign) ABAddressBookRef addressBook;

@property BOOL type;

@end

@implementation EventDetailVC

-(void) dealloc
{
    [_defaultCalendar release],_defaultCalendar=nil;
    [_eventStore release],_eventStore=nil;
    [_faceboookBtn release],_faceboookBtn=nil;
    [_TwitterBtn release],_TwitterBtn=nil;
    [_iAmInBtn release],_iAmInBtn=nil;
    [_calenderButton release],_iAmInBtn=nil;
    [_contactsbutton release],_iAmInBtn=nil;
    [_eventName release],_eventName=nil;
    [_eventDate release],_eventDate=nil;
    [_eventdescription release],_eventdescription=nil;
    [_description release],_description=nil;
    [_shareThisEvent release],_shareThisEvent=nil;
    
    [_eventDeatail release],_eventDeatail=nil;
    
    [_eventCity release],_eventCity=nil;
    [_eventVenue release],_eventVenue=nil;
    
    [_eventCountry release],_eventCountry=nil;
    [_eventStreet release],_eventStreet=nil;
    [_scrollView release],_scrollView=nil;
    [_networkManager release],_networkManager=nil;
    
    [_feedbackBtn release];
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andEventDetail:(NSMutableDictionary *) eventDet
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.eventDeatail=[[NSMutableDictionary alloc] initWithDictionary:eventDet];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    self.eventStore = [[EKEventStore alloc] init];
    
//    self.iAmInBtn.layer.borderColor=[UIColor whiteColor].CGColor;
//    self.iAmInBtn.layer.borderWidth=1;
//    self.iAmInBtn.layer.cornerRadius=5;

    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    self.scrollView.contentSize=CGSizeMake(320, IS_IPHONE_5?720:750);
    
    if([self.eventDeatail objectForKey:@"type"])
       [self.iAmInBtn setTitle:@"Pull Out" forState:UIControlStateNormal];
    
    [self hideUIViews:YES];
    [self setUpNavigationBar];
    
    [self performSelector:@selector(getEventDetail) withObject:nil afterDelay:0.1];
    
    // Do any additional setup after loading the view from its nib.
    
    [self addTapGestureToVenueLbl];

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - Custom Methods Start

-(void) hideUIViews:(BOOL) hide
{
    self.description.hidden=hide;
    self.venueAddressLabel.hidden=hide;
    self.shareThisEvent.hidden=hide;
    self.faceboookBtn.hidden=hide;
    self.TwitterBtn.hidden=hide;
    self.iAmInBtn.hidden=hide;
    self.calenderButton.hidden=hide;
    self.contactsbutton.hidden=hide;
    self.eventdescription.hidden=hide;
}

-(void) getEventDetail
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/events/getevent/%@/by/%@",[SharedAppManager sharedInstance].baseURL,[self.eventDeatail   objectForKey:@"id"],[self.eventDeatail objectForKey:@"source"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    // NSDictionary * params = [[NSDictionary alloc]initWithObjectsAndKeys:[self.profileData objectForKey:@"id"],@"follower_id", nil];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",[data objectForKey:@"id"],@"REQUESTER_ID",nil] forKey:@"headers"];
    [parameters setObject:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"eventDetail" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.networkManager.delegate = self;
    [self.networkManager sendRequestforAction:parameters];
}

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
//    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addTapGestureToVenueLbl
{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(venueLblSingleTap:)];
    self.eventVenue.userInteractionEnabled = YES;
    self.eventVenue.multipleTouchEnabled = YES;
    [self.eventVenue addGestureRecognizer:singleTap];
    
}

-(void)venueLblSingleTap:(UITapGestureRecognizer*)tgs
{
//    NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:@"Favorited by", @"title", [gearsDict objectForKey:@"users"], @"users", nil];
//    
//    FavoriteVC *favoriteVC=[[FavoriteVC alloc] initWithNibName:@"FavoriteVC" bundle:nil andWithUsersDict:dictionary];
//    [self.navigationController pushViewController:favoriteVC animated:YES];
    
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:self.venueId];
    venueDetail.venueDictionaryFromLastFm = [self.eventDeatail objectForKey:@"venue"];
    venueDetail.eventSource = self.eventSource;
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}


#pragma mark - IBActions Methods Statrt

-(IBAction)iAmInClicked:(id)sender
{
    UIButton * button = (UIButton *) sender;
    if([button.titleLabel.text isEqualToString:@"I'M IN!"])
    {
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/followevent/%@/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"],[_eventDeatail objectForKey:@"id"]];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
        
        [jsonWriter release];
        
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"PUT" forKey:@"callType"];
        [parameters setObject:@"followEvent" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
    else
    {
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/unfollowevent/%@/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"],[_eventDeatail objectForKey:@"id"]];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
        
        [jsonWriter release];
        
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"DELETE" forKey:@"callType"];
        [parameters setObject:@"unfollowEvent" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
        
    }
}

- (IBAction)bringFeedBackVC:(id)sender
{
    FeedbackVC *feedbackVC = [[FeedbackVC alloc]initWithNibName:@"FeedbackVC" bundle:nil];
    feedbackVC.feedbackIssueId = [self.eventDeatail objectForKey:@"id"];
    feedbackVC.issueAbout = @"event";
    [self.navigationController pushViewController:feedbackVC animated:YES];
}

-(IBAction)facebookShareClicked:(id)sender
{
    [[FaceBookManager sharedManager] uploadStausWithText:[NSString stringWithFormat:@"%@ is going to %@",[[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0] valueForKey:@"username"],[self.eventDeatail objectForKey:@"title"]]];
}

-(IBAction)twitterShareClicked:(id)sender
{
    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    
    //[tweetSheet addImage:self.photoToUpload.image];
    //[tweetSheet addURL:[NSURL URLWithString:displayURLstring]];
    
    [self presentModalViewController:tweetSheet animated:YES];
    TWTweetComposeViewControllerCompletionHandler
    completionHandler =
    ^(TWTweetComposeViewControllerResult result) {
        switch (result)
        {
            case TWTweetComposeViewControllerResultCancelled:
                NSLog(@"Twitter Result: canceled");
                
                break;
            case TWTweetComposeViewControllerResultDone:
                [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                                type:AJNotificationTypeGreen
                                               title:@"Posted on Twitter Successfully."
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                break;
            default:
                NSLog(@"Twitter Result: default");
                [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                                type:AJNotificationTypeRed
                                               title:@"Failed to post on Twitter."
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                break;
        }
        [self dismissModalViewControllerAnimated:YES];
    };
    [tweetSheet setCompletionHandler:completionHandler];
}

- (IBAction)addCalenderEvent:(id)sender
{
	// Create an instance of EKEventEditViewController
	[self requestCalendarAccess];
}

-(IBAction)addToContacts:(id)sender
{
    [self checkAddressBookAccess];
}


#pragma mark - Delegates Statrt


#pragma mark - NetworkMangerDelegate Delegate Methods


- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"eventDetail"])
        {
            [self hideUIViews:NO];
            if(![[[object objectForKey:@"data"] objectForKey:@"source"]isEqualToString:@"DB"])
            {
                self.iAmInBtn.hidden = YES;
                self.feedbackBtn.hidden = YES;
            }
            if(_eventDeatail)
                [_eventDeatail release],_eventDeatail = nil;
            self.eventDeatail = [[NSMutableDictionary alloc]initWithDictionary:[object valueForKey:@"data"]];
            if([[[self.eventDeatail objectForKey:@"following"] stringValue]isEqualToString:@"1"])
                [self.iAmInBtn setTitle:@"Pull Out" forState:UIControlStateNormal];
            self.eventDate.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"startDate"]];
            self.eventVenue.text=[NSString stringWithFormat:@"%@",[_eventDeatail[@"venue"][@"name"] capitalizedString]];
            self.eventName.text=[NSString stringWithFormat:@"%@",[_eventDeatail[@"title"] capitalizedString]];
            self.eventCountry.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"venue"][@"location"][@"country"]];
            self.eventCity.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"venue"][@"location"][@"city"]];
            if([_eventDeatail[@"venue"][@"location"][@"street"] isEqualToString:@""])
            {
                self.eventStreet.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"venue"][@"location"][@"postalcode"]];
            }
            else if([_eventDeatail[@"venue"][@"location"][@"postalcode"] isEqualToString:@""])
            {
                self.eventStreet.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"venue"][@"location"][@"street"]];
            }
            else
            {
                self.eventStreet.text=[NSString stringWithFormat:@"%@, %@",_eventDeatail[@"venue"][@"location"][@"street"],_eventDeatail[@"venue"][@"location"][@"postalcode"]];
            }
            
            self.eventdescription.text=[NSString stringWithFormat:@"%@",_eventDeatail[@"description"]];
        }
        else if([networkManager.identifier isEqualToString:@"followEvent"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshEvents"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object objectForKey:@"data"]];
        }
        else if([networkManager.identifier isEqualToString:@"unfollowEvent"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshEvents"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object objectForKey:@"data"]];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Access Calendar

// Check the authorization status of our application for Calendar
-(void)checkEventStoreAccessForCalendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status)
    {
            // Update our UI if the user has granted access to their Calendar
        case EKAuthorizationStatusAuthorized: [self accessGrantedForCalendar];
            break;
            // Prompt the user for access to Calendar if there is no definitive answer
        case EKAuthorizationStatusNotDetermined: [self requestCalendarAccess];
            break;
            // Display a message if the user has denied or restricted access to Calendar
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning" message:@"Permission was not granted for Calendar"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}


// Prompt the user for access to their Calendar
-(void)requestCalendarAccess
{
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             EventDetailVC *weakSelf = self;
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [weakSelf accessGrantedForCalendar];
             });
         }
     }];
}

// This method is called when the user has granted permission to Calendar
-(void)accessGrantedForCalendar
{// Let's get the default calendar associated with our event store
    self.defaultCalendar = self.eventStore.defaultCalendarForNewEvents;
    
    EKEventEditViewController *addController = [[EKEventEditViewController alloc] init];

    EKEvent *event  = [EKEvent eventWithEventStore:self.eventStore];
    event.title     =_eventDeatail[@"venue"][@"name"];
    event.location  =[NSString stringWithFormat:@"%@,%@,%@,%@",_eventDeatail[@"venue"][@"location"][@"street"],_eventDeatail[@"venue"][@"location"][@"city"],_eventDeatail[@"venue"][@"location"][@"country"],_eventDeatail[@"venue"][@"location"][@"postalcode"]];
    
    NSString *dateString           = _eventDeatail[@"startDate"];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat       = @"EEE, dd MM yyyy HH:mm:ss";
    
    NSDate *date                   = [dateFormatter dateFromString:dateString];

    event.startDate = date;
    event.endDate   = [NSDate date];
    event.notes     = @"";
    event.URL   = [NSURL URLWithString:_eventDeatail[@"venue"][@"url"]];
    
    
	// Set addController's event store to the current event store
    addController.event=event;
	addController.eventStore = self.eventStore;
    addController.editViewDelegate = self;
    [self presentViewController:addController animated:YES completion:nil];
}
#pragma mark -
#pragma mark EKEventEditViewDelegate

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller
		  didCompleteWithAction:(EKEventEditViewAction)action
{
	// Dismiss the modal view controller
    [self dismissViewControllerAnimated:YES completion:^{}];
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller
{
	return self.defaultCalendar;
}

#pragma mark -
#pragma mark Address Book Access
// Check the authorization status of our application for Address Book
-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess];
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
        case  kABAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning"
                                                            message:@"Permission was not granted for Contacts."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}

// Prompt the user for access to their Address Book data
-(void)requestAddressBookAccess
{
    EventDetailVC *weakSelf = self;
    
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook];
                                                         
                                                     });
                                                 }
                                             });
}

// This method is called when the user has granted access to their address book data.
-(void)accessGrantedForAddressBook
{
    ABAddressBookRef addressBookLocal = ABAddressBookCreateWithOptions(NULL,NULL);
    
    ABRecordRef record = ABPersonCreate();
    
    NSString *fname = [NSString stringWithFormat:@"%@",_eventDeatail[@"title"]];
    NSString *lname = [NSString stringWithFormat:@"%@",_eventDeatail[@"venue"][@"name"]];
    
    // add the first name
    ABRecordSetValue(record, kABPersonFirstNameProperty, fname, NULL);
    
    // add the last name
    ABRecordSetValue(record, kABPersonLastNameProperty, lname, NULL);
    
    // add the home email
    
//    ABMutableMultiValueRef multi = ABMultiValueCreateMutable(kABStringPropertyType);
    //
    //    NSString *email = [NSString stringWithFormat:@"ishaq@hotmail.com"];
    //    ABMultiValueAddValueAndLabel(multi, email, kABHomeLabel, NULL);
//    ABRecordSetValue(record, kABPersonEmailProperty, multi, NULL);
    
    // add address
    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    addressDictionary[(NSString *) kABPersonAddressStreetKey] =_eventDeatail[@"venue"][@"location"][@"street"];
    addressDictionary[(NSString *)kABPersonAddressCityKey] = _eventDeatail[@"venue"][@"location"][@"city"];
   
    addressDictionary[(NSString *)kABPersonAddressStateKey] = @"";
   
    addressDictionary[(NSString *)kABPersonAddressZIPKey] = _eventDeatail[@"venue"][@"location"][@"postalcode"];
   
    addressDictionary[(NSString *)kABPersonAddressCountryKey] = _eventDeatail[@"venue"][@"location"][@"country"];
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABHomeLabel, NULL);
    ABRecordSetValue(record, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);
    
    //add phone numbers
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);

    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef)_eventDeatail[@"venue"][@"phonenumber"], kABPersonPhoneMainLabel, NULL);
     const CFStringRef customLabel = CFSTR( "Phone1" );
      ABMultiValueAddValueAndLabel(phoneNumberMultiValue, @"1-555-555-555", customLabel, NULL);
    ABRecordSetValue(record, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
   
   
//     ABRecordSetValue(record, customLabel, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);
    
    ABMutableMultiValueRef multiURL = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiURL, @"", kABPersonHomePageLabel, NULL);
   
    ABRecordSetValue(record, kABPersonURLProperty, multiURL,nil);
    CFRelease(multiURL);
    
    // add the record
    ABAddressBookAddRecord(addressBookLocal, record, NULL);
    // save the address book
    ABAddressBookSave(addressBookLocal, NULL);
    
    // release
    CFRelease(addressBookLocal);
    
    [[SharedAppManager sharedInstance] initWithTitle:@"PartiFy" message:@"Contact added successfully"];
}


@end
