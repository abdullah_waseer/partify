//
//  SwitchLoginVC.m
//  WTPA
//
//  Created by Admin on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SwitchLoginVC.h"

@interface SwitchLoginVC ()<NetworkManagerDelegate>

@property (nonatomic,retain) IBOutlet UITextField *txtUserName;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;

@property (nonatomic,retain) NetworkManager * networkManager;


@end

@implementation SwitchLoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    //[self.navigationItem setHidesBackButton:YES animated:NO];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) dealloc
{
    [_networkManager release],_networkManager=nil;
    [_txtPassword release],_txtPassword=nil;
    
    [_txtUserName release],_txtUserName=nil;
    
    [super dealloc];
}

#pragma mark - Custom Methods

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - IBActions and Methods

-(IBAction)logInClicked:(id)sender
{
    if([self.txtUserName.text length]>0 || [self.txtPassword.text length]>0)
    {
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/authenticate",[SharedAppManager sharedInstance].baseURL];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
        [parameters setObject:jsonString forKey:@"param"];
        
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:self.txtUserName.text forKey:@"USERNAME"];
        
        [dictionary setObject:self.txtPassword.text forKey:@"PASSWORD"];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"loginuser" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please enter a valid username and password"];
    }
}
#pragma mark - Delegate Started
#pragma mark - NetworkMangerDelegate Delegate Methods

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
        
        [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
        [[NSUserDefaults standardUserDefaults] setObject:self.txtPassword.text  forKey:PASSWORD];
        [self.delegate switchLoginVC:self];
        //        [[NSUserDefaults standardUserDefaults] synchronize];
        //self.hidesBottomBarWhenPushed = NO;
        //[self.navigationController popViewControllerAnimated:YES ];
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
//        [[self.tabBarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:YES];
//        [[self.tabBarController.viewControllers objectAtIndex:1]popToRootViewControllerAnimated:YES];
//        [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:YES];
//        [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:YES];
//        [[self.tabBarController.viewControllers objectAtIndex:4]popToRootViewControllerAnimated:YES];
//        self.tabBarController.selectedIndex = 0;
//        
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
//        [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
//        [[NSUserDefaults standardUserDefaults] setObject:self.txtPassword.text  forKey:PASSWORD];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self dismissViewControllerAnimated:YES completion:^{}];
            
//        [self performSelector:@selector(dismiss) withObject:nil afterDelay:0.01];
        /* NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
         NSString *plistPath = [directoryPath objectAtIndex:0];
         plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
         NSMutableArray * userArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
         NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA]);
         NSMutableDictionary * dataToWrite = [[NSMutableDictionary alloc]initWithDictionary:[[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0]];
         [dataToWrite setObject:[[NSUserDefaults standardUserDefaults] valueForKey:PASSWORD] forKey:@"password"];
         if(!userArray)
         userArray = [[NSMutableArray alloc]init];
         [userArray addObject:dataToWrite];
         [userArray exchangeObjectAtIndex:0 withObjectAtIndex:userArray.count-1];
         [userArray writeToFile:plistPath atomically:YES];*/
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
-(void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    
    }];
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - UITextFiled Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text=@"";
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextResponder=textField.tag+1;
    
    UITextField *txtfield=(UITextField *)[self.view viewWithTag:nextResponder];
    if(txtfield)
    {
        [txtfield becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(IBAction)hidekeyBorad:(id)sender
{
    [_txtPassword resignFirstResponder];
    [_txtUserName resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
