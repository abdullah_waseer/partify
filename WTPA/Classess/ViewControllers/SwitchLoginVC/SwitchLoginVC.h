//
//  SwitchLoginVC.h
//  WTPA
//
//  Created by Admin on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SwitchLoginVC;

@protocol SwitchLoginVCDelegate <NSObject>

- (void) switchLoginVC:(SwitchLoginVC*) switchLoginVC;

@end
@interface SwitchLoginVC : UIViewController
@property (assign, nonatomic) id<SwitchLoginVCDelegate> delegate;
@end
