//
//  SettingsVC.m
//  WTPA
//
//  Created by Admin on 22/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SettingsVC.h"
#import "UserListVC.h"
#import "SwitchLoginVC.h"
#import "UserProfileVC.h"
#import "HomeVC.h"
#import "EditProfileVC.h"
#import "TersmAndPrivacyVC.h"

@interface SettingsVC ()<SwitchLoginVCDelegate>

@property (nonatomic , retain) NSString * username;
@property (nonatomic , retain) IBOutlet UISwitch * facbookToggle;
@property (nonatomic , retain) IBOutlet UISwitch * twitterToggle;
@property (nonatomic , retain) IBOutlet UISwitch * instagramToggle;
@property (nonatomic , retain) IBOutlet UISwitch * pushToggle;

@property (nonatomic , retain) IBOutlet UIButton * logOututon;
@property (nonatomic , retain) IBOutlet UIButton * editprofileButton;
@property (nonatomic , retain) IBOutlet UIButton * changeAddButton;


//@property BOOL facebookToggle;
//@property BOOL twitterToggle;
//@property BOOL instagramToggle;

@end

@implementation SettingsVC



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)username
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.username = [NSString stringWithFormat:@"%@",username];
    }
    return self;
}

- (IBAction)changeAddUserBtnPressed:(id)sender
{
//    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *plistPath = [directoryPath objectAtIndex:0];
//    plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
//    NSMutableArray * prodArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
//    for(int i=0;i<prodArray.count;i++)
//        NSLog(@"%@",[prodArray objectAtIndex:i]);
//    UserListVC * userListVC = [[UserListVC alloc]init];
//    [self.navigationController pushViewController:userListVC animated:YES];
//    [userListVC release];
    
    SwitchLoginVC * switchLoginVC = [[SwitchLoginVC alloc]init];
    switchLoginVC.hidesBottomBarWhenPushed = YES;
    switchLoginVC.delegate = self;
    
    //[self.navigationController presentViewController:switchLoginVC animated:YES completion:^{}];
    [self.navigationController pushViewController:switchLoginVC animated:YES];
    [switchLoginVC release];
    
}

- (IBAction)logoutBtnPressed:(id)sender
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [SharedAppManager sharedInstance].addUser = YES;
    //[HomeVC changeConfigBool];
    UserProfileVC *userProfileVC = (UserProfileVC *)[[[self.tabBarController.viewControllers objectAtIndex:3]viewControllers] objectAtIndex:0];
    [userProfileVC resetAllProperties];
    self.tabBarController.selectedIndex = 0;
    [[self.tabBarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:1]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:4]popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    
    [self setUpNavigationBar];
    self.scroll.contentSize = CGSizeMake(320, 650);
    [self.facbookToggle setOn:[SharedAppManager sharedInstance].facebookToggle];
    [self.twitterToggle setOn:[SharedAppManager sharedInstance].twitterToggle];
    [self.instagramToggle setOn:[SharedAppManager sharedInstance].instagramToggle];
    [self.pushToggle setOn:[SharedAppManager sharedInstance].pushToggle];
    
    [self setupTermsAndPolicyBtnsAboveLogoutBtn];
    
    
//    self.logOututon.layer.borderColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1].CGColor;
//    self.logOututon.layer.borderWidth=1;
//    self.logOututon.layer.cornerRadius=5;
//    
//    self.editprofileButton.layer.borderColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1].CGColor;
//    self.editprofileButton.layer.borderWidth=1;
//     self.editprofileButton.layer.cornerRadius=5;
//    
//    self.changeAddButton.layer.borderColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1].CGColor;
//    self.changeAddButton.layer.borderWidth=1;
//     self.changeAddButton.layer.cornerRadius=5;
    // Do any additional setup after loading the view from its nib.
}

-(void)setupTermsAndPolicyBtnsAboveLogoutBtn
{
    UIButton *termsBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    termsBtn.frame = CGRectMake(CGRectGetMinX(self.logOututon.frame), CGRectGetMinY(self.logOututon.frame)-14, CGRectGetWidth(self.logOututon.frame)+80, CGRectGetHeight(self.logOututon.frame));
    [termsBtn setTitle:@"Terms and Conditions" forState:UIControlStateNormal];
    [termsBtn setTitleColor:[UIColor colorWithRed:253.0/255.0 green:0.0/255.0 blue:43.0/255.0 alpha:1] forState:UIControlStateNormal];
    termsBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    termsBtn.titleLabel.font = self.logOututon.titleLabel.font;
    [termsBtn addTarget:self action:@selector(showTermAndConditoins:) forControlEvents:UIControlEventTouchUpInside];
    [self.scroll addSubview:termsBtn];
    
    UIButton *policyBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    policyBtn.frame = CGRectMake(CGRectGetMinX(termsBtn.frame), CGRectGetMaxY(termsBtn.frame)+5, CGRectGetWidth(termsBtn.frame), CGRectGetHeight(termsBtn.frame));
    [policyBtn setTitle:@"Privacy Policy" forState:UIControlStateNormal];
    [policyBtn setTitleColor:[UIColor colorWithRed:253.0/255.0 green:0.0/255.0 blue:43.0/255.0 alpha:1] forState:UIControlStateNormal];
    policyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    policyBtn.titleLabel.font = self.logOututon.titleLabel.font;

    [policyBtn addTarget:self action:@selector(showPrivacyPolicy:) forControlEvents:UIControlEventTouchUpInside];
    [self.scroll addSubview:policyBtn];
    
    self.logOututon.frame = CGRectMake(CGRectGetMinX(policyBtn.frame), CGRectGetMaxY(policyBtn.frame)+5, CGRectGetWidth(policyBtn.frame), CGRectGetHeight(policyBtn.frame));
}

- (void)showTermAndConditoins:(id)sender
{
    TersmAndPrivacyVC *termsVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    termsVc.accessibilityHint = @"Terms";
    termsVc.cameFromPage = @"settings";
    [self.navigationController pushViewController:termsVc animated:YES];
}

- (void)showPrivacyPolicy:(id)sender
{
    TersmAndPrivacyVC *privacyVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    privacyVc.accessibilityHint = @"Privacy";
    privacyVc.cameFromPage = @"settings";
    [self.navigationController pushViewController:privacyVc animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 44)];
    titleView.font = [UIFont fontWithName:@"Thonburi" size:25.0];
    titleView.textColor = [UIColor whiteColor];
    titleView.backgroundColor=[UIColor clearColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = [self.username uppercaseString];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)facebookToggleBtnPressed:(UISwitch *)sender
{
    [SharedAppManager sharedInstance].facebookToggle = ![SharedAppManager sharedInstance].facebookToggle;
    [self.facbookToggle setOn:[SharedAppManager sharedInstance].facebookToggle];
    
}
- (IBAction)twitterToggleBtnPressed:(UISwitch *)sender
{
    [SharedAppManager sharedInstance].twitterToggle = ![SharedAppManager sharedInstance].twitterToggle;
    [self.twitterToggle setOn:[SharedAppManager sharedInstance].twitterToggle];
}
- (IBAction)instagramToggleBtnPressed:(UISwitch *)sender
{
    [SharedAppManager sharedInstance].instagramToggle = ![SharedAppManager sharedInstance].instagramToggle;
    [self.instagramToggle setOn:[SharedAppManager sharedInstance].instagramToggle];
}
- (IBAction)profileToggleBtnPressed:(UIButton *)sender
{
    sender.selected=!sender.selected;
}

- (IBAction)pushToggleBtnPressed:(UISwitch *)sender
{
    [SharedAppManager sharedInstance].pushToggle = ![SharedAppManager sharedInstance].pushToggle;
    [self.pushToggle setOn:[SharedAppManager sharedInstance].pushToggle];
}

- (IBAction)editProfileBtnPressed:(id)sender
{
    EditProfileVC * editProfileVC = [[EditProfileVC alloc]init];
    [self.navigationController pushViewController:editProfileVC animated:YES];
    [editProfileVC release];
}

#pragma mark - SwitvhLoginVC Delegate
-(void)switchLoginVC:(SwitchLoginVC *)switchLoginVC
{
    UserProfileVC *userProfileVC = (UserProfileVC *)[[[self.tabBarController.viewControllers objectAtIndex:3]viewControllers] objectAtIndex:0];
    [userProfileVC resetAllProperties];
    self.tabBarController.selectedIndex = 0;
    [[self.tabBarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:1]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:YES];
    [[self.tabBarController.viewControllers objectAtIndex:4]popToRootViewControllerAnimated:YES];
    
    
//    [self performSelector:@selector(reset) withObject:nil afterDelay:1.5];
}
-(void)reset
{
    
    
}

- (void)dealloc {
    [_scroll release];
    [super dealloc];
}
@end
