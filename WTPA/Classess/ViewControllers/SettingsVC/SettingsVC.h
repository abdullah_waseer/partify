//
//  SettingsVC.h
//  WTPA
//
//  Created by Admin on 22/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (retain, nonatomic) IBOutlet UIScrollView *scroll;

- (IBAction)facebookToggleBtnPressed:(UIButton *)sender;
- (IBAction)twitterToggleBtnPressed:(UIButton *)sender;
- (IBAction)instagramToggleBtnPressed:(UIButton *)sender;
- (IBAction)profileToggleBtnPressed:(id)sender;
- (IBAction)pushToggleBtnPressed:(UIButton *)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)username;
- (IBAction)changeAddUserBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;

@end
