//
//  ViewController.m
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "CreateUserVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "GCPlaceholderTextView.h"
#import "LinkToOtherNetworkVC.h"
#import "TersmAndPrivacyVC.h"

@interface CreateUserVC ()


@property (nonatomic,retain) IBOutlet UITextField *txtUserName;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;
@property (nonatomic,retain) IBOutlet GCPlaceholderTextView *txtBio;
@property (nonatomic,retain) IBOutlet UIImageView *profileImage;
@property (nonatomic,retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic,retain) NSString * error;
@property (nonatomic,retain) NetworkManager * networkManager;


@end

@implementation CreateUserVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setUpNavigationBar];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    self.profileImage.layer.borderWidth=2;
    self.profileImage.layer.borderColor=[UIColor blackColor].CGColor;
    
    self.scrollView.contentSize=CGSizeMake(320, IS_IPHONE_5?568:600);
    
    self.txtBio.placeholder=@"Bio";
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

-(void) dealloc
{
    [_txtBio release],_txtBio=nil;
    [_txtEmail release],_txtEmail=nil;
    
    [_txtPassword release],_txtPassword=nil;
    
    [_txtUserName release],_txtUserName=nil;
    
    [_profileImage release],_profileImage=nil;
    
    [_networkManager release],_networkManager=nil;
    
    [_scrollView release],_scrollView=nil;
    
    [super dealloc];
}


#pragma mark - IBActions and Methods

- (IBAction)showTermAndConditoins:(id)sender
{
    TersmAndPrivacyVC *termsVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    termsVc.accessibilityHint = @"Terms";
    [self.navigationController pushViewController:termsVc animated:YES];
}

- (IBAction)showPrivacyPolicy:(id)sender
{
    TersmAndPrivacyVC *privacyVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    privacyVc.accessibilityHint = @"Privacy";
    [self.navigationController pushViewController:privacyVc animated:YES];
}

-(IBAction)addImageClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Select image"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Capture with camera", @"From PhotoLibrary", nil];
    [actionSheet showInView:self.view];
    [actionSheet release];
}


-(IBAction)createUserClicked:(id)sender
{
    if([self validate])
    {
            [self hidekeyBorad:nil];
            NSMutableDictionary *param=[NSMutableDictionary dictionaryWithObjectsAndKeys:self.txtUserName.text,@"username",
                                        self.txtEmail.text,@"email",
                                        self.txtPassword.text,@"password",
                                        self.txtBio.text,@"bio",
                                        @"public",@"visibility",
                                        nil];
            
            SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
            
            NSString *jsonString = [jsonWriter stringWithObject:param];
            
            [jsonWriter release];
            NSString* urlString = [NSString stringWithFormat:@"%@api/users/create",[SharedAppManager sharedInstance].baseURL];
            NSMutableDictionary  *parameters=[NSMutableDictionary dictionary];
            [parameters setObject:jsonString forKey:@"param"];
            [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
            [parameters setObject:urlString forKey:@"url"];
            [parameters setObject:@"POST" forKey:@"callType"];
            [parameters setObject:@"createUser" forKey:@"idetntifier"];
            
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            [_networkManager sendRequestforAction:parameters];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:self.error];
    }
}

#pragma mark - Custom Methods Statrt

- (BOOL)validate
{
    if(self.txtUserName.text.length == 0)
    {
        self.error = [NSString stringWithFormat:@"Please enter username"];
        return NO;
    }
    else if(![AppUtils validateEmail:self.txtEmail.text])
    {
        self.error = [NSString stringWithFormat:@"Please enter valid e-mail"];
        return NO;
    }
    else if(self.txtPassword.text.length == 0)
    {
        self.error = [NSString stringWithFormat:@"Please enter password"];
        return NO;
    }
    return YES;
}

-(void) setUpNavigationBar
{
    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
    titleView.image=[UIImage imageNamed:@"logo.png"];
    titleView.backgroundColor=[UIColor clearColor];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
//    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
//    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
//    backButton.frame=BACK_FRAME;
//    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
//    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = barButtonItem;
//    [barButtonItem release];
    
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) uploadImageToserver
{
    NSData *param = UIImageJPEGRepresentation(self.profileImage.image, 1.0);
    
     NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/uploadprofilephoto/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:param forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"image/png",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"POST" forKey:@"callType"];
    [parameters setObject:@"uploadImage" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_networkManager sendRequestforAction:parameters];

}

-(void) openLinkSocialnetworks
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    //[self.navigationController popToRootViewControllerAnimated:YES];
    LinkToOtherNetworkVC * linkToOtherNetworkVC = [[LinkToOtherNetworkVC alloc]init];
    [self.navigationController pushViewController:linkToOtherNetworkVC animated:YES];
    [linkToOtherNetworkVC release];
}

#pragma mark - Delegate Started
#pragma mark - NetworkMangerDelegate Delegate Methods

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"createUser"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            [[NSUserDefaults standardUserDefaults] setObject:self.txtPassword.text  forKey:PASSWORD];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if(self.profileImage.image)
            {
                [self uploadImageToserver];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController setNavigationBarHidden:NO animated:YES];
                //[self.navigationController popToRootViewControllerAnimated:YES];
                NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
                NSString *plistPath = [directoryPath objectAtIndex:0];
                plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
                NSMutableArray * userArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
                NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA]);
                NSMutableDictionary * dataToWrite = [[NSMutableDictionary alloc]initWithDictionary:[[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0]];
                [dataToWrite setObject:[[NSUserDefaults standardUserDefaults] valueForKey:PASSWORD] forKey:@"password"];
                if(!userArray)
                    userArray = [[NSMutableArray alloc]init];
                [userArray addObject:dataToWrite];
                [userArray exchangeObjectAtIndex:0 withObjectAtIndex:userArray.count-1];
                [userArray writeToFile:plistPath atomically:YES];
                LinkToOtherNetworkVC * linkToOtherNetworkVC = [[LinkToOtherNetworkVC alloc]init];
                [self.navigationController pushViewController:linkToOtherNetworkVC animated:YES];
                [linkToOtherNetworkVC release];
            }
        }
        else
        {
            NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
            NSString *plistPath = [directoryPath objectAtIndex:0];
            plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
            NSMutableArray * userArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA]);
            NSMutableDictionary * dataToWrite = [[NSMutableDictionary alloc]initWithDictionary:[[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0]];
            [dataToWrite setObject:[[NSUserDefaults standardUserDefaults] valueForKey:PASSWORD] forKey:@"password"];
            if(!userArray)
                userArray = [[NSMutableArray alloc]init];
            [userArray addObject:dataToWrite];
            [userArray exchangeObjectAtIndex:0 withObjectAtIndex:userArray.count-1];
            [userArray writeToFile:plistPath atomically:YES];
            [self.navigationController setNavigationBarHidden:NO animated:YES];
             //[self.navigationController  popToRootViewControllerAnimated:YES];
            if([SharedAppManager sharedInstance].addUser)
            {
                //[userProfileVC resetAllProperties];
                [SharedAppManager sharedInstance].addUser = NO;
                [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:NO];
                [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:NO];
            }
            LinkToOtherNetworkVC * linkToOtherNetworkVC = [[LinkToOtherNetworkVC alloc]init];
            [self.navigationController pushViewController:linkToOtherNetworkVC animated:YES];
            [linkToOtherNetworkVC release];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - UIACtionSheet Delegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0:
        {
            [self showImagePickerController: UIImagePickerControllerSourceTypeCamera];
        }
            break;
        case 1:
        {
            [self showImagePickerController: UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        }
        default:
            // Do Nothing.........
            break;
    }
}
-(void) showImagePickerController:(UIImagePickerControllerSourceType) sourceType
{
    if([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        UIImagePickerController * picker = [[[UIImagePickerController alloc] init] autorelease];
        picker.delegate = self;
        picker.allowsEditing=YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{}];
    }
}
#pragma mark - UIImagePIckerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *selectedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (!selectedImage)
    {
        return;
    }
    
    self.profileImage.image=selectedImage;
    
//    // Adjusting Image Orientation
//    NSData *data = UIImagePNGRepresentation(selectedImage);
//    UIImage *tmp = [UIImage imageWithData:data];
//    UIImage *fixed = [UIImage imageWithCGImage:tmp.CGImage
//                                         scale:selectedImage.scale
//                                   orientation:selectedImage.imageOrientation];
//    self.profileImage.image = fixed;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
}


#pragma mark - UITextFiled Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text=@"";
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextResponder=textField.tag+1;
    if(nextResponder!=3)
    {
        UITextField *txtfield=(UITextField *)[self.view viewWithTag:nextResponder];
        if(txtfield)
        {
            [txtfield becomeFirstResponder];
        }
    }
    else
    {
        UITextView *txtfield=(UITextView *)[self.view viewWithTag:nextResponder];
        if(txtfield)
        {
            [txtfield becomeFirstResponder];
        }
    }
    return YES;
}

-(IBAction)hidekeyBorad:(id)sender
{
    [_txtBio resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    [_txtUserName resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
