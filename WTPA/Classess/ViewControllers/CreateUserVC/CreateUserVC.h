//
//  ViewController.h
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateUserVC : UIViewController<NetworkManagerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@end
