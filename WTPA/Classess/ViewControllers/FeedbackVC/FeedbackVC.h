//
//  FeedbackVC.h
//  WTPA
//
//  Created by Sovoia on 02/06/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackVC : UIViewController


@property(strong, nonatomic) NSString *feedbackIssueId;

@property(strong, nonatomic) NSString *issueAbout;

@end
