//
//  FeedbackVC.m
//  WTPA
//
//  Created by Sovoia on 02/06/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import "FeedbackVC.h"

@interface FeedbackVC () <NetworkManagerDelegate, UIAlertViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableVw;
@property(strong,nonatomic) NSArray *feedbackArray;
@property(strong,nonatomic) NetworkManager *networkManager;
@end

@implementation FeedbackVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self.issueAbout  isEqual: @"event"])
    {
        self.feedbackArray = [[NSArray alloc]initWithObjects:@"I don't like this event", @"This event is a scam", @"This event puts people at risk", @"This event shouldn't be on Partify", nil];
    }
    else
    {
        self.feedbackArray = [[NSArray alloc]initWithObjects:@"I don't like this photo", @"This photo is a scam", @"This photo puts people at risk", @"This photo shouldn't be on Partify", nil];
    }
    
    [self setUpNavigationBar];
    
    self.networkManager = [[NetworkManager alloc]init];
    self.networkManager.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_tableVw release];
    [super dealloc];
}

-(void) setUpNavigationBar
{
    //    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
    //    self.view.layer.borderWidth=3;
    
    //    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
    //    titleView.image=[UIImage imageNamed:@"logo.png"];
    //    titleView.backgroundColor=[UIColor clearColor];
    //    [self.navigationItem setTitleView:titleView];
    //    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
//    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
    //    UIButton* done= [UIButton buttonWithType:UIButtonTypeCustom];
    //    [done addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    done.frame=CGRectMake(0, 5.0, 40.0, 34.0);
    //    done.backgroundColor=CUSTOM_APP_COLOR;
    //    done.titleLabel.font=[UIFont fontWithName:@"Thonburi" size:16];
    //    done.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    //    [done setTitle:@"Done" forState:UIControlStateNormal];
    //    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:done];
    //    self.navigationItem.rightBarButtonItem = rightBarButton;
    //    [rightBarButton release];
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Network Manager Delegate

-(void)submitFeedbackWithSubject:(NSString*)issueSubject
{
    NSString* urlString = [[NSString stringWithFormat:@"%@api/abusive/email?id=%@&type=%@&subject=%@",[SharedAppManager sharedInstance].baseURL,self.feedbackIssueId,self.issueAbout,issueSubject]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:AUTH_VALUE forKey:AUTH_KEY];
    [parameters setObject:dictionary forKey:@"headers"];

    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"feedback" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([networkManager.identifier isEqualToString:@"feedback"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thanks" message:@"Your feedback has been received" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


#pragma mark - Table Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedbackArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.indentationLevel = 2;
    }
    
    cell.textLabel.text = [self.feedbackArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self submitFeedbackWithSubject:cell.textLabel.text];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

@end
