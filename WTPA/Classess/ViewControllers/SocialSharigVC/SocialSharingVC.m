//

//  SocialSharingVC.m

//  WTPA

//

//  Created by Admin on 31/10/2013.

//  Copyright (c) 2013 Sovoia. All rights reserved.

//



#import "SocialSharingVC.h"

#import <Social/Social.h>

#import "HomeVC.h"
#import "AppDelegate.h"
#import "FaceBookManager.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "TagPickerVC.h"
#import "PeopleTagPickerVC.h"

#import <Twitter/Twitter.h>
#import "AJNotificationView.h"



@interface SocialSharingVC () <NetworkManagerDelegate,tagPickerVCDelegate,peopleTagPickerVCDelegate>



@property (nonatomic , retain) UIImageView * photoToUpload;

@property (nonatomic , retain) IBOutlet UIButton *tagPeopleButton;

@property (nonatomic , retain) NSString * imageId;

@property (nonatomic , retain) NSString * venueId;

@property (nonatomic , retain) NSString * locationId;

@property (nonatomic , retain) NSString * imageURL;

@property (nonatomic , retain) NSMutableArray * tagPeopleList;
@property  BOOL locationAdded;
@property  BOOL fromURL;

@end

@implementation SocialSharingVC

#pragma mark - Init



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil parameters:(NSMutableDictionary *)params
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization

        self.locationAdded = NO;
        self.photoToUpload = [[UIImageView alloc]init];
        if([params objectForKey:@"image"])
        {
            self.photoToUpload = [params objectForKey:@"image"];
            self.fromURL = NO;
        }
        if([params objectForKey:@"imageURL"])
        {
            self.imageURL = [[NSString alloc]initWithString:[params objectForKey:@"imageURL"]];
            self.fromURL = YES;
        }

        self.venueId = [NSString stringWithFormat:@"%@",[params objectForKey:@"caption"]];
        
        self.locationId = [NSString stringWithFormat:@"%@",[params objectForKey:@"locationid"]];
    }
    
    return self;
    
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpNavigationBar];

    self.scrollView.contentSize = CGSizeMake(320, 620);
    if(self.fromURL)
    {
//        self.postButton.frame = CGRectMake(106, 340, 109, 57);
//        self.socialNetworkBar.hidden = YES;
        [self.thumbnailImage setImageWithURL:[NSURL URLWithString:self.imageURL]
                        placeholderImage:nil options:nil
                                 success:^(UIImage *image, BOOL cached) {
                                     
                                     
                                     UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:320.0].CGImage];
                                     
                                     self.thumbnailImage.image = scaledImage;
                                     self.thumbnailImage.clipsToBounds = YES;
                                     self.photoToUpload.image = self.thumbnailImage.image;
                                     [scaledImage release];
                                 }
                                 failure:^(NSError *error) {
                                     
                                 }];
    }
    else
        self.thumbnailImage.image = self.photoToUpload.image;

    self.thumbnailImage.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.thumbnailImage.layer.borderWidth = 1.0;
    
    // Do any additional setup after loading the view from its nib.
    
}



- (void)didReceiveMemoryWarning

{
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}



#pragma mark - Custom Method Implementation

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (void)uploadPhotoOnFacebookWithLocation
{
    [[FaceBookManager sharedManager] publishImageWithLocation:self.locationId withImage:self.photoToUpload.image];
}

-(void) uploadImageToserver
{
    
    NSData *param = UIImageJPEGRepresentation(self.thumbnailImage.image, 0.5);
    
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/photos/upload/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    [parameters setObject:param forKey:@"param"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:self.tagPeopleList];
    
    [jsonWriter release];
    
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"image/png",@"Content-type",jsonString,@"TAGS",self.venueId,@"VENU_ID",nil] forKey:@"headers"];
    
    [parameters setObject:urlString forKey:@"url"];
    
    [parameters setObject:@"POST" forKey:@"callType"];
    
    [parameters setObject:@"uploadImage" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    
    networkManager.delegate = self;
    
    [networkManager sendRequestforAction:parameters];
}



-(void) tagImage

{
    
    NSMutableDictionary *params=[ NSMutableDictionary dictionary];
    
    [params setObject:self.imageId forKey:@"image_id"];
    
    [params setObject:self.venueId forKey:@"venue_id"];
    
    
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/photos/tagLocation",[SharedAppManager sharedInstance].baseURL];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:params];
    
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    [parameters setObject:jsonString forKey:@"param"];
    
    [jsonWriter release];
    
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json ",@"Content-type",nil] forKey:@"headers"];
    
    [parameters setObject:urlString forKey:@"url"];
    
    [parameters setObject:@"PUT" forKey:@"callType"];
    
    [parameters setObject:@"tagLocation" forKey:@"idetntifier"];
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    
    networkManager.delegate = self;
    
    [networkManager sendRequestforAction:parameters];
    
    
    
}



-(void) setUpNavigationBar

{
    
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    
//    self.view.layer.borderWidth=3;
    
    
    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    
//    titleView.backgroundColor=[UIColor clearColor];
//    
//    [self.navigationItem setTitleView:titleView];
//    
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    
    backButton.frame=BACK_FRAME;
    
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    [barButtonItem release];
    
    
    
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}



- (void)backAction:(id)sender

{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}





#pragma mark - IBActions Implementation



- (IBAction)shareFacebookPressed:(id)sender

{

    if(self.locationAdded)
        [self uploadPhotoOnFacebookWithLocation];
    else
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please add a location for this photo"];
    
    /*SLComposeViewController *composeController = [SLComposeViewController
     
     composeViewControllerForServiceType:  SLServiceTypeFacebook];
     
     
     
     [composeController setInitialText:@"Just found this great website"];
     
     //[composeController addImage:self.photoToUpload];
     
     [composeController addURL: [NSURL URLWithString:
     
     @"http://www.ebookfrenzy.com"]];
     
     
     
     [self presentViewController:composeController
     
     animated:YES completion:nil];*/
    
}



- (IBAction)shareTwitterPressed:(id)sender

{
    
    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    
    [tweetSheet addImage:self.photoToUpload.image];
    
    //[tweetSheet addURL:[NSURL URLWithString:displayURLstring]];
    
    [self presentModalViewController:tweetSheet animated:YES];
    TWTweetComposeViewControllerCompletionHandler
    completionHandler =
    ^(TWTweetComposeViewControllerResult result) {
        switch (result)
        {
            case TWTweetComposeViewControllerResultCancelled:
                NSLog(@"Twitter Result: canceled");
                
                break;
            case TWTweetComposeViewControllerResultDone:
                [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                                type:AJNotificationTypeGreen
                                               title:@"Posted on Twitter Successfully."
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                break;
            default:
                NSLog(@"Twitter Result: default");
                [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                                type:AJNotificationTypeRed
                                               title:@"Failed to post on Twitter."
                                     linedBackground:AJLinedBackgroundTypeDisabled
                                           hideAfter:2.5f];
                break;
        }
        [self dismissModalViewControllerAnimated:YES];
    };
    [tweetSheet setCompletionHandler:completionHandler];
}



- (IBAction)shareInstagramPressed:(id)sender

{
    
    
    
}



- (IBAction)sharePostPressed:(id)sender

{
    if(self.locationAdded)
        [self uploadImageToserver];
    else
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please add a location for this photo"];
    
}



- (IBAction)addLocationPressed:(id)sender

{
    
    TagPickerVC * tagPickerVC = [[TagPickerVC alloc]initWithNibName:@"TagPickerVC" bundle:nil];
    
    tagPickerVC.delegate = self;
    
    [self.navigationController pushViewController:tagPickerVC animated:YES];
    
}

- (IBAction)tagPeopleBnPressed:(id)sender
{
    
    NSData *imgData= UIImageJPEGRepresentation(_photoToUpload.image,0.8);
    PeopleTagPickerVC *peopleTagPickerVC = [[PeopleTagPickerVC alloc]initWithNibName:@"PeopleTagPickerVC" bundle:nil withdataDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:_fromURL?_imageURL:imgData,_fromURL?@"imageurl":@"image", nil]];
    peopleTagPickerVC.delegate = self;
    [self.navigationController pushViewController:peopleTagPickerVC animated:YES];
    [peopleTagPickerVC release];
}



#pragma mark - Delegate Method Implementation



#pragma mark - NetworkMangerDelegate Delegate Methods



- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object

{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"uploadImage"])
        
    {
        
        self.imageId = [NSString stringWithFormat:@"%@",[object objectForKey:@"data"]];
        
        [self tagImage];
        
    }
    
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"tagLocation"])
        
    {
        
        self.tabBarController.selectedIndex=0;
        
        UINavigationController *homeNav=[[self.tabBarController viewControllers] objectAtIndex:0];
        
        [homeNav popToRootViewControllerAnimated:NO];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    
    else
        
    {
        
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
        
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message

{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
    
}



#pragma mark - TagPickerVCDelegate

- (void) tagPickerVC:(TagPickerVC *)tagPickerVc withLocation:(NSMutableDictionary *)data

{
    
    self.locationId = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",[data objectForKey:@"locationid"]]];
    
    self.venueId = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",[data objectForKey:@"venuid"]]];
    [self.tagLocationBtn setTitle:[data objectForKey:@"name"] forState:UIControlStateNormal];
    self.locationAdded = YES;
}

#pragma mark - PeopleTagPickerVCDelegate

- (void) peopleTagPickerVC:(PeopleTagPickerVC*) peopleTagPickerVc withTagList:(NSMutableArray *)data
{
    self.tagPeopleList = [[NSMutableArray alloc]initWithArray:data];
    
    NSString *tagePeople=@"";
    
    for(int i=0;i<_tagPeopleList.count;i++)
    {
        tagePeople=[NSString stringWithFormat:@"%@%@%@ ",tagePeople,i==0?@"":@",",[[_tagPeopleList objectAtIndex:i] objectForKey:@"tagged_username"]];
    }
    if([tagePeople isEqualToString:@""])
    {
        [self.tagPeopleButton setTitle:@"Tag People" forState:UIControlStateNormal];
    }
    else
    {
        [self.tagPeopleButton setTitle:tagePeople forState:UIControlStateNormal];
    }
}



- (void)dealloc {
    
    [_thumbnailImage release];
    [_scrollView release];
    [_tagLocationBtn release];
    [_socialNetworkBar release];
    [_postButton release];
    [super dealloc];
    
}



@end
