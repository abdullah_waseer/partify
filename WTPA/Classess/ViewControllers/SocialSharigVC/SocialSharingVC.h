//
//  SocialSharingVC.h
//  WTPA
//
//  Created by Admin on 31/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialSharingVC : UIViewController
- (IBAction)shareFacebookPressed:(id)sender;
- (IBAction)shareTwitterPressed:(id)sender;
- (IBAction)shareInstagramPressed:(id)sender;
- (IBAction)sharePostPressed:(id)sender;
- (IBAction)addLocationPressed:(id)sender;
- (IBAction)tagPeopleBnPressed:(id)sender;


@property (retain, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (retain, nonatomic) IBOutlet UIButton *postButton;
@property (retain, nonatomic) IBOutlet UIButton *tagLocationBtn;
@property (retain, nonatomic) IBOutlet UIView *socialNetworkBar;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil parameters:(NSMutableDictionary *)params;

@end
