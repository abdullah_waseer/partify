//
//  LinkToOtherNetworkVC.h
//  WTPA
//
//  Created by Admin on 25/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkToOtherNetworkVC : UIViewController
- (IBAction)facebookBtnPressed:(id)sender;
- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)InstagramBtnPressed:(id)sender;
- (IBAction)doneBtnPressed:(id)sender;

@end
