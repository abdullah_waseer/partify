//
//  LogInVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 04/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "LogInVC.h"
#import "CreateUserVC.h"
#import "TersmAndPrivacyVC.h"

@interface LogInVC ()<NetworkManagerDelegate>

@property (nonatomic,retain) IBOutlet UITextField *txtUserName;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;

@property (nonatomic,retain) NetworkManager * networkManager;


@end

@implementation LogInVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) dealloc
{
    [_networkManager release],_networkManager=nil;
    [_txtPassword release],_txtPassword=nil;
    
    [_txtUserName release],_txtUserName=nil;
    
    [super dealloc];
}


#pragma mark - IBActions and Methods

- (IBAction)showTermAndConditoins:(id)sender
{
    TersmAndPrivacyVC *termsVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    termsVc.accessibilityHint = @"Terms";
    [self.navigationController pushViewController:termsVc animated:YES];
}

- (IBAction)showPrivacyPolicy:(id)sender
{
    TersmAndPrivacyVC *privacyVc = [[TersmAndPrivacyVC alloc]initWithNibName:@"TersmAndPrivacyVC" bundle:nil];
    privacyVc.accessibilityHint = @"Privacy";
    [self.navigationController pushViewController:privacyVc animated:YES];
}

-(IBAction)signUpClicked:(id)sender
{
    CreateUserVC *createUserVC=[[CreateUserVC alloc] initWithNibName:@"CreateUserVC" bundle:nil];
    createUserVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:createUserVC animated:NO];
    [createUserVC release];
    
}

-(IBAction)logInClicked:(id)sender
{
    if([self.txtUserName.text length]>0 || [self.txtPassword.text length]>0)
    {
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/authenticate",[SharedAppManager sharedInstance].baseURL];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
        [parameters setObject:jsonString forKey:@"param"];
        
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:self.txtUserName.text forKey:@"USERNAME"];
        
        [dictionary setObject:self.txtPassword.text forKey:@"PASSWORD"];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"loginuser" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please enter a valid username and password"];
    }
}
#pragma mark - Delegate Started
#pragma mark - NetworkMangerDelegate Delegate Methods

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
        
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            [[NSUserDefaults standardUserDefaults] setObject:self.txtPassword.text  forKey:PASSWORD];
            [[NSUserDefaults standardUserDefaults] synchronize];
       /* NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *plistPath = [directoryPath objectAtIndex:0];
        plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
        NSMutableArray * userArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA]);
        NSMutableDictionary * dataToWrite = [[NSMutableDictionary alloc]initWithDictionary:[[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0]];
        [dataToWrite setObject:[[NSUserDefaults standardUserDefaults] valueForKey:PASSWORD] forKey:@"password"];
        if(!userArray)
            userArray = [[NSMutableArray alloc]init];
        [userArray addObject:dataToWrite];
        [userArray exchangeObjectAtIndex:0 withObjectAtIndex:userArray.count-1];
        [userArray writeToFile:plistPath atomically:YES];*/
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - UITextFiled Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text=@"";
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextResponder=textField.tag+1;
    
    UITextField *txtfield=(UITextField *)[self.view viewWithTag:nextResponder];
    if(txtfield)
    {
        [txtfield becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(IBAction)hidekeyBorad:(id)sender
{
    [_txtPassword resignFirstResponder];
    [_txtUserName resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
