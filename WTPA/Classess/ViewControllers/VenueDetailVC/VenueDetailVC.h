//
//  VenueDetailVC.h
//  WTPA
//
//  Created by Admin on 23/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>
@interface myAnnotation : NSObject<MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
	NSInteger index;
}

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mSubTitle;
@property (nonatomic) NSInteger index;

@end
@interface VenueDetailVC : UIViewController
{
    myAnnotation *addressAnotation;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withVenueId:(NSString *)venueId;
@property (retain, nonatomic) IBOutlet UITableView *venueTable;
- (IBAction)eventCreatedBtnPessed:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;

@property(strong,nonatomic) NSDictionary *venueDictionaryFromLastFm;
@property(strong,nonatomic) NSString *eventSource;


@end
