//
//  VenueDetailVC.m
//  WTPA
//
//  Created by Admin on 23/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "VenueDetailVC.h"
#import "VenueDetailCustomCell.h"
#import "VenueDetailCustomCell1.h"
#import "UIImageView+WebCache.h"
#import "NSString+SBJSON.h"
#import "EventDetailVC.h"
#import "FeedbackVC.h"


@implementation myAnnotation

@synthesize coordinate;
@synthesize mTitle, mSubTitle;
@synthesize index;


static NSString* const GMAP_ANNOTATION_SELECTED = @"gMapAnnontationSelected";

- (NSString *)subtitle
{
	return mSubTitle;
}
- (NSString *)title
{
	return mTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTitle:(NSString*)thisTitle withSubTitle:(NSString*)thisSubTitle
{
	if(self = [super init])
	{
		coordinate=c;
		mTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisTitle]];
		mSubTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisSubTitle]];
	}
	return self;
}

- (void)dealloc
{
	[mTitle release];
	[mSubTitle release];
    [super dealloc];
}

@end


@interface VenueDetailVC ()<NetworkManagerDelegate>

@property (nonatomic , retain) NSMutableArray * venueData;
@property (nonatomic , retain) NSString * venueId;
@property (nonatomic , retain) NSDictionary * venueDic;
@property (nonatomic , retain) UILabel * titleView;

@end

@implementation VenueDetailVC

NetworkManager * networkManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withVenueId:(NSString *)venueId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.venueId = [NSString stringWithFormat:@"%@",venueId];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    
    if (![self.eventSource  isEqual: @"LastFm"])
    {
        [self performSelector:@selector(makeUserFeedCall) withObject:nil afterDelay:0.1];
    }
    else
    {
        _titleView.text = [self.venueDictionaryFromLastFm objectForKey:@"name"];
        
        [self drawPinWithLatitude:[[self.venueDictionaryFromLastFm objectForKey:@"location"] objectForKey:@"lat"] longitude:[[self.venueDictionaryFromLastFm objectForKey:@"location"] objectForKey:@"long"]];
    }
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Custom Methods

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    self.titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 200, 27)];
    _titleView.font = [UIFont fontWithName:@"Thonburi" size:22.0];
    _titleView.textColor = [UIColor whiteColor];
    _titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.text =[[data objectForKey:@"username" ] uppercaseString];
    [self.navigationItem setTitleView:_titleView];
    [_titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];

}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)makeUserFeedCall
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/venu/detail/%@",[SharedAppManager sharedInstance].baseURL,self.venueId];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getUserFeed" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}
- (void) drawPinCordinates
{
    	MKCoordinateRegion region;
    	MKCoordinateSpan span;
    	span.latitudeDelta=0.06;
    	span.longitudeDelta=0.06;
    
    	region.span = span;
    
    region.center.latitude = [[self.venueDic objectForKey:@"lat"] floatValue];
    region.center.longitude = [[self.venueDic objectForKey:@"long"] floatValue];
	
	if(addressAnotation != nil)
	{
		[_mapView removeAnnotation:addressAnotation];
		[addressAnotation release];
		addressAnotation = nil;
	}
	
	addressAnotation = [[myAnnotation alloc] initWithCoordinate:region.center withTitle:@"Venue" withSubTitle:[NSString stringWithFormat:@"%@ ,%@",[self.venueDic objectForKey:@"name"], [self.venueDic objectForKey:@"address"]]];
	[_mapView addAnnotation:addressAnotation];
    [_mapView setRegion:region animated:TRUE];
    [_mapView regionThatFits:region];
}

- (void) drawPinWithLatitude:(NSString*)latitude longitude:(NSString*)longitude
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.06;
    span.longitudeDelta=0.06;
    
    region.span = span;
    
    region.center.latitude = [latitude floatValue];
    region.center.longitude = [longitude floatValue];
	
	if(addressAnotation != nil)
	{
		[_mapView removeAnnotation:addressAnotation];
		[addressAnotation release];
		addressAnotation = nil;
	}
	
	addressAnotation = [[myAnnotation alloc] initWithCoordinate:region.center withTitle:@"Venue" withSubTitle:[NSString stringWithFormat:@"%@ ,%@",[[self.venueDictionaryFromLastFm objectForKey:@"location"] objectForKey:@"city"], [[self.venueDictionaryFromLastFm objectForKey:@"location"] objectForKey:@"street"]]];
	[_mapView addAnnotation:addressAnotation];
    [_mapView setRegion:region animated:TRUE];
    [_mapView regionThatFits:region];
}

-(void)bringFeedbackVC:(UIButton*)senderBtn
{
    FeedbackVC *feedbackVC = [[FeedbackVC alloc]initWithNibName:@"FeedbackVC" bundle:nil];
    feedbackVC.feedbackIssueId = senderBtn.accessibilityIdentifier;
    feedbackVC.issueAbout = @"photo";
    [self.navigationController pushViewController:feedbackVC animated:YES];
}

#pragma mark - IBActions

- (IBAction)eventCreatedBtnPessed:(UIButton *)sender
{
    NSMutableDictionary * eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[[[[self.venueData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"event_detail"]objectForKey:@"id"],@"id",@"YES",@"type",@"DB",@"source",nil];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventDetail];
    eventDetailVC.venueId = [[[[self.venueData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetail release];
}

#pragma mark - Delegates
#pragma mark - MKMapView Delegate Methods

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    if([annotation isKindOfClass:[myAnnotation class]])
    {
        MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
        annView.pinColor = MKPinAnnotationColorGreen;
        //annView.frame = CGRectMake(-5, 5, 200, 70);
        annView.animatesDrop=TRUE;
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-5, 5);
        
        return [annView autorelease];
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
{
	
}

#pragma mark - Table Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.venueData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    int padding = 5;
    if([[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"image_upload"])
    {
        VenueDetailCustomCell *cell = (VenueDetailCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"VenueDetailCustomCell"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (VenueDetailCustomCell *)currentObject;
                    [cell.feedbackBtn addTarget:self action:@selector(bringFeedbackVC:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                }
            }
        }
        
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"image_url"]];
        [cell.image setImageWithURL:[NSURL URLWithString:url]];
        cell.image.clipsToBounds=YES;
        
        cell.feedbackBtn.accessibilityIdentifier = [[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"id"];
        
        float maxSize = 90.0;
        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize = [[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.venueAt.frame = CGRectMake(cell.venueAt.frame.origin.x, cell.venueAt.frame.origin.y, expectedSize.width+padding+10>157?157:expectedSize.width+padding+10, 20);
        [cell.venueAt setTitle:[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
            cell.tag = indexPath.row;
        NSDate * actDate = [AppUtils getDateFromString:[[self.venueData objectAtIndex:indexPath.row]objectForKey:@"activity_time"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        cell.time.text = [AppUtils timeFormatted:[NSDate date] toDate:actDate];
        return cell;
    }
    else if([[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"event_creation"])
    {
        
        VenueDetailCustomCell1 *cell = (VenueDetailCustomCell1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"VenueDetailCustomCell1"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (VenueDetailCustomCell1 *)currentObject;
                    break;
                }
            }
        }

        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        expectedSize = [[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.eventName.frame = CGRectMake(cell.eventName.frame.origin.x, cell.eventName.frame.origin.y, expectedSize.width+padding+10, 30);
        cell.hosted.frame = CGRectMake(cell.eventName.frame.origin.x+cell.eventName.frame.size.width, cell.hosted.frame.origin.y, cell.hosted.frame.size.width, cell.hosted.frame.size.height);
        expectedSize = [[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.venueName.frame = CGRectMake(cell.hosted.frame.origin.x+cell.hosted.frame.size.width, cell.venueName.frame.origin.y, expectedSize.width+padding+10, 30);
        [cell.eventName setTitle:[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] forState:UIControlStateNormal];
        [cell.venueName setTitle:[[[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        cell.tag = indexPath.row;
        cell.eventName.tag=indexPath.row;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[self.venueData objectAtIndex:indexPath.row] objectForKey:@"activity_type"]isEqualToString:@"image_upload"])
        return 151;
    else
        return 44;
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"getUserFeed"])
    {
        self.venueDic = [[NSDictionary alloc]initWithDictionary:[[object objectForKey:@"data"] objectAtIndex:0]];
        self.venueData = [[NSMutableArray alloc]initWithArray:[[object objectForKey:@"data"] objectAtIndex:1]];
        _titleView.text = [[[object objectForKey:@"data"] objectAtIndex:0] valueForKey:@"name"];
        [self drawPinCordinates];
        [self.venueTable reloadData];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_venueTable release];
    [_mapView release];
    [super dealloc];
}

@end
