//
//  VenueDetailCustomCell.h
//  WTPA
//
//  Created by Admin on 23/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueDetailCustomCell : UITableViewCell

@property (nonatomic , retain) IBOutlet UIImageView * image;
@property (nonatomic , retain) IBOutlet UIButton * venueAt;
@property (nonatomic , retain) IBOutlet UILabel * takenAt;
@property (nonatomic , retain) IBOutlet UILabel * time;

@property (retain, nonatomic) IBOutlet UIButton *feedbackBtn;

@end
