//
//  VenueDetailCustomCell1.h
//  WTPA
//
//  Created by Admin on 23/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueDetailCustomCell1 : UITableViewCell

@property (nonatomic , retain) IBOutlet UIButton * eventName;
@property (nonatomic , retain) IBOutlet UIButton * venueName;
@property (nonatomic , retain) IBOutlet UILabel * hosted;

@end
