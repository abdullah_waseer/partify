//
//  FollowingCustomCellVC.h
//  WTPA
//
//  Created by Admin on 21/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowerCustomCellVC : UITableViewCell

@property (nonatomic , retain) IBOutlet UILabel * nameLbl;
@property (nonatomic , retain) IBOutlet UIButton * blockBtn;
@property (nonatomic , retain) IBOutlet UIImageView * userImage;
@property (nonatomic , retain) IBOutlet UIImageView * userNameBackground;

@end
