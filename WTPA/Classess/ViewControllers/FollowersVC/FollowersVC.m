//
//  FollowersVC.m
//  WTPA
//
//  Created by Admin on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "FollowersVC.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "UserProfileVC.h"
#import "FollowerCustomCellVC.h"

@interface FollowersVC () <NetworkManagerDelegate,UIAlertViewDelegate>

@property (nonatomic , retain) NSString * username;
@property (nonatomic , retain) NSString * reqId;
@property (nonatomic , retain) NSMutableArray * followersData;
@property (nonatomic , retain) NSString * idToBlock;
@end

@implementation FollowersVC

NetworkManager * networkManager;

int indexofBtn;

#pragma mark - View init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)username andRequester:(NSString *)reqId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.username = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",username]];
        self.reqId = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",reqId]];
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self followersDetail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, 240, 44)];
//    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
//    titleView.textColor = [UIColor whiteColor];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.text = [self.username uppercaseString];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.titleLabel.font = [UIFont fontWithName:@"Thonburi" size:36.0];
    titleButton.titleLabel.textColor = [UIColor whiteColor];
    titleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleButton setTitle:[self.username uppercaseString] forState:UIControlStateNormal];
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}
-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

- (void)backAction:(id)sender
{
     networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) followersDetail
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/userfollowers/%@",[SharedAppManager sharedInstance].baseURL,self.reqId];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",@"all",@"STATUS",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"followersDetail" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

- (void) blockFollower
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/changefollowerstatus/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSMutableDictionary * params= [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.idToBlock,@"follower_id",@"blocked",@"status", nil];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:params] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"PUT" forKey:@"callType"];
    [parameters setObject:@"blockFollower" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [networkManager sendRequestforAction:parameters];
}

- (void) unblockFollower
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/changefollowerstatus/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSMutableDictionary * params= [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.idToBlock,@"follower_id",@"following",@"status", nil];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:params] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"PUT" forKey:@"callType"];
    [parameters setObject:@"unblockFollower" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [networkManager sendRequestforAction:parameters];
}

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)imageWithImage:(UIImage*)sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height; // 161
    float scaleFactor = i_height / oldHeight; // 51/161 = 0.3167
    
    float newHeight =  oldHeight * scaleFactor;//sourceImage.size.height * scaleFactor; //54.87
    float newWidth =  sourceImage.size.width * scaleFactor;//oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}



#pragma mark - IBAction Implementation

- (IBAction)blockBtnPressed:(UIButton *)sender
{
    NSLog(@"%d",sender.tag);
    if(sender.tag > 0)
    {
        indexofBtn = sender.tag-1;
        self.idToBlock = [NSString stringWithFormat:@"%@",[[self.followersData objectAtIndex:indexofBtn] objectForKey:@"id"]];
        NSString * message = [NSString stringWithFormat:@"Are you sure, you want to block %@?",[[self.followersData objectAtIndex:indexofBtn] objectForKey:@"username"]];
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Partify"
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"Ok",nil] autorelease];
        alert.tag = 0;
        [alert show];
        
    }
    else
    {
        indexofBtn = (-1 * sender.tag)-1;
        self.idToBlock = [NSString stringWithFormat:@"%@",[[self.followersData objectAtIndex:indexofBtn] objectForKey:@"id"]];
        [self unblockFollower];
    }
}

#pragma mark - Delegate Methods

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self blockFollower];
    }
    else
    {
        
    }
}
#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.followersData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    FollowerCustomCellVC *cell = (FollowerCustomCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"FollowerCustomCellVC"] owner:self options:nil];
        
        for(id currentObject in objects)
        {
            
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (FollowerCustomCellVC *)currentObject;
                break;
            }
        }
    }
    NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[self.followersData objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]];
    cell.userImage.clipsToBounds=YES;
    [cell.userImage setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil options:nil
                            success:^(UIImage *image, BOOL cached) {
                                
                                
//                                UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:221.0].CGImage];
                                
                                cell.userImage.image = image;
//                                cell.userImage.clipsToBounds = YES;
//                                [scaledImage release];
                            }
                            failure:^(NSError *error) {
                                
                            }];
    //cell.textLabel.font = [UIFont fontWithName:@"Thonburi" size:20.0];
    //cell.textLabel.backgroundColor = [UIColor clearColor];
    //cell.textLabel.textColor = [UIColor blackColor];
    cell.nameLbl.text = [[self.followersData objectAtIndex:indexPath.row] objectForKey:@"username"];
    [cell.blockBtn setBackgroundImage:[UIImage imageNamed:@"block.png"] forState:UIControlStateNormal];
    cell.blockBtn.tag = indexPath.row+1;
    if([[[self.followersData objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"blocked"])
    {
        //[cell.blockBtn setImage:[UIImage imageNamed:@"unblock.png"] forState:UIControlStateNormal];
        [cell.blockBtn setBackgroundImage:[UIImage imageNamed:@"unblock.png"] forState:UIControlStateNormal];
        cell.blockBtn.tag = -1 * (indexPath.row+1);
    }
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    if(![[data objectForKey:@"id"] isEqualToString:self.reqId])
    {
        cell.blockBtn.hidden=YES;
        
        CGRect si=cell.nameLbl.frame;
        si.size.width=320;
        
        [cell.nameLbl setFrame:si];
    }
    
    [cell setNeedsLayout];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor=[UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[self.followersData objectAtIndex:indexPath.row] objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"followersDetail"])
    {
        self.followersData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
        [self.table reloadData];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"blockFollower"])
    {
        [[self.followersData objectAtIndex:indexofBtn ] setObject:@"blocked" forKey:@"status"];
        [self.table reloadData];
        //self.table cellForRowAtIndexPath:<#(NSIndexPath *)#>
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"unblockFollower"])
    {
        [[self.followersData objectAtIndex:indexofBtn] setObject:@"following" forKey:@"status"];
        [self.table reloadData];
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }    }
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - View Dealloc

- (void)dealloc {
    [_table release];
    [super dealloc];
}

@end
