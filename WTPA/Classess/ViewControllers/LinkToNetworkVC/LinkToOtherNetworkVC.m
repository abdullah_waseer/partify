//
//  LinkToOtherNetworkVC.m
//  WTPA
//
//  Created by Admin on 25/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "LinkToOtherNetworkVC.h"
#import "FaceBookManager.h"
#import <Twitter/Twitter.h>
#import "SharedAppManager.h"

@interface LinkToOtherNetworkVC () <IGSessionDelegate>

@end

@implementation LinkToOtherNetworkVC

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Method Implementation

-(void) setUpNavigationBar
{
    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
    titleView.image=[UIImage imageNamed:@"logo.png"];
    titleView.backgroundColor=[UIColor clearColor];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IbActions Implementation

- (IBAction)facebookBtnPressed:(id)sender
{
    [[FaceBookManager sharedManager] checkIfFacebookIsLoggedIn];
}

- (IBAction)twitterBtnPressed:(id)sender
{
    if(![TWTweetComposeViewController canSendTweet])
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Twitter application is required."];
    }
    
}

- (IBAction)InstagramBtnPressed:(id)sender
{
    [SharedAppManager sharedInstance].instagram.sessionDelegate = self;
    [[SharedAppManager sharedInstance].instagram authorize:[NSArray arrayWithObjects:@"comments", @"likes", nil]];
}

- (IBAction)doneBtnPressed:(id)sender
{
    [self.navigationController  popToRootViewControllerAnimated:YES];
}

#pragma mark - Delegates Implementation

#pragma - IGSessionDelegate

-(void)igDidLogin {
    NSLog(@"Instagram did login");
    // here i can store accessToken
    
    [[NSUserDefaults standardUserDefaults] setObject:[SharedAppManager sharedInstance].instagram.accessToken forKey:@"accessTokenIns"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)igDidNotLogin:(BOOL)cancelled {
    NSLog(@"Instagram did not login");
    NSString* message = nil;
    if (cancelled) {
        message = @"Access cancelled!";
    } else {
        message = @"Access denied!";
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(void)igDidLogout {
    NSLog(@"Instagram did logout");
    // remove the accessToken
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"accessToken"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)igSessionInvalidated {
    NSLog(@"Instagram session was invalidated");
}


@end
