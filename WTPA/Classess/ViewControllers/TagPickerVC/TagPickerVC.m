//
//  TagPickerVC.m
//  WTPA
//
//  Created by Admin on 31/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "TagPickerVC.h"
#import "SocialSharingVC.h"
#import "FaceBookManager.h"


@interface TagPickerVC () <NetworkManagerDelegate>

@property (nonatomic , retain) NSMutableArray * venueArray;
@property (nonatomic , retain) NSMutableDictionary * imageWithCaptionDictionary
;
@property (nonatomic , retain) NSDictionary * createVenueData;
@property (nonatomic , retain) NSDictionary *LatLng;
@property (nonatomic , retain) NSString *keyword;
@property (nonatomic , retain) NSString * locationid;
@property (nonatomic , retain) NSString *locationName;

@end

@implementation TagPickerVC



#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil parameters:(NSMutableDictionary *)params
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        //[FaceBookManager sharedManager].requestType = RequestPostFeed;
       
        self.imageWithCaptionDictionary = [[NSMutableDictionary alloc]initWithDictionary:params];
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"])
        [[FaceBookManager sharedManager] checkIfFacebookIsLoggedIn];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	if([CLLocationManager locationServicesEnabled])
    {
        [locationManager startUpdatingLocation];
    }
    //[self getVenueListFromServer];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Method Implementation
-(void) getVenuListFromFacebookByLatLng
{
    NSString* urlString = [NSString stringWithFormat:@"https://graph.facebook.com/search?type=place&center=%@,%@&distance=1000&access_token=%@",[self.LatLng objectForKey:@"lat"],[self.LatLng objectForKey:@"long"],[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    [jsonWriter release];
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionary] forKey:@"headers"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getVenueListFromFacebookByLatLng" forKey:@"idetntifier"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

-(void) getVenuListFromFacebook
{
    NSString* urlString = [NSString stringWithFormat:@"https://graph.facebook.com/search?q=%@&type=place&access_token=%@",self.keyword,[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"]];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    [jsonWriter release];

    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionary] forKey:@"headers"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getVenueListFromFacebook" forKey:@"idetntifier"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

-(void) getVenueListFromServer
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/venu/list",[SharedAppManager sharedInstance].baseURL];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionary] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getVenueList" forKey:@"idetntifier"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

-(void) createVenue
{
        NSString* urlString = [NSString stringWithFormat:@"%@api/venu/createvenu",[SharedAppManager sharedInstance].baseURL];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:self.createVenueData];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        [parameters setObject:jsonString forKey:@"param"];
        [jsonWriter release];
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json ",@"Content-type",nil] forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"POST" forKey:@"callType"];
        [parameters setObject:@"createVenue" forKey:@"idetntifier"];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NetworkManager * networkManager = [[NetworkManager alloc] init];
        networkManager.delegate = self;
        [networkManager sendRequestforAction:parameters];
}

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    locationManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction Implementation

#pragma mark - Delegate Implementation
#pragma mark - Network Delegate Implementation
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([networkManager.identifier isEqualToString:@"getVenueList"])
    {
        if([[object objectForKey:@"status"] isEqualToString:@"success"])
        {
            self.venueArray = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
            [self.table reloadData];
        }
        else
        {
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
        }
    }
    else if([networkManager.identifier isEqualToString:@"getVenueListFromFacebook"])
    {
       self.venueArray = [NSMutableArray arrayWithArray:[object objectForKey:@"data"]];
        
        [self.table reloadData]; 
    }
    else if([networkManager.identifier isEqualToString:@"getVenueListFromFacebookByLatLng"])
    {
        NSArray *tempArray=[object objectForKey:@"data"];
        NSMutableArray *array=[NSMutableArray array];
        
        for(int i=0;i<tempArray.count;i++)
        {
            if(![[[[tempArray objectAtIndex:i] objectForKey:@"location"] objectForKey:@"street"] isEqualToString:@""])
            {
                [array addObject:[tempArray objectAtIndex:i]];
            }
            
        }
        self.venueArray = [NSMutableArray arrayWithArray:array];
        
        
        [self.table reloadData];
    }
    else if([networkManager.identifier isEqualToString:@"createVenue"] && [[object valueForKey:@"status"] isEqualToString:@"success"])
    {
        NSMutableDictionary * datatoPass = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.locationName,@"name",[object valueForKey:@"data"],@"venuid",self.locationid,@"locationid",self.createVenueData,@"venu", nil];
        if([self.delegate respondsToSelector:@selector(tagPickerVC:withLocation:)])
        {
            [self.delegate tagPickerVC:self withLocation:datatoPass];
            [self.navigationController popViewControllerAnimated:YES];
        }
       
         /*SocialSharingVC * socialSharingVC = [[SocialSharingVC alloc]initWithNibName:@"SocialSharingVC" bundle:nil parameters:self.imageWithCaptionDictionary];
         [self.navigationController pushViewController:socialSharingVC animated:YES];
         [socialSharingVC release];*/
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}
#pragma mark - TableView Delegate Implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venueArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [cell.textLabel setText:[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
    [cell.detailTextLabel setText:[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"street"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar resignFirstResponder];
    self.createVenueData =[[NSDictionary alloc]initWithObjectsAndKeys:[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"name"],@"name",[[NSDictionary alloc] initWithObjectsAndKeys:[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"latitude"],@"lat",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"longitude"],@"long", nil],@"venu_location",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"street"],@"address",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"city"],@"city",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"country"],@"country",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"state"],@"state",[[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"location"] objectForKey:@"zip"],@"zip",nil];
    self.locationid = [NSString stringWithFormat:@"%@",[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"id"]];
    self.locationName = [NSString stringWithFormat:@"%@",[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
    
    
    [self createVenue];
    /*NSString * caption = [NSString stringWithFormat:@"%@",[[self.venueArray objectAtIndex:indexPath.row] valueForKey:@"id"]];
    [self.imageWithCaptionDictionary setObject:caption forKey:@"caption"];
    SocialSharingVC * socialSharingVC = [[SocialSharingVC alloc]initWithNibName:@"SocialSharingVC" bundle:nil parameters:self.imageWithCaptionDictionary];
    [self.navigationController pushViewController:socialSharingVC animated:YES];
    [socialSharingVC release];*/
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Location Manager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {

        self.LatLng=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude],@"long",nil];
        [locationManager stopUpdatingLocation];
    [self getVenuListFromFacebookByLatLng];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"
                                                    message:errorType delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.keyword = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",self.searchBar.text]];
    [self getVenuListFromFacebook];
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [_searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length==0 )
    {
        [searchBar resignFirstResponder];
    }
}

#pragma mark - Destructor

- (void)dealloc {
    [_table release];
    [_searchBar release];
    [super dealloc];
}
@end
