//
//  TagPickerVC.h
//  WTPA
//
//  Created by Admin on 31/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class TagPickerVC;

@protocol tagPickerVCDelegate <NSObject>

- (void) tagPickerVC:(TagPickerVC*) tagPickerVc withLocation:(NSMutableDictionary *)data;

@end

@interface TagPickerVC : UIViewController <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property (retain, nonatomic) IBOutlet UITableView *table;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property (assign, nonatomic) id<tagPickerVCDelegate> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil parameters:(NSMutableDictionary *)params;

@end
