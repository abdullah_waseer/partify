//
//  PartyMapVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 28/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import "PartyMapVC.h"
#import "CUIButton.h"
#import "UIImageView+WebCache.h"
#import "PartyImageVC.h"


@implementation PartyMapAnnotation

@synthesize coordinate;
@synthesize mTitle, mSubTitle;
@synthesize index;


static NSString* const GMAP_ANNOTATION_SELECTED = @"gMapAnnontationSelected";

- (NSString *)subtitle
{
	return mSubTitle;
}
- (NSString *)title
{
	return mTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTitle:(NSString*)thisTitle withSubTitle:(NSString*)thisSubTitle
{
	if(self = [super init])
	{
		coordinate=c;
		mTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisTitle]];
		mSubTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisSubTitle]];
	}
	return self;
}

- (void)dealloc
{
	[mTitle release];
	[mSubTitle release];
    [super dealloc];
}

@end


@interface PartyMapVC ()<NetworkManagerDelegate>

@property (nonatomic,retain) NetworkManager * networkManager;
@property (nonatomic,retain) NSMutableDictionary *latLong;
@property (nonatomic,retain) NSMutableArray *followingArray;

@end

@implementation PartyMapVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpNavigationBar];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
    }

    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
//    [self performSelector:@selector(followingData) withObject:nil afterDelay:0.1];

    // Do any additional setup after loading the view from its nib.
    
}

-(void) setUpNavigationBar
{
    //    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
    //    self.view.layer.borderWidth=3;
    
    //    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
    //    titleView.image=[UIImage imageNamed:@"logo.png"];
    //    titleView.backgroundColor=[UIColor clearColor];
    //    [self.navigationItem setTitleView:titleView];
    //    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}
- (void)backAction:(id)sender
{
    locationManager.delegate=nil;
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

-(void) followingData
{
    NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/events/userevents/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    
    
    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    [dictionary setObject:jsonString forKey:@"param"];
    
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"ownevents" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

- (void) drawMultiPleEventsPins
{
	MKCoordinateRegion region;
    MKCoordinateSpan span;
	span.latitudeDelta=0.06;
	span.longitudeDelta=0.06;
	region.span = span;
   
    for(int i=0;i<_followingArray.count;i++)
    {
       
        CLLocationCoordinate2D cord ;
        cord.latitude=[[[[[_followingArray objectAtIndex:i]  objectForKey:@"venue"] objectForKey:@"location"]objectForKey:@"lat"] floatValue];
        cord.longitude=[[[[[_followingArray objectAtIndex:i]  objectForKey:@"venue"] objectForKey:@"location"]objectForKey:@"long"] floatValue];
        
        addressAnotation = [[PartyMapAnnotation alloc] initWithCoordinate:cord withTitle:_followingArray[i][@"event_name"] withSubTitle:_followingArray[i][@"event_start_time"]];
        addressAnotation.index=i;
        
        [_partryMapView addAnnotation:addressAnotation];
        [addressAnotation  release],addressAnotation=nil;
        
        if(i==0)
        {
            region.center=cord;
            [_partryMapView setRegion:region animated:false];
            [_partryMapView regionThatFits:region];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if(!location)
    {
        location=YES;
        self.latLong=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude],@"long",nil];
        [locationManager stopUpdatingLocation];
        [self followingData];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"
                                                    message:errorType delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

#pragma mark - MapView Delegate Methods
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"");
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[PartyMapAnnotation class]])
    {
        PartyMapAnnotation *basic=(PartyMapAnnotation *)annotation;
        
		MKPinAnnotationView *annotationView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                                               reuseIdentifier:@"CustomAnnotation"] autorelease];
		annotationView.canShowCallout = YES;
        annotationView.calloutOffset=CGPointMake(-5, 5);
        annotationView.image=nil;
//        annotationView.animatesDrop=YES;
        
        UIImageView *borderImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 61, 62)];
        borderImage.backgroundColor=[UIColor clearColor];
        borderImage.tag=basic.index;
        borderImage.image=[UIImage imageNamed:@"photo_thumbnail"];
        
        [annotationView addSubview:borderImage];
        
        UIImageView *venueImage=[[UIImageView alloc] initWithFrame:CGRectMake(3, 3, 55, 56)];
        venueImage.backgroundColor=[UIColor clearColor];
        venueImage.tag=basic.index;
        venueImage.userInteractionEnabled=YES;
        NSArray *images=[[_followingArray objectAtIndex:basic.index] objectForKey:@"images"];
        
        if(images.count>0)
        {
            [venueImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL  ,[[images objectAtIndex:0] objectForKey:@"image_path"
                                                              ]]]
                       placeholderImage:nil options:nil
                                success:^(UIImage *image, BOOL cached) {
                                    
                                    venueImage.image=image;                                }
                                failure:^(NSError *error) {
                                    
                                }];
        }
        else
        {
            venueImage.image=[UIImage imageNamed:@""];
        }
        [borderImage addSubview:venueImage];
        
        CUIButton *button=[CUIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(callOutClicked:) forControlEvents:UIControlEventTouchDown];
//        button.tag=CALLOUT_BUTTON_TAG;
        button.frame=button.frame=CGRectMake(3, 3, 55, 56);
        button.backgroundColor=[UIColor clearColor];
        button.index=basic.index;
        [venueImage addSubview:button];
//        [button setImage:[UIImage imageNamed:@"next-page-arrow.png"] forState:UIControlStateNormal];
        
		return annotationView;
    }
    //	}
	return nil;
}

-(void) callOutClicked:(id) sender
{
    CUIButton *button=(CUIButton *)sender;
    NSMutableDictionary *eventData=[NSMutableDictionary dictionaryWithDictionary:[_followingArray objectAtIndex:button.index]];
    
    PartyImageVC *eventDetailVC=[[PartyImageVC alloc]initWithNibName:@"PartyImageVC" bundle:nil andData:eventData];
    [self presentViewController:eventDetailVC animated:YES completion:^{}];
   [eventDetailVC release];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    location=NO;
    
    if ([view.annotation isKindOfClass:[PartyMapAnnotation class]])
    {
        PartyMapAnnotation *basic=(PartyMapAnnotation *)view.annotation;
        
        NSMutableDictionary *eventData=[NSMutableDictionary dictionaryWithDictionary:[_followingArray objectAtIndex:basic.index]];
        
        PartyImageVC *eventDetailVC=[[PartyImageVC alloc]initWithNibName:@"PartyImageVC" bundle:nil andData:eventData];
        [self presentViewController:eventDetailVC animated:YES completion:^{}];
        [eventDetailVC release];
    }
    
    for( PartyMapAnnotation *aMKAnn in  [_partryMapView annotations])
    {
    	//dodgy select then deselect each annotation
    	[_partryMapView deselectAnnotation:aMKAnn animated:NO];
    }
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    location=YES;

}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    //    CLLocationCoordinate2D centerCoordinate=mapView.centerCoordinate;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if(location)
    {
        CLLocationCoordinate2D centerCoordinate=mapView.centerCoordinate;
        if(self.latLong)
            [self.latLong release],self.latLong=nil;
        
        self.latLong=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",centerCoordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",centerCoordinate.longitude],@"long",nil];
        
    }
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"ownevents"])
    {
        _followingArray=[[NSMutableArray alloc] initWithArray:[[object objectForKey:@"data"] objectForKey:@"following"]];
        [self drawMultiPleEventsPins];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
