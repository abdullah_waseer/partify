//
//  PartyImageVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 29/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import "PartyImageVC.h"
#import "UIImageView+WebCache.h"

@interface PartyImageVC ()
@property (nonatomic , retain) NSMutableDictionary *dataDictionary;
@property (nonatomic , retain) IBOutlet UIImageView *partyImage;

@property (nonatomic , retain) IBOutlet UIView *subView;

@end

@implementation PartyImageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andData:(NSMutableDictionary *) data
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dataDictionary =[[NSMutableDictionary alloc] initWithDictionary:data];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(IS_IPHONE_5)
    {
        self.view.frame=CGRectMake(0, 0, 320, 568);
        self.subView.frame=CGRectMake(0, 84, 320, 400);
    }
    else
    {
        self.view.frame=CGRectMake(0, 0, 320, 480);
        self.subView.frame=CGRectMake(0, 40, 320, 400);
    }
    
    
    NSArray *images=[_dataDictionary objectForKey:@"images"];
    if(images.count>0)
    {
        [_partyImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL  ,[[images objectAtIndex:0] objectForKey:@"image_path"
                                                                                                                                     ]]]
               placeholderImage:nil options:nil
                        success:^(UIImage *image, BOOL cached) {
                            
                            _partyImage.image=image;                                }
                        failure:^(NSError *error) {
                            
                        }];
    }
    else
    {
        _partyImage.image=[UIImage imageNamed:@""];
    }
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)CrossCliked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
