//
//  PartyImageVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 29/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartyImageVC : UIViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andData:(NSMutableDictionary *) data;
@end
