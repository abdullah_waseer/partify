//
//  PartyMapVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 28/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>

@interface PartyMapAnnotation : NSObject<MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
	NSInteger index;
}

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mSubTitle;
@property (nonatomic) NSInteger index;

@end


@interface PartyMapVC : UIViewController<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    PartyMapAnnotation *addressAnotation;
    BOOL location;
}

@property (nonatomic,retain) IBOutlet MKMapView *partryMapView;
@end
