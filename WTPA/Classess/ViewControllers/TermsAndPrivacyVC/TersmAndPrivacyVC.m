//
//  TersmAndPrivacyVC.m
//  WTPA
//
//  Created by Sovoia on 03/06/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import "TersmAndPrivacyVC.h"

@interface TersmAndPrivacyVC ()

@property (retain, nonatomic) IBOutlet UIWebView *webVw;

@end

@implementation TersmAndPrivacyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBarHidden = NO;
    [self setUpNavigationBar];
    
    NSLog(@"hint: %@",self.accessibilityHint);
    
    if ([self.accessibilityHint isEqualToString:@"Terms"]) {
        [self loadTermsOrPrivacyWithURLString:@"http://partifyapp.com/terms/"];
    }
    else
        [self loadTermsOrPrivacyWithURLString:@"http://partifyapp.com/privacy/"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_webVw release];
    [super dealloc];
}

-(void) setUpNavigationBar
{
    //    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
    //    self.view.layer.borderWidth=3;
    
    //    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
    //    titleView.image=[UIImage imageNamed:@"logo.png"];
    //    titleView.backgroundColor=[UIColor clearColor];
    //    [self.navigationItem setTitleView:titleView];
    //    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    //    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
    //    UIButton* done= [UIButton buttonWithType:UIButtonTypeCustom];
    //    [done addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    done.frame=CGRectMake(0, 5.0, 40.0, 34.0);
    //    done.backgroundColor=CUSTOM_APP_COLOR;
    //    done.titleLabel.font=[UIFont fontWithName:@"Thonburi" size:16];
    //    done.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    //    [done setTitle:@"Done" forState:UIControlStateNormal];
    //    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:done];
    //    self.navigationItem.rightBarButtonItem = rightBarButton;
    //    [rightBarButton release];
}

- (void)backAction:(id)sender
{
    [self.webVw stopLoading];
    
    if (![self.cameFromPage isEqualToString:@"settings"]) {
        self.navigationController.navigationBarHidden = YES;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadTermsOrPrivacyWithURLString:(NSString*)urlString
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:
                                       [NSURL URLWithString:urlString]
                                                              cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                          timeoutInterval:500.0];
    
    [self.webVw loadRequest:theRequest];
    
}

#pragma mark - webview delegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"The Internet connection appears to be offline." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) webViewDidFinishLoad:(UIWebView*)webView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    self.webVw.scrollView.contentOffset = CGPointMake(0, 150);
}

@end
