//
//  StartVC.h
//  BlueInc
//
//  Created by Ishaq Shafiq on 30/04/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ConfigResponse <NSObject>

-(void) configDownloadedWithResponse;

@end


@interface StartVC : UIViewController
{
    id <ConfigResponse> delegate;
}

@property(nonatomic, retain) id <ConfigResponse> delegate;

@end
