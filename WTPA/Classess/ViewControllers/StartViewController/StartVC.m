//
//  StartVC.m
//  BlueInc
//
//  Created by Ishaq Shafiq on 30/04/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "StartVC.h"
#import "SharedAppManager.h"
#import "MBProgressHUD.h"

@interface StartVC ()

@property (nonatomic,retain) IBOutlet UILabel *errorLabel;
@property (nonatomic,retain) IBOutlet UIImageView *errorImage;

@end



@implementation StartVC
@synthesize delegate;
@synthesize errorLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
   
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
    SharedAppManager* sharedAppManager= [SharedAppManager sharedInstance];
    if([[sharedAppManager.appStatus objectAtIndex:0] isEqualToString:@"Active"])
    {
        [delegate configDownloadedWithResponse];
        
        [self performSelector:@selector(backAction) withObject:nil afterDelay:0.0];
    }
    else
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.errorLabel.hidden=NO;
        self.errorImage.hidden=NO;
        self.errorLabel.text=[sharedAppManager.appStatus objectAtIndex:1];
    }
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    
}

-(void) backAction
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc
{
    [super dealloc];
}


@end
