//
//  ALLEventsVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 30/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>
#import "CalloutMapAnnotation.h"
#import "BasicMapAnnotation.h"
#import "StartVC.h"
 
@interface AddressAnnotation : NSObject<MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
	NSInteger index;
}

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mSubTitle;
@property (nonatomic) NSInteger index;

@end
@interface ALLEventsVC : UIViewController <CLLocationManagerDelegate,NetworkManagerDelegate,ConfigResponse>
{
    CLLocationManager *locationManager;
    AddressAnnotation *addressAnotation;
    NSInteger numberOfPins;
    BOOL call;
    BOOL location;
    BOOL Zoom;
    
    BOOL homeImagesLoaded;
    
    BOOL isLogINCalled;
}

@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;

@property (nonatomic,retain) IBOutlet MKMapView *eventsmapView;
+ (void)changeConfigBool;
+ (void)changeImageLoadedBool;

@end
