//
//  ALLEventsVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 30/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "ALLEventsVC.h"
#import "BasicMapAnnotation.h"
#import "CalloutMapAnnotation.h"
#import "CalloutMapAnnotationView.h"
#import "CUIButton.h"
#import "EventDetailVC.h"
#import "StartVC.h"
#import "LogInVC.h"

#define CALLOUT_BUTTON_TAG  123
#define CALLOUT_TITLE_LABEL_TAG  124
#define CALLOUT_TIME_LABEL_TAG  125
#define CALLOUT_DETAIL_LABEL_TAG  126

@implementation AddressAnnotation

@synthesize coordinate;
@synthesize mTitle, mSubTitle;
@synthesize index;


static NSString* const GMAP_ANNOTATION_SELECTED = @"gMapAnnontationSelected";

- (NSString *)subtitle
{
	return mSubTitle;
}
- (NSString *)title
{
	return mTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTitle:(NSString*)thisTitle withSubTitle:(NSString*)thisSubTitle
{
	if(self = [super init])
	{
		coordinate=c;
		mTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisTitle]];
		mSubTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisSubTitle]];
	}
	return self;
}

- (void)dealloc
{
	[mTitle release];
	[mSubTitle release];
    [super dealloc];
}

@end



@interface ALLEventsVC ()
@property (nonatomic,retain) NSMutableDictionary *latLong;
@property (nonatomic,retain) NSMutableArray *eventsArray;
@property (nonatomic) NSInteger distence;

@property (nonatomic,retain) IBOutlet UIButton *zoomInButton;
@property (nonatomic,retain) IBOutlet UIButton *zoomOUTButton;

@property (nonatomic,retain) IBOutlet UIButton *allEvents;
@property (nonatomic,retain) IBOutlet UIButton *follwoingEvnts;


@property (nonatomic,retain) NetworkManager * networkManager;

@property (nonatomic, retain) CalloutMapAnnotation *calloutAnnotation;
@property (nonatomic, retain) BasicMapAnnotation *customAnnotation;
@property (nonatomic, retain) BasicMapAnnotation *normalAnnotation;

@end

@implementation ALLEventsVC
BOOL configLoaded;
+ (void)changeConfigBool
{
    configLoaded = NO;
}
+ (void)changeImageLoadedBool
{
    
}

-(void) dealloc
{
    [locationManager release];
    [_networkManager release],_networkManager=nil;
    [_zoomInButton release],_zoomInButton=nil;
    [_zoomOUTButton release],_zoomOUTButton=nil;
    [_latLong release],_latLong=nil;
    [_eventsArray release],_eventsArray=nil;
    [_eventsmapView release],_eventsmapView=nil;
    
    [_calloutAnnotation release],_calloutAnnotation=nil;
    [_customAnnotation release],_customAnnotation=nil;
    [_normalAnnotation release],_normalAnnotation=nil;
    
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    call=YES;
    Zoom=YES;
    
    self.zoomInButton.frame=CGRectMake(self.zoomInButton.frame.origin.x,IS_IPHONE_5?self.zoomInButton.frame.origin.y:self.zoomInButton.frame.origin.y-screenDifference, self.zoomInButton.frame.size.width, self.zoomInButton.frame.size.height);
    
    self.zoomOUTButton.frame=CGRectMake(self.zoomOUTButton.frame.origin.x,IS_IPHONE_5?self.zoomOUTButton.frame.origin.y:self.zoomOUTButton.frame.origin.y-screenDifference, self.zoomOUTButton.frame.size.width, self.zoomOUTButton.frame.size.height);
    
    self.zoomInButton.layer.borderColor=[UIColor blackColor].CGColor;
    self.zoomInButton.layer.borderWidth=1;
    
    self.zoomOUTButton.layer.borderColor=[UIColor blackColor].CGColor;
    self.zoomOUTButton.layer.borderWidth=1;
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    _allEvents.selected=NO;
    
    [self populateDistence:3];
    
     [self setUpNavigationBar];
    
       // Do any additional setup after loading the view from its nib.
}

-(void) setLocationMangerDelegate
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
    }
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.leftBarButtonItem=nil;
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA]);
//    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    //    if(data.count>0)
    //    {
    //        [self.userNameButton setTitle:[[data objectForKey:@"username"] capitalizedString] forState:UIControlStateNormal];
    //        if(homeImagesLoaded)
    //            [self loadHomePageDataForUser];
    //    }
    if(!configLoaded)
    {
        StartVC *startVC=[[StartVC alloc] initWithNibName:@"StartVC" bundle:nil];
        startVC.delegate=self;
        startVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:startVC animated:NO];
        [startVC release];
    }
    else
    {
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        if(data.count>0)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:USER_AUTHENTICATE])
            {
//                [self setLocationMangerDelegate];
            }
            else
            {
                [self authenticateUser];
            }
        }
        else
        {
            [SharedAppManager sharedInstance].addUser = NO;
            if(!isLogINCalled)
                [self performSelector:@selector(showCreateUserController) withObject:nil afterDelay:0.1];
        }
        
    }
    isLogINCalled=NO;
}

-(void) showCreateUserController
{
    if([SharedAppManager sharedInstance].addUser)
    {
        //        CreateUserVC *createUserVC=[[CreateUserVC alloc] initWithNibName:@"CreateUserVC" bundle:nil];
        //        createUserVC.hidesBottomBarWhenPushed=YES;
        //        [self.navigationController pushViewController:createUserVC animated:YES];
        //        //[SharedAppManager sharedInstance].addUser = NO;
        //        [createUserVC release];
        LogInVC *logInVC=[[LogInVC alloc] initWithNibName:@"LogInVC" bundle:nil];
        logInVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:logInVC animated:YES];
        [logInVC release];
    }
    else
    {
        LogInVC *logInVC=[[LogInVC alloc] initWithNibName:@"LogInVC" bundle:nil];
        logInVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:logInVC animated:YES];
//        [logInVC release];
    }
    
}

-(void) authenticateUser
{
    if(call)
    {
        call=NO;
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/authenticate",[SharedAppManager sharedInstance].baseURL];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
        [parameters setObject:jsonString forKey:@"param"];
        
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:[data objectForKey:@"username"] forKey:@"USERNAME"];
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PASSWORD] forKey:@"PASSWORD"];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"authenticateUser" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [_networkManager sendRequestforAction:parameters];
    }
}

#pragma mark - IBActions Methods Statrt

-(IBAction) allEventsClicked:(id) sender
{
    _allEvents.selected=!_allEvents.selected;
    [self releaseArrayAndRemovePin];
    
    if(_allEvents.selected)
        [self loadFollowingEventsByLatLong];
    else
        [self loadEventsAccordingLatLong];
       
}

-(IBAction) follwingEventsClicked:(id) sender
{
     _allEvents.selected=NO;
    
    [self releaseArrayAndRemovePin];
    [self loadFollowingEventsByLatLong];
}

-(IBAction) zoomInClicked:(id) sender
{
    if(self.distence>=3)
    {
        Zoom=YES;
        [self populateDistence:self.distence+3];
        [self setRegionForZooming:_eventsmapView.region];
//        [self loadEventsAccordingLatLong];
    }
}

-(IBAction) zoomOutClicked:(id) sender
{
    if(self.distence>3)
    {
        Zoom=YES;
        [self populateDistence:self.distence-3];
        [self setRegionForZooming:_eventsmapView.region];
//        [self loadEventsAccordingLatLong];
    }
}

#pragma mark - Custom Methods Statrt

-(void) releaseArrayAndRemovePin
{
    if(_eventsArray)
    {
        [_eventsArray release],_eventsArray=nil;
    }
    for (id<MKAnnotation> annotation in _eventsmapView.annotations)
    {
        [_eventsmapView removeAnnotation:annotation];
    }
}
-(void) populateDistence:(int) dis
{
    self.distence=dis;
}
-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    locationManager.delegate=nil;
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) drawMultiPleEventsPins
{
	MKCoordinateRegion region;
    
    NSMutableArray *drawableArray=[NSMutableArray array];
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"EEE, dd MM yyyy HH:mm:ss"];
    [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSDateFormatter *df2 = [[NSDateFormatter alloc]init];
    [df2 setDateFormat:@"dd MM yyyy"];
    [df2 setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    for(int i=0; i<_eventsArray.count; i++)
    {
        NSDate *eventDate = [df dateFromString:[[_eventsArray objectAtIndex:i] objectForKey:@"startDate"]];
//        NSLog(@"date: %@ ... %@",[df2 stringFromDate:eventDate],[df2 stringFromDate:[NSDate date]]);
        
        if ([[df2 stringFromDate:eventDate] isEqual:[df2 stringFromDate:[NSDate date]]])
        {
            NSDictionary *geoDict =[[[_eventsArray objectAtIndex:i] objectForKey:@"venue"] objectForKey:@"location"];
            NSLog(@"%@",geoDict);
            
            BOOL addPin=YES;
            for (id<MKAnnotation> annotation in _eventsmapView.annotations)
            {
                NSString *lattiude=[NSString stringWithFormat:@"%0.6f",[[geoDict objectForKey:@"lat"] floatValue]];
                NSString *pinlattiude=[NSString stringWithFormat:@"%0.6f",annotation.coordinate.latitude];
                NSString *longitude=[NSString stringWithFormat:@"%0.6f",[[geoDict objectForKey:@"long"] floatValue]];
                NSString *pinlongitude=[NSString stringWithFormat:@"%0.6f",annotation.coordinate.longitude];
                
                if([lattiude floatValue]==[pinlattiude floatValue] && [longitude floatValue]==[pinlongitude floatValue])
                {
                    addPin=NO;
                    break;
                }
            }
            if(addPin)
            {
                [[_eventsArray objectAtIndex:i] setValue:[NSString stringWithFormat:@"%d",i] forKey:@"indexPin"];
                [drawableArray addObject:[_eventsArray objectAtIndex:i]];
            }
        }
    }
    
    for(int i=0;i<drawableArray.count;i++)
    {
         NSDictionary *geoDict =[[[drawableArray objectAtIndex:i] objectForKey:@"venue"] objectForKey:@"location"];
        if(i==0)
        {
            if(Zoom)
            {
                region.center.latitude = [[geoDict objectForKey:@"lat"] floatValue];
                region.center.longitude = [[geoDict objectForKey:@"long"] floatValue];;
                [self setRegionForZooming:region];
                Zoom=NO;
            }
        }
        
        CLLocationCoordinate2D cord ;
        cord.latitude=[[geoDict objectForKey:@"lat"] floatValue];
        cord.longitude=[[geoDict objectForKey:@"long"] floatValue];
        
        addressAnotation = [[AddressAnnotation alloc] initWithCoordinate:cord withTitle:drawableArray[i][@"title"] withSubTitle:drawableArray[i][@"startDate"]];
        addressAnotation.index=[drawableArray[i][@"indexPin"] integerValue];
        
        [_eventsmapView addAnnotation:addressAnotation];
        [addressAnotation  release],addressAnotation=nil;
    }
}
-(void) setRegionForZooming:(MKCoordinateRegion) region
{
    MKCoordinateSpan span;
	span.latitudeDelta=0.2*(self.distence/3);
	span.longitudeDelta=0.2*(self.distence/3);
	region.span = span;
    [_eventsmapView setRegion:region animated:false];
    [_eventsmapView regionThatFits:region];
    Zoom=NO;

}

-(void) callOutClicked:(id) sender
{
    CUIButton *button=(CUIButton *)sender;
    NSMutableDictionary *eventData=[NSMutableDictionary dictionaryWithDictionary:[_eventsArray objectAtIndex:button.index]];
    
    EventDetailVC *eventDetailVC=[[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventData];
    eventDetailVC.venueId = [[[_eventsArray objectAtIndex:button.index] objectForKey:@"venue"] objectForKey:@"venu_id"];
    eventDetailVC.eventSource = [[_eventsArray objectAtIndex:button.index] objectForKey:@"source"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
//    [eventDetailVC release];
}


-(void) loadEventsAccordingLatLong
{
    if(call)
    {
        call=NO;
        NSString* urlString = [NSString stringWithFormat:@"%@api/events/byLatLong",[SharedAppManager sharedInstance].baseURL];
        
        NSMutableDictionary *param=[NSMutableDictionary dictionary];
        [param setObject:[_latLong objectForKey:@"long"] forKey:@"long"];
        [param setObject:[_latLong objectForKey:@"lat"] forKey:@"lat"];
        [param setObject:@"" forKey:@"event_time"];
        [param setObject:@"" forKey:@"event_name"];
        [param setObject:@"" forKey:@"login_id"];
        [param setObject:@"" forKey:@"event_description"];
        [param setObject:@"" forKey:@"venu_id"];
        [param setObject:[NSString stringWithFormat:@"%ld",(long)self.distence] forKey:@"distance"];
        [param setObject:@"" forKey:@"page"];
        [param setObject:@"" forKey:@"tag"];
        [param setObject:@"" forKey:@"festivalsonly"];
        
        [param setObject:@"" forKey:@"location"];
        
        [param setObject:[NSString stringWithFormat:@"%d",100] forKey:@"limit"];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:param];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];

        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:@"application/json" forKey:@"Content-type"];
        [dictionary setObject:jsonString forKey:@"param"];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"bylatlong" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
}


-(void) loadFollowingEventsByLatLong
{
    if(call)
    {
        call=NO;
        NSString* urlString = [NSString stringWithFormat:@"%@api/events/searchEventsFollowedByUser",[SharedAppManager sharedInstance].baseURL];
        
        NSMutableDictionary *param=[NSMutableDictionary dictionary];
        [param setObject:[NSString stringWithFormat:@"%@",[_latLong objectForKey:@"long"]] forKey:@"long"];
        [param setObject:[NSString stringWithFormat:@"%@",[_latLong objectForKey:@"lat"]] forKey:@"lat"];
        [param setObject:[NSString stringWithFormat:@"%d",300] forKey:@"distance"];
        [param setObject:@"" forKey:@"location"];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:param];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:@"application/json" forKey:@"Content-type"];
        [dictionary setObject:jsonString forKey:@"param"];
        
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"bylatlong" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
}

#pragma mark - Delegates Statrt

#pragma mark - NetworkMangerDelegate Delegate Methods

#pragma mark - ConfigDownloaded Delegate Methods

-(void) configDownloadedWithResponse
{
    configLoaded=YES;
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN])
    {
        isLogINCalled=YES;
        [self performSelector:@selector(showCreateUserController) withObject:nil afterDelay:0.1];
    }
    else
    {
        [self performSelector:@selector(authenticateUser) withObject:nil afterDelay:0.1];
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"authenticateUser"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_AUTHENTICATE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self setLocationMangerDelegate];
        }
        else if([networkManager.identifier isEqualToString:@"bylatlong"])
        {
            if([[object objectForKey:@"data"]  isKindOfClass:[NSArray class]])
            {
                if([self.eventsArray count]>0)
                {
                    NSMutableArray *newPinsArray=[NSMutableArray array];
                    NSArray *tempArray=[[NSArray alloc] initWithArray:[object objectForKey:@"data"]];
                    for(int i=0;i<tempArray.count;i++)
                    {
                        BOOL found=NO;
                        NSDictionary *newgeoDict =[[[tempArray objectAtIndex:i] objectForKey:@"venue"] objectForKey:@"location"];
                        for(int j=0;j<_eventsArray.count;j++)
                        {
                            NSDictionary *localgeoDict =[[[_eventsArray objectAtIndex:j] objectForKey:@"venue"] objectForKey:@"location"];
                            
                            if([[newgeoDict objectForKey:@"lat"] floatValue] == [[localgeoDict objectForKey:@"lat"] floatValue] && [[newgeoDict objectForKey:@"long"] floatValue] == [[localgeoDict objectForKey:@"long"] floatValue] )
                            {
                                found=YES;
                                break;
                            }
                        }
                        if(!found)
                        {
                            [newPinsArray addObject:[tempArray objectAtIndex:i]];
                        }
                    }
                    if(newPinsArray.count>0)
                    {
                        [_eventsArray addObjectsFromArray:newPinsArray];
                    }
                }
                else
                {
                    self.eventsArray=[[NSMutableArray alloc] initWithArray:[object objectForKey:@"data"]];
                }
                if(_eventsArray.count>0)
                {
                    [self drawMultiPleEventsPins];
                }
                else
                {
                    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No Event Found"];
                }
            }
        }
        else
        {
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No Event Found"];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
    
    call=YES;
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No Event Found"];
}

#pragma mark - Location Manager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(!location)
    {
        location=YES;
        self.latLong=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude],@"long",nil];
        [locationManager stopUpdatingLocation];
        
        if(_allEvents.selected)
            [self loadFollowingEventsByLatLong];
        else
            [self loadEventsAccordingLatLong];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"
                                                    message:@"Please check and enable your location services" delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

#pragma mark - MapView Delegate Methods
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"");
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[AddressAnnotation class]])
    {
        AddressAnnotation *basic=(AddressAnnotation *)annotation;
        
		MKPinAnnotationView *annotationView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                                               reuseIdentifier:@"CustomAnnotation"] autorelease];
		annotationView.canShowCallout = YES;
        annotationView.calloutOffset=CGPointMake(-5, 5);
//		annotationView.image=[UIImage imageNamed:@"pin.png"];
        annotationView.animatesDrop=YES;
        
        CUIButton *button=[CUIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(callOutClicked:) forControlEvents:UIControlEventTouchDown];
        button.tag=CALLOUT_BUTTON_TAG;
        button.frame=button.frame=CGRectMake(0, 0,35, 35);
        button.backgroundColor=[UIColor clearColor];
        button.index=basic.index;
        annotationView.rightCalloutAccessoryView=button;
        [button setImage:[UIImage imageNamed:@"next-page-arrow.png"] forState:UIControlStateNormal];
    
		return annotationView;
    }
//	}
	return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
        location=NO;
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    location=YES;
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
//    CLLocationCoordinate2D centerCoordinate=mapView.centerCoordinate;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if(location)
    {
        CLLocationCoordinate2D centerCoordinate=mapView.centerCoordinate;
        if(self.latLong)
            [self.latLong release],self.latLong=nil;
        
        self.latLong=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",centerCoordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",centerCoordinate.longitude],@"long",nil];
        
        if(_allEvents.selected)
            [self loadFollowingEventsByLatLong];
        else
            [self loadEventsAccordingLatLong];    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
