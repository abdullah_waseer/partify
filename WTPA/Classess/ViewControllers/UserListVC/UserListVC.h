//
//  UserListVC.h
//  WTPA
//
//  Created by Admin on 26/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserListVC : UIViewController
@property (retain, nonatomic) IBOutlet UITableView *table;
- (IBAction)addBtnPressed:(id)sender;

@end
