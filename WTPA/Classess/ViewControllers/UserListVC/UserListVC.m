//
//  UserListVC.m
//  WTPA
//
//  Created by Admin on 26/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "UserListVC.h"
#import "ALLEventsVC.h"
#import "UserProfileVC.h"

@interface UserListVC ()

@end

@implementation UserListVC

NSMutableArray * prodArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *plistPath = [directoryPath objectAtIndex:0];
    plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
    prodArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
    for(int i=0;i<prodArray.count;i++)
        NSLog(@"%@",[prodArray objectAtIndex:i]);
    // Do any additional setup after loading the view from its nib.
}

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, 240, 44)];
    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
    titleView.textColor = [UIColor whiteColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0] valueForKey:@"username"];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [prodArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    if(indexPath.row == 0)
        cell.backgroundColor = [UIColor greenColor];
    cell.textLabel.font = [UIFont fontWithName:@"Thonburi" size:20];
    [cell.textLabel setText:[[prodArray objectAtIndex:indexPath.row] valueForKey:@"username"]];
    //[cell.detailTextLabel setText:[[self.jobsData objectAtIndex:indexPath.row] valueForKey:@"date"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:[prodArray objectAtIndex:indexPath.row], nil]  forKey:USER_DATA];
    [[NSUserDefaults standardUserDefaults] setObject:[[prodArray objectAtIndex:indexPath.row] valueForKey:@"password"] forKey:PASSWORD];
    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *plistPath = [directoryPath objectAtIndex:0];
    plistPath = [plistPath stringByAppendingPathComponent:@"accounts.plist"];
    NSMutableArray * userArray = [[NSMutableArray alloc]initWithContentsOfFile:plistPath];
    [userArray exchangeObjectAtIndex:indexPath.row withObjectAtIndex:0];
    [userArray writeToFile:plistPath atomically:YES];
    [[self.tabBarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:NO];
    [ALLEventsVC changeImageLoadedBool];
    UserProfileVC *userProfileVC = (UserProfileVC *)[[[self.tabBarController.viewControllers objectAtIndex:3]viewControllers] objectAtIndex:0];
    [userProfileVC resetAllProperties];
    [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:NO];
    [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:NO];
    self.tabBarController.selectedIndex = 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


- (void)dealloc {
    [_table release];
    [super dealloc];
}
- (IBAction)addBtnPressed:(id)sender
{
    UserProfileVC *userProfileVC = (UserProfileVC *)[[[self.tabBarController.viewControllers objectAtIndex:3]viewControllers] objectAtIndex:0];
//    [[self.tabBarController.viewControllers objectAtIndex:2]popToRootViewControllerAnimated:NO];
//    [[self.tabBarController.viewControllers objectAtIndex:3]popToRootViewControllerAnimated:NO];
    [userProfileVC resetAllProperties];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [SharedAppManager sharedInstance].addUser = YES;
    [ALLEventsVC changeConfigBool];
    [[self.tabBarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:YES];
    self.tabBarController.selectedIndex = 0;
}
@end
