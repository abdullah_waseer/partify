//
//  UserFeedCustomCellVC2.h
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFeedCustomCellVC2 : UITableViewCell

@property (nonatomic , retain) IBOutlet UIButton * userCreated;
@property (nonatomic , retain) IBOutlet UIButton * venueAt;
@property (nonatomic , retain) IBOutlet UIButton * eventCreated;
@property (nonatomic , retain) IBOutlet UILabel * created;
@property (nonatomic , retain) IBOutlet UILabel * at;

@end
