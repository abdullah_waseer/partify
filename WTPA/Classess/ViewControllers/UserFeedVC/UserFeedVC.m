//
//  UserFeedVC.m
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "UserFeedVC.h"
#import "UserFeedCustomCellVC.h"
#import "UserFeedCustomCellVC1.h"
#import "UserFeedCustomCellVC2.h"
#import "UserFeedCustomCellVC3.h"
#import "UIImageView+WebCache.h"
#import "NSString+SBJSON.h"
#import "UserProfileVC.h"
#import "EventDetailVC.h"
#import "VenueDetailVC.h"
#import "AppUtils.h"
#import "NSDate-Utilities.h"
#import "TaggedView.h"
#import "FeedbackVC.h"

@interface UserFeedVC ()<NetworkManagerDelegate,TaggedViewDelegate>

@property (nonatomic , retain) NSMutableArray *feedData;

@end

@implementation UserFeedVC

NetworkManager * networkManager;
#pragma mark - View Init
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    [self makeUserFeedCall];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
#pragma mark - Custom Methods

-(void) setUpNavigationBar
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 157, 27)];
    titleView.font = [UIFont fontWithName:@"Thonburi" size:28.0];
    titleView.textColor = [UIColor whiteColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text =[[data objectForKey:@"username" ] uppercaseString];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)bringFeedbackVC:(UIButton*)senderBtn
{
    FeedbackVC *feedbackVC = [[FeedbackVC alloc]initWithNibName:@"FeedbackVC" bundle:nil];
    feedbackVC.feedbackIssueId = senderBtn.accessibilityIdentifier;
    feedbackVC.issueAbout = @"photo";
    [self.navigationController pushViewController:feedbackVC animated:YES];
}

-(void)makeUserFeedCall
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/followerfeed/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
//    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",@"300",@"limit",jsonString,@"param",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getUserFeed" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}
#pragma mark - IBactions

- (IBAction)venueDetailBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}

- (IBAction)userTagedAttendingDetailBtnPressed:(UIButton *)sender
{
     NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[data objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}
- (IBAction)userTagedPoopUPBtnPressed:(UIButton *)sender
{
    TaggedView *tagedView=[[TaggedView alloc] initWithFrame:CGRectMake(35, IS_IPHONE_5?134:90, 250, 300) withArray:[[[self.feedData objectAtIndex:[sender tag]] objectForKey:@"data"] objectForKey:@"taged_user_detail"]];
    tagedView.delegate=self;
    [self.view addSubview:tagedView];
    
}


- (IBAction)userAttendingDetailBtnPressed:(UIButton *)sender
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"user_detail"]objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}

- (IBAction)eventAttendingBtnPressed:(UIButton *)sender
{
    NSMutableDictionary * eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"event_detail"]objectForKey:@"id"],@"id",@"YES",@"type",@"DB",@"source",nil];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventDetail];
//    eventDetailVC.venueId = [[[eventDetail objectAtIndex:button.index] objectForKey:@"venue"] objectForKey:@"venu_id"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetail release];
}
- (IBAction)venueAttendingBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}

- (IBAction)userCreatedBtnPressed:(UIButton *)sender
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"user_detail"]objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}

- (IBAction)eventCreatedBtnPressed:(UIButton *)sender
{
    NSMutableDictionary * eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"YES",@"type",@"DB",@"source",[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"event_detail"]objectForKey:@"id"],@"id",nil];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventDetail];
//    eventDetailVC.venueId = [[[_eventsArray objectAtIndex:button.index] objectForKey:@"venue"] objectForKey:@"venu_id"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetail release];
}

- (IBAction)venueCreatedBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}
#pragma mark - Delegates
#pragma mark - Table Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    int padding = 5;
    if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"image_upload"])
    {
        UserFeedCustomCellVC *cell = (UserFeedCustomCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC"] owner:self options:nil];
        
            for(id currentObject in objects)
            {
            
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC *)currentObject;
                    [cell.feedbackBtn addTarget:self action:@selector(bringFeedbackVC:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                }
            }
        }
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"image_url"]];
        [cell.image setImageWithURL:[NSURL URLWithString:url]];
        cell.image.clipsToBounds=YES;

        cell.feedbackBtn.accessibilityIdentifier = [[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"id"];
        
        //    cell.venueBtn.frame = CGRectMake(cell.venueBtn.frame.origin.x, cell.venueBtn.frame.origin.y, expectedSize.width+padding+20, 20);
        [cell.venueBtn addTarget:self action:@selector(venueDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        [cell.venueBtn setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        /*[cell.image setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:nil options:nil
                            success:^(UIImage *image, BOOL cached) {
                                
                                
                                UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:305.0].CGImage];
                                
                                cell.feedImage.image = scaledImage;
                                cell.feedImage.clipsToBounds = YES;
                                [scaledImage release];
                            }
                            failure:^(NSError *error) {
                                
                            }];*/
        cell.venueBtn.tag = indexPath.row;

        NSDate * actDate = [AppUtils getDateFromString:[[self.feedData objectAtIndex:indexPath.row]objectForKey:@"activity_time"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
        [cell.timeBtn setTitle:[AppUtils timeFormatted:[NSDate date] toDate:actDate] forState:UIControlStateNormal];
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"event_follow"])
    {
        
        UserFeedCustomCellVC1 *cell = (UserFeedCustomCellVC1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC1"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC1 *)currentObject;
                    break;
                }
            }
        }
    
        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.userName.frame = CGRectMake(cell.userName.frame.origin.x, cell.userName.frame.origin.y, expectedSize.width+padding+5, 30);
        [cell.userName addTarget:self action:@selector(userAttendingDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.attending.frame = CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.attending.frame.origin.y, cell.attending.frame.size.width, cell.attending.frame.size.height);
        cell.eventName.frame = CGRectMake(cell.attending.frame.origin.x+cell.attending.frame.size.width, cell.eventName.frame.origin.y, expectedSize.width+padding+5, 30);
        
        [cell.eventName addTarget:self action:@selector(eventAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.at.frame = CGRectMake(cell.eventName.frame.origin.x+cell.eventName.frame.size.width, cell.at.frame.origin.y, cell.at.frame.size.width, cell.at.frame.size.height);
        cell.venueName.frame = CGRectMake(cell.at.frame.origin.x+cell.at.frame.size.width, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
        
        [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        [cell.userName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] forState:UIControlStateNormal];
        [cell.eventName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        cell.userName.tag = indexPath.row;
        cell.eventName.tag = indexPath.row;
        cell.venueName.tag = indexPath.row;
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"event_creation"])
    {
        UserFeedCustomCellVC2 *cell = (UserFeedCustomCellVC2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC2"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC2 *)currentObject;
                    break;
                }
            }
        }

        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.userCreated.frame = CGRectMake(cell.userCreated.frame.origin.x, cell.userCreated.frame.origin.y, expectedSize.width+padding, 30);
        
        [cell.userCreated addTarget:self action:@selector(userCreatedBtnPressed:) forControlEvents:UIControlEventTouchDown];
        cell.created.frame = CGRectMake(cell.userCreated.frame.origin.x+cell.userCreated.frame.size.width, cell.created.frame.origin.y, cell.created.frame.size.width, 30);
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.eventCreated.frame = CGRectMake(cell.created.frame.origin.x+cell.created.frame.size.width, cell.eventCreated.frame.origin.y, expectedSize.width+padding+10, 30);
        [cell.eventCreated addTarget:self action:@selector(eventAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.at.frame = CGRectMake(cell.eventCreated.frame.origin.x+cell.eventCreated.frame.size.width, cell.at.frame.origin.y, cell.at.frame.size.width, 30);
        cell.venueAt.frame = CGRectMake(cell.at.frame.origin.x+cell.at.frame.size.width, cell.venueAt.frame.origin.y, expectedSize.width+padding+10, 30);
        
        
        [cell.userCreated setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] forState:UIControlStateNormal];
        [cell.eventCreated setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] forState:UIControlStateNormal];
        [cell.venueAt setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        [cell.venueAt addTarget:self action:@selector(venueCreatedBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        cell.userCreated.tag = indexPath.row;
        cell.eventCreated.tag = indexPath.row;
        cell.venueAt.tag = indexPath.row;
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"image_taged"])
    {
        
        UserFeedCustomCellVC3 *cell = (UserFeedCustomCellVC3 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC3"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC3 *)currentObject;
                    [cell.feedbackBtn addTarget:self action:@selector(bringFeedbackVC:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                }
            }
        }
        
        
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"image_url"]];
        [cell.image setImageWithURL:[NSURL URLWithString:url]];
        cell.image.clipsToBounds=YES;

        cell.feedbackBtn.accessibilityIdentifier = [[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"id"];
        
        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        
        expectedSize = [[data objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        
        cell.userName.frame = CGRectMake(cell.userName.frame.origin.x, cell.userName.frame.origin.y, expectedSize.width+padding+5, 30);
        [cell.userName addTarget:self action:@selector(userTagedAttendingDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        [cell.userName setTitle:[data objectForKey:@"username"] forState:UIControlStateNormal];
        
        NSArray *array=[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"taged_user_detail"];
        
        if(array.count>1)
        {
            cell.andLabel.hidden=NO;
            cell.multipleTag.hidden=NO;
            cell.andLabel.frame=CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.andLabel.frame.origin.y, cell.andLabel.frame.size.width, cell.andLabel.frame.size.height);
        
            expectedSize = [[NSString stringWithFormat:@"%i other%@",array.count-1,array.count-1==1?@"":@"s"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.multipleTag.frame=CGRectMake(cell.andLabel.frame.origin.x+cell.andLabel.frame.size.width, cell.multipleTag.frame.origin.y, expectedSize.width+10, 30);
            [cell.multipleTag addTarget:self action:@selector(userTagedPoopUPBtnPressed:) forControlEvents:UIControlEventTouchDown];
            [cell.multipleTag setTitle:[NSString stringWithFormat:@"%i other%@",array.count-1,array.count-1==1?@"":@"s"] forState:UIControlStateNormal];
            
            cell.wasAtLabel.frame = CGRectMake(cell.multipleTag.frame.origin.x+cell.multipleTag.frame.size.width, cell.wasAtLabel.frame.origin.y, cell.wasAtLabel.frame.size.width, cell.wasAtLabel.frame.size.height);
            
            expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.venueName.frame = CGRectMake(cell.wasAtLabel.frame.origin.x+cell.wasAtLabel.frame.size.width+5, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
            
            [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
            
            [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
            
        }
        else
        {
            cell.andLabel.hidden=YES;
            cell.multipleTag.hidden=YES;
            
            cell.wasAtLabel.text=@"was at";
            
            cell.wasAtLabel.frame = CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.wasAtLabel.frame.origin.y, cell.wasAtLabel.frame.size.width, cell.wasAtLabel.frame.size.height);
            
            expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.venueName.frame = CGRectMake(cell.wasAtLabel.frame.origin.x+cell.wasAtLabel.frame.size.width+5, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
            
            [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
            
            [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        
        }
        
        cell.multipleTag.tag=indexPath.row;
        cell.userName.tag = indexPath.row;
        cell.venueName.tag = indexPath.row;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"]isEqualToString:@"image_upload"] || [[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"]isEqualToString:@"image_taged"])
        return 151;
    else
        return 44;
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"getUserFeed"])
    {
        self.feedData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
        [self.feedTable reloadData];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

# pragma mark - TaggedView Delegates

- (void)TagedViewcrossTapped:(TaggedView *)view
{
    [view removeFromSuperview];
}

- (void)TagedView:(TaggedView *)view withTaggedDictionary:(NSMutableDictionary *) tagged
{
    
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[tagged objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
    
    [view removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - View Dealloc
- (void)dealloc {
    [_feedTable release];
    [super dealloc];
}

@end
