//
//  UserFeedCustomCellVC1.h
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFeedCustomCellVC1 : UITableViewCell

@property (nonatomic , retain) IBOutlet UIButton * userName;
@property (nonatomic , retain) IBOutlet UIButton * eventName;
@property (nonatomic , retain) IBOutlet UIButton * venueName;
@property (nonatomic , retain) IBOutlet UILabel * attending;
@property (nonatomic , retain) IBOutlet UILabel * at;

@end
