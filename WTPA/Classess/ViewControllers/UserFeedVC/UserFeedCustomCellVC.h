//
//  UserFeedCustomCellVC.h
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFeedCustomCellVC : UITableViewCell

@property (nonatomic , retain) IBOutlet UIImageView * image;
@property (nonatomic , retain) IBOutlet UIButton * venueBtn;
@property (nonatomic , retain) IBOutlet UIButton * timeBtn;

@property (retain, nonatomic) IBOutlet UIButton *feedbackBtn;

@end
