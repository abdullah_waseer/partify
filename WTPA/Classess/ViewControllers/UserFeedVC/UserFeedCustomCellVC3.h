//
//  UserFeedCustomCellVC.h
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFeedCustomCellVC3 : UITableViewCell

@property (nonatomic , retain) IBOutlet UIImageView * image;
@property (nonatomic , retain) IBOutlet UIButton * userName;
@property (nonatomic , retain) IBOutlet UIButton * venueName;
@property (nonatomic , retain) IBOutlet UIButton * multipleTag;
@property (nonatomic , retain) IBOutlet UILabel * andLabel;
@property (nonatomic , retain) IBOutlet UILabel * wasAtLabel;

@property (retain, nonatomic) IBOutlet UIButton *feedbackBtn;
@end
