//
//  UserFeedVC.h
//  WTPA
//
//  Created by Admin on 17/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFeedVC : UIViewController
@property (retain, nonatomic) IBOutlet UITableView *feedTable;
- (IBAction)venueDetailBtnPressed:(UIButton *)sender;
- (IBAction)userAttendingDetailBtnPressed:(UIButton *)sender;
- (IBAction)eventAttendingBtnPressed:(UIButton *)sender;
- (IBAction)venueAttendingBtnPressed:(UIButton *)sender;
- (IBAction)userCreatedBtnPressed:(UIButton *)sender;
- (IBAction)eventCreatedBtnPressed:(UIButton *)sender;
- (IBAction)venueCreatedBtnPressed:(UIButton *)sender;

@end
