//
//  MoreVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 04/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEventVC : UIViewController
- (IBAction)editClicked:(id)sender;
- (IBAction)deleteClicked:(id)sender;

@end
