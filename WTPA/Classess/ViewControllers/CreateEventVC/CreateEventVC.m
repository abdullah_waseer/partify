//
//  MoreVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 04/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "CreateEventVC.h"
#import "CreateEventFormVC.h"
#import "SelectCategoryPopupView.h"
#import "CreateEventCell.h"
#import "EventDetailVC.h"


@interface CreateEventVC ()<NetworkManagerDelegate>
{
   
}


@property (nonatomic,retain) NSMutableArray *userEventsArray;
@property (nonatomic,retain) NSMutableArray *inEventsArray;
@property (nonatomic,retain) IBOutlet UITableView *userEvntsTable;
@property (nonatomic,retain) IBOutlet UILabel *noEventLabel;
@property (nonatomic,retain) IBOutlet UIButton *test;
@property (nonatomic)  NSInteger deleteIndex;


@property (nonatomic,retain) NetworkManager * networkManager;


@end

@implementation CreateEventVC

-(void) dealloc
{
    [_networkManager release],_networkManager=nil;
    [_userEventsArray release],_userEventsArray=nil;
    [_userEvntsTable release],_userEvntsTable=nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.noEventLabel.hidden=YES;
    _userEvntsTable.hidden=YES;
//    _test.layer.borderColor=[UIColor whiteColor].CGColor;
//    _test.layer.borderWidth=1;
//    _test.layer.cornerRadius=5;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setUpNavigationBar];
    
    self.userEvntsTable.frame=CGRectMake(0, 103, 320,IS_IPHONE_5?365:365-screenDifference);
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshEvents"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) viewWillAppear:(BOOL)animated
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"refreshEvents"])
    {
        [self performSelector:@selector(loadEventsByUser) withObject:nil afterDelay:0.00];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"refreshEvents"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - Custom methods Started

-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

-(void) loadEventsByUser
{
    NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/events/userevents/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    [dictionary setObject:jsonString forKey:@"param"];
    
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"ownevents" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

#pragma mark - IBActions Started

- (IBAction)editClicked:(id)sender
{
    CreateEventFormVC *createEventFormVC=[[CreateEventFormVC alloc] initWithNibName:@"CreateEventFormVC" bundle:nil andDictionary:_userEventsArray[[sender tag]]];
    [self.navigationController pushViewController:createEventFormVC animated:YES];
    [createEventFormVC release];
}

- (IBAction)deleteClicked:(id)sender
{
    self.deleteIndex=[sender tag];
    NSString* urlString;
    if([sender tag] >= 0)
    {
       
        urlString = [NSString stringWithFormat:@"%@api/events/by/%@",[SharedAppManager sharedInstance].baseURL,_userEventsArray[[sender tag]][@"id"]];
    }
    else
    {
         NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
        urlString = [NSString stringWithFormat:@"%@api/users/unfollowevent/%@/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"],_inEventsArray[([sender tag] * -1) - 1][@"id"]];
    }
    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    
    
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"DELETE" forKey:@"callType"];
    [parameters setObject:@"deleteevent" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

-(IBAction)promoteNewEventClicked:(id)sender
{
    CreateEventFormVC *createEventFormVC=[[CreateEventFormVC alloc] initWithNibName:@"CreateEventFormVC" bundle:nil];
    [self.navigationController pushViewController:createEventFormVC animated:YES];
    [createEventFormVC release];
    
}


#pragma mark - Delegates Statrt

#pragma mark - UITableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return _userEventsArray.count;
    else
        return _inEventsArray.count;
}

//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//}

-(CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 44;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
}


//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//
//
//    NSMutableArray *tempArray = [[[NSMutableArray alloc] init] autorelease];
//    [tempArray addObject:@"A"];
//    [tempArray addObject:@"B"];
//    [tempArray addObject:@"C"];
//    [tempArray addObject:@"D"];
//    [tempArray addObject:@"E"];
//    [tempArray addObject:@"F"];
//    [tempArray addObject:@"G"];
//    [tempArray addObject:@"H"];
//    [tempArray addObject:@"I"];
//    [tempArray addObject:@"J"];
//    [tempArray addObject:@"K"];
//    [tempArray addObject:@"L"];
//    [tempArray addObject:@"M"];
//    [tempArray addObject:@"N"];
//    [tempArray addObject:@"O"];
//    [tempArray addObject:@"P"];
//    [tempArray addObject:@"Q"];
//    [tempArray addObject:@"R"];
//    [tempArray addObject:@"S"];
//    [tempArray addObject:@"T"];
//    [tempArray addObject:@"U"];
//    [tempArray addObject:@"V"];
//    [tempArray addObject:@"W"];
//    [tempArray addObject:@"X"];
//    [tempArray addObject:@"Y"];
//    [tempArray addObject:@"Z"];
//
//    return tempArray;
//}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *HeaderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, _userEvntsTable.frame.size.width, 44)];
    HeaderView.backgroundColor=[UIColor blackColor];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, _userEvntsTable.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Thonburi" size:20.0];
    //label.textAlignment = NSTextAlignmentCenter;
    if(section == 0)
        label.text = @"Posted Events";
    else
        label.text = @"Attending Events";
    [HeaderView addSubview:label];
    return HeaderView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    CreateEventCell *cell = (CreateEventCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"CreateEventCell"] owner:self options:nil];
        
        for(id currentObject in objects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (CreateEventCell *)currentObject;
                break;
            }
        }
    }
    if(indexPath.section == 0)
    {
        cell.titleLabel.text=[[NSString stringWithFormat:@"%@",[[_userEventsArray objectAtIndex:indexPath.row] objectForKey:@"event_name"]] capitalizedString];
        cell.editButton.tag=indexPath.row;
        cell.deleteButton.tag=indexPath.row;
        
        cell.accessibilityIdentifier = [[_userEventsArray objectAtIndex:indexPath.row] objectForKey:@"venue_id"];
    }
    if(indexPath.section == 1)
    {
        cell.titleLabel.text=[[NSString stringWithFormat:@"%@",[[_inEventsArray objectAtIndex:indexPath.row] objectForKey:@"event_name"]] capitalizedString];
        cell.editButton.hidden = YES;
        cell.deleteButton.tag=(indexPath.row+1) * -1;
        
        cell.accessibilityIdentifier = [[_inEventsArray objectAtIndex:indexPath.row] objectForKey:@"venue_id"];
    }
    
    
    // Configure the cell...
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.font=[UIFont fontWithName:@"" size:15];
    
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}


#pragma mark UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(indexPath.section == 1)
        [[_userEventsArray objectAtIndex:indexPath.row] setObject:@"YES" forKey:@"type"];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:[_userEventsArray objectAtIndex:indexPath.row]];
    eventDetailVC.venueId = cell.accessibilityIdentifier;
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetailVC release];
}

#pragma mark - NetworkMangerDelegate Delegate Methods

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"ownevents"])
        {
            self.userEventsArray=[[NSMutableArray alloc] initWithArray:[[object objectForKey:@"data"] objectForKey:@"posted"]];
            self.inEventsArray=[[NSMutableArray alloc] initWithArray:[[object objectForKey:@"data"] objectForKey:@"following"]];
        }
        else if([networkManager.identifier isEqualToString:@"deleteevent"])
        {
            if(_deleteIndex>=0)
            {
                [self.userEventsArray removeObjectAtIndex:self.deleteIndex];
            }
            else
            {
                [self.inEventsArray removeObjectAtIndex:(self.deleteIndex * -1) - 1];
            }
        }
        if(_userEventsArray.count<1 && _inEventsArray.count<1)
        {
            self.noEventLabel.hidden=NO;
            _userEvntsTable.hidden=YES;
        }
        else
        {
            self.noEventLabel.hidden=YES;
            _userEvntsTable.hidden=NO;
        }
        
        [_userEvntsTable reloadData];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
