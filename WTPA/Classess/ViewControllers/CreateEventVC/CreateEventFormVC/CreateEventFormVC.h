//
//  CreateEventFormVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEventFormVC : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDictionary:(NSMutableDictionary *) dic;

@end
