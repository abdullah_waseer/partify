//
//  CreateEventFormVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "CreateEventFormVC.h"
#import "GCPlaceholderTextView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SelectCategoryPopupView.h"
#import "NSDate-Utilities.h"
#import "EventPreviewVC.h"
#import "TagPickerVC.h"
#import "DropPinVC.h"


@interface CreateEventFormVC ()<NetworkManagerDelegate,SelectCategoryPopupViewDelegate,tagPickerVCDelegate,dropPinVCDelegate>

@property (nonatomic,retain) IBOutlet GCPlaceholderTextView *txtAddress;
@property (nonatomic,retain) IBOutlet GCPlaceholderTextView *txtDetailLink;
@property (nonatomic,retain) SelectCategoryPopupView *datePickerView;
@property (nonatomic,retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic,retain) IBOutlet UIButton *startDateBtn;
@property (nonatomic,retain) IBOutlet UIButton *endDateBtn;
@property (nonatomic,retain) IBOutlet UIButton *venuNameBtn;

@property (nonatomic,retain) IBOutlet UITextField *eventName;
@property (nonatomic,retain) IBOutlet UITextField *venueName;

@property (nonatomic,retain) NSMutableDictionary *selectedDictionary;

@property (nonatomic,assign) BOOL isUpdateCall;

@end

@implementation CreateEventFormVC

-(void) dealloc
{
    [_startDateBtn  release],_startDateBtn=nil;
    [_endDateBtn  release],_endDateBtn=nil;
    [_venuNameBtn  release],_venuNameBtn=nil;
    [_txtAddress release],_txtAddress=nil;
    [_txtDetailLink release],_txtDetailLink=nil;
    [_datePickerView release],_datePickerView=nil;
    [_scrollView release],_scrollView=nil;
    [_txtAddress release],_txtAddress=nil;
    [_eventName release],_eventName=nil;
    [_venueName release],_venueName=nil;
    [_selectedDictionary release],_selectedDictionary=nil;
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andDictionary:(NSMutableDictionary *) dict
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.isUpdateCall=YES;
        [self popualteDictionary:dict];
       
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    self.scrollView.contentSize=CGSizeMake(320, IS_IPHONE_5?720:770);
    
    
    if(!self.isUpdateCall)
    {
        self.txtAddress.placeholder=@"Address";
        _txtDetailLink.placeholder=@"Detail & Links";
        
        self.selectedDictionary=[[NSMutableDictionary alloc] init];
        
        [_startDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:[NSDate date]] forState:UIControlStateNormal];
        
        [_selectedDictionary setObject:[NSDate date] forKey:@"startdate"];
    }
    else
    {
        [self filedViewsWithData];
    }
    [self setUpNavigationBar];
    // Do any additional setup after loading the view from its nib.
}

-(void) popualteDictionary:(NSDictionary *) dict
{
    self.selectedDictionary=[[NSMutableDictionary alloc] init];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_name"]] forKey:@"eventname"];
    [_selectedDictionary setObject:[AppUtils getDateFromString:[dict objectForKey:@"event_start_time"] withFormat:@"yyyy-MM-dd hh:mm:ss"]forKey:@"startdate"];
    [_selectedDictionary setObject:[AppUtils getDateFromString:[dict objectForKey:@"event_end_time"] withFormat:@"yyyy-MM-dd hh:mm:ss"] forKey:@"enddate"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[[dict objectForKey:@"venue"] objectForKey:@"venu_id"]] forKey:@"venueid"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[[dict objectForKey:@"venue"] objectForKey:@"name"]] forKey:@"venuename"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[self returnAddressFromKeys:[[dict objectForKey:@"venue"] objectForKey:@"location"]]] forKey:@"venueddress"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_description"]] forKey:@"detaillink"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]] forKey:@"eventid"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"visibility"]] forKey:@"visibility"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@""]] forKey:@"archive"];
    
}

-(NSString *)  returnAddressFromKeys:(NSDictionary *) diuctionary
{
    NSString *string=[NSString stringWithFormat:@"%@, %@, %@, %@",[diuctionary objectForKey:@"country"],[diuctionary objectForKey:@"city"],[diuctionary objectForKey:@"street"],[diuctionary objectForKey:@"postalcode"]];
    
    return string;
}

-(void) filedViewsWithData
{
    self.eventName.text=[_selectedDictionary objectForKey:@"eventname"];
    self.venueName.text=[_selectedDictionary objectForKey:@"venuename"];
    self.txtAddress.text=[_selectedDictionary objectForKey:@"venueddress"];
    self.txtDetailLink.text=[_selectedDictionary objectForKey:@"detaillink"];
    [_startDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:[_selectedDictionary objectForKey:@"startdate"]] forState:UIControlStateNormal];
    [_endDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:[_selectedDictionary objectForKey:@"enddate"]] forState:UIControlStateNormal];
}

#pragma mark - Custom methods Started

-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)ifContainsPicker
{
    BOOL containsIntrmdtView = [[UIApplication sharedApplication].keyWindow.subviews  containsObject:_datePickerView];
    return containsIntrmdtView;
}

- (void)showPopover:(NSString *) selectedValue andArrayOfValues:(NSArray *) values
{
    if ([self ifContainsPicker])
    {
        [_datePickerView setDelegate:nil];
        [_datePickerView removeFromSuperview];
    }
    
    CGRect theFrame = CGRectMake(0,  380 + (IS_IPHONE_5?screenDifference:0.0)-(155), 320, 216+42);
    _datePickerView = [[SelectCategoryPopupView alloc] initWithFrame:theFrame withArray:values withSelectedCategory:selectedValue withPickerSelected:YES withOnlyTimeMode:NO andDelegate:self];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_datePickerView];
    [_datePickerView show];
}

-(BOOL) isValidated
{
    if(self.eventName.text.length == 0)
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please enter event name!" afterDelay:0.0];
        return NO;
    }
    else if(![_selectedDictionary objectForKey:@"startdate"])
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please select start date!" afterDelay:0.0];
        return NO;
    }
    else if(![_selectedDictionary objectForKey:@"enddate"])
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please select end date!" afterDelay:0.0];
        return NO;
    }
    else if(![_selectedDictionary objectForKey:@"venueid"])
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please select venue!" afterDelay:0.0];
        return NO;
    }
    else if(self.txtDetailLink.text.length == 0)
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please enter event details!" afterDelay:0.0];
        return NO;
    }
   return YES;
}

-(void) showErrorAlert:(NSString *) error
{
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:error];
}

#pragma mark - IBActions methods Started

-(IBAction)addPinClicked:(id)sender
{
    [self hidekeyBorad:nil];
    
    DropPinVC * dropPinVC = [[DropPinVC alloc]initWithNibName:@"DropPinVC" bundle:nil];
    
    dropPinVC.delegate = self;
    
    [self.navigationController pushViewController:dropPinVC animated:YES];
    
    [dropPinVC release];
}

-(IBAction)tagLocationClicked:(id)sender
{
    [self hidekeyBorad:nil];
    
    TagPickerVC * tagPickerVC = [[TagPickerVC alloc]initWithNibName:@"TagPickerVC" bundle:nil];
    
    tagPickerVC.delegate = self;
    
    [self.navigationController pushViewController:tagPickerVC animated:YES];
    
    [tagPickerVC release];
}


-(IBAction)hidekeyBorad:(id)sender
{
    [_eventName resignFirstResponder];
    [_txtAddress resignFirstResponder];
    [_txtDetailLink resignFirstResponder];
    
}

-(IBAction)StartDateClicked:(id)sender
{
    [sender setSelected:YES];
    [self hidekeyBorad:nil];
    [self showPopover:[[SharedAppManager sharedInstance] returnDateAfterForamting:[_selectedDictionary objectForKey:@"startdate"]] andArrayOfValues:nil];
}

-(IBAction)EndDateClicked:(id)sender
{
     [sender setSelected:YES];
     [self hidekeyBorad:nil];
    
    NSDate *endDate=[_selectedDictionary objectForKey:@"enddate"];
    [self showPopover:endDate?[[SharedAppManager sharedInstance] returnDateAfterForamting:endDate]:nil andArrayOfValues:nil];
}

- (IBAction)submitClicked:(id)sender
{
    if([self isValidated])
    {
        if(self.isUpdateCall)
        {
            [_selectedDictionary setObject:@"y" forKey:@"update"];
        }
        else
        {
            [_selectedDictionary setObject:@"n" forKey:@"update"];
        }
        
        [_selectedDictionary setObject:[NSString stringWithFormat:@"%@", _eventName.text] forKey:@"eventname"];
        [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",_txtDetailLink.text] forKey:@"detaillink"];
        
        EventPreviewVC *eventPreviewVC=[[EventPreviewVC alloc] initWithNibName:@"EventPreviewVC" bundle:nil andEventData:self.selectedDictionary];
        [self.navigationController pushViewController:eventPreviewVC animated:YES];
        [eventPreviewVC release];
    }
}


#pragma mark -  Delegate Methods starts


#pragma mark - TagPickerVC Delegate Methods

- (void) tagPickerVC:(TagPickerVC *)tagPickerVc withLocation:(NSMutableDictionary *)data
{
   _venueName.text=data[@"venu"][@"name"];
    
    _txtAddress.text=[NSString stringWithFormat:@"%@ ,%@ , %@ ,",data[@"venu"][@"country"],data[@"venu"][@"city"],data[@"venu"][@"address"]];
    
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",data[@"venu"][@"name"]] forKey:@"venuename"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",data[@"venuid"]] forKey:@"venueid"];
}

#pragma mark - SelectCategoryPopupView Delegate Methods

- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedDateDidSave:(NSDate*)value
{
	if(value != nil)
	{
        if(_startDateBtn.selected)
        {
             [_startDateBtn setSelected:NO];
            if(![value isEarlierThanDate:[NSDate date]])
            {
                [_startDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:value] forState:UIControlStateNormal];
                [_selectedDictionary setObject:value forKey:@"startdate"];
            }
            else
            {
                [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please select a valid date"];
            }
        }
        else
        {
             [_endDateBtn setSelected:NO];
            if(![value isEarlierThanDate:[_selectedDictionary objectForKey:@"startdate"]])
            {
                [_endDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:value] forState:UIControlStateNormal];
                [_selectedDictionary setObject:value forKey:@"enddate"];
            }
            else
            {
                [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please select a valid date"];
            }
        }
    }
    self.navigationController.hidesBottomBarWhenPushed = NO;
    [self.datePickerView hide];
}

- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedValueDidSave:(NSString*)value withIndex:(NSInteger)selectedIndex
{
    if(value)
    {
    }
    self.navigationController.hidesBottomBarWhenPushed = NO;
    [self.datePickerView hide];
    
}

#pragma mark - DropPinVC Delegate Methods

- (void) pinDropVc:(DropPinVC*) dropPinVC withVenuDictionary:(NSMutableDictionary *) venuData
{
    _venueName.text=venuData[@"name"];
    
    _txtAddress.text=[NSString stringWithFormat:@"%@ ,%@ , %@ ",venuData[@"country"],venuData[@"city"],venuData[@"address"]];
    
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",venuData[@"name"]] forKey:@"venuename"];
    [_selectedDictionary setObject:[NSString stringWithFormat:@"%@",venuData[@"venuid"]] forKey:@"venueid"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
