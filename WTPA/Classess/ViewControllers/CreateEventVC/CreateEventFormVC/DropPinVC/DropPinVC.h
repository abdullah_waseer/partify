//
//  DropPinVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 11/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>

@class DropPinVC;
@protocol dropPinVCDelegate <NSObject>

- (void) pinDropVc:(DropPinVC*) dropPinVC withVenuDictionary:(NSMutableDictionary *) venuData;

@end
@interface Annotation : NSObject<MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	
	NSString *mTitle;
	NSString *mSubTitle;
	NSInteger index;
}

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mSubTitle;
@property (nonatomic) NSInteger index;

@end
@interface DropPinVC : UIViewController<CLLocationManagerDelegate,UIGestureRecognizerDelegate>
{
    CLLocationManager *locationManager;
    Annotation *addressAnotation;
    CLLocationCoordinate2D geoCoordinatesAddress;
    BOOL panned;
    BOOL callInProgress;
}

@property (assign, nonatomic) id<dropPinVCDelegate> delegate;

@end
