//
//  DropPinVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 11/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "DropPinVC.h"
#import "TPKeyboardAvoidingScrollView.h"

#import "DDAnnotation.h"
#import "DDAnnotationView.h"

@implementation Annotation

@synthesize coordinate;
@synthesize mTitle, mSubTitle;
@synthesize index;


static NSString* const GMAP_ANNOTATION_SELECTED = @"gMapAnnontationSelected";

- (NSString *)subtitle
{
	return mSubTitle;
}
- (NSString *)title
{
	return mTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTitle:(NSString*)thisTitle withSubTitle:(NSString*)thisSubTitle
{
	if(self = [super init])
	{
		coordinate=c;
		mTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisTitle]];
		mSubTitle = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",thisSubTitle]];
	}
	return self;
}

- (void)dealloc
{
	[mTitle release];
	[mSubTitle release];
    [super dealloc];
}

@end


@interface DropPinVC ()<NetworkManagerDelegate>

@property (nonatomic,retain) IBOutlet MKMapView *dropPinMap;
@property (nonatomic,retain) IBOutlet UIButton *doneButton;
@property (nonatomic,retain) IBOutlet UITextField *venueName;
@property (nonatomic,retain) NetworkManager * networkManager;
@property (nonatomic,retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic,retain) NSMutableDictionary *addressDictionary;

@end

@implementation DropPinVC


-(void) dealloc
{
    [_dropPinMap release],_dropPinMap=nil;
    [_doneButton release],_doneButton=nil;
    [_venueName release],_venueName=nil;
    [_networkManager release],_networkManager=nil;
    [_addressDictionary release],_addressDictionary=nil;
    [_scrollView release],_scrollView=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    callInProgress=YES;
    self.scrollView.contentSize=CGSizeMake(320, 568);

    [self setUpNavigationBar];
    
    _addressDictionary=[[NSMutableDictionary alloc]init];
    
    [_addressDictionary setObject:@"" forKey:@"name"];
    [_addressDictionary setObject:@"" forKey:@"address"];
    [_addressDictionary setObject:@"" forKey:@"city"];
    [_addressDictionary setObject:@"" forKey:@"state"];
    [_addressDictionary setObject:@"" forKey:@"zip"];
    [_addressDictionary setObject:@"" forKey:@"country"];
    [_addressDictionary setObject:@"" forKey:@"street"];

    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
    }
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
     panned=NO;
    
    self.doneButton.frame=CGRectMake(0, IS_IPHONE_5?416:416-screenDifference, 320,38);
    
    UILongPressGestureRecognizer *tapRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewTapped:)];
    tapRecognizer.minimumPressDuration=0.5;
    tapRecognizer.delegate = self;
    [_dropPinMap addGestureRecognizer:tapRecognizer];
    
}

#pragma mark - Custom methods Started

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
//    UIButton* done= [UIButton buttonWithType:UIButtonTypeCustom];
//    [done addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    done.frame=CGRectMake(0, 5.0, 40.0, 34.0);
//    done.backgroundColor=CUSTOM_APP_COLOR;
//    done.titleLabel.font=[UIFont fontWithName:@"Thonburi" size:16];
//    done.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
//    [done setTitle:@"Done" forState:UIControlStateNormal];
//    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:done];
//    self.navigationItem.rightBarButtonItem = rightBarButton;
//    [rightBarButton release];
}
-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    locationManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)mapViewTapped:(UILongPressGestureRecognizer *)recognizer
{
    
    panned=NO;
    CGPoint pointTappedInMapView = [recognizer locationInView:_dropPinMap];
    CLLocationCoordinate2D geoCoordinatesTapped = [_dropPinMap convertPoint:pointTappedInMapView toCoordinateFromView:_dropPinMap];
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            /* equivalent to touchesBegan:withEvent: */
            break;
            
        case UIGestureRecognizerStateChanged:
            /* equivalent to touchesMoved:withEvent: */
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self getAddredssFromCoordinates:geoCoordinatesTapped];
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
            /* equivalent to touchesCancelled:withEvent: */
            break;
            
        default:
            break;
    }
}

- (void)pinPanned:(UIPanGestureRecognizer *)recognizer
{
    CGPoint pointTappedInMapView = [recognizer locationInView:_dropPinMap];
    geoCoordinatesAddress= [_dropPinMap convertPoint:pointTappedInMapView toCoordinateFromView:_dropPinMap];
    
    UIView *v = [_dropPinMap hitTest:pointTappedInMapView withEvent:nil];
    
    if ([v isKindOfClass:[MKAnnotationView class]])
    {
        _dropPinMap.scrollEnabled=NO;
        
         panned=YES;
        
        [self performSelector:@selector(drawPinCordinates) withObject:nil afterDelay:0.2];
    }
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            /* equivalent to touchesBegan:withEvent: */
            break;
            
        case UIGestureRecognizerStateChanged:
            /* equivalent to touchesMoved:withEvent: */
            break;
            
        case UIGestureRecognizerStateEnded:
        {
//            tapped=YES;
//            [self getAddredssFromCoordinates:geoCoordinatesTapped];
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
            /* equivalent to touchesCancelled:withEvent: */
            break;
            
        default:
            break;
    }
}

- (void) drawPinCordinates
{
//	MKCoordinateRegion region;
//	MKCoordinateSpan span;
//	span.latitudeDelta=0.06;
//	span.longitudeDelta=0.06;
//    
//	region.span = span;
    
//	region.center = coordinates;
	
	for (id<MKAnnotation> annotation in _dropPinMap.annotations)
    {
        [_dropPinMap removeAnnotation:annotation];
	}
	
//	addressAnotation = [[Annotation alloc] initWithCoordinate:geoCoordinatesAddress withTitle:@"Venue" withSubTitle:[NSString stringWithFormat:@"%@ ,%@",[_addressDictionary objectForKey:@"address"], [_addressDictionary objectForKey:@"city"]]];
    
    DDAnnotation *annotation = [[[DDAnnotation alloc] initWithCoordinate:geoCoordinatesAddress addressDictionary:nil] autorelease];
	annotation.title = @"Venue";
	annotation.subtitle =[NSString stringWithFormat:@"%@ ,%@",[_addressDictionary objectForKey:@"address"], [_addressDictionary objectForKey:@"city"]];
	[_dropPinMap addAnnotation:annotation];
    
//	[_dropPinMap setRegion:region animated:TRUE];
//	[_dropPinMap regionThatFits:region];
}


-(void) getAddredssFromCoordinates:(CLLocationCoordinate2D ) coordinates
{
    if(callInProgress)
    {
        callInProgress=NO;
        
        NSString *latLongCombined = [NSString stringWithFormat:@"%f,%f",coordinates.latitude,coordinates.longitude];
        NSString *strURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@&sensor=true", latLongCombined];
        strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:[NSMutableDictionary dictionary]];
        
        [jsonWriter release];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        
        [parameters setObject:jsonString forKey:@"param"];
        [parameters setObject:[NSMutableDictionary dictionary] forKey:@"headers"];
        [parameters setObject:strURL forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"addressFind" forKey:@"idetntifier"];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
   
    }
}

-(BOOL) isValidated
{
    if(self.venueName.text.length == 0)
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please enter venue name!" afterDelay:0.0];
        return NO;
    }
    else if(![_addressDictionary objectForKey:@"venu_location"])
    {
        [self performSelector:@selector(showErrorAlert:) withObject:@"Please select start date!" afterDelay:0.0];
        return NO;
    }
    return YES;
}

-(void) showErrorAlert:(NSString *) error
{
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:error];
}

-(void) popuLateAddressDictionary:(NSArray *) result
{
    NSArray *results = [[NSArray alloc] initWithArray:result];
    
    NSArray *address=[[results objectAtIndex:0] objectForKey:@"address_components"];
    
    NSString *street=@"";
    NSString *route=@"";
    NSString *locality=@"";
    
    for (int i=0;i<address.count;i++)
    {
        if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_2"]) //city
        {
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"city"];
        }
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"street_number"]) //street
        {
            street=[NSString stringWithFormat:@"%@",
                    [[address objectAtIndex:i] objectForKey:@"long_name"]];
            
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"street"];
            
        }
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"country"]) //country
        {
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"country"];
            
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"short_name"]] forKey:@"state"];
            
        }
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_2"]) //city
        {
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"state"];
        }
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"postal_code"]) //Zip
        {
            [_addressDictionary setObject:[NSString stringWithFormat:@"%@",
                                           [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"zip"];
        }
        
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"locality"]) //Zip
        {
            locality=[NSString stringWithFormat:@"%@",
                      [[address objectAtIndex:i] objectForKey:@"long_name"]];
        }
        else if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"route"]) //Zip
        {
            route=[NSString stringWithFormat:@"%@",
                   [[address objectAtIndex:i] objectForKey:@"long_name"]];
        }
    }
    
    [_addressDictionary setObject:[NSString stringWithFormat:@"%@,%@,%@",
                                   street,route,locality] forKey:@"address"];
    NSString *lat=[NSString stringWithFormat:@"%@",
                   [[[[results objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"]];
    NSString *lng=[NSString stringWithFormat:@"%@",
                   [[[[results objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"]];
    
    [_addressDictionary setObject:[NSDictionary dictionaryWithObjectsAndKeys:lat,@"lat",lng,@"long", nil] forKey:@"venu_location"];
    geoCoordinatesAddress.latitude=[lat floatValue];
    geoCoordinatesAddress.longitude=[lng floatValue];
    
    [self drawPinCordinates];

}

#pragma mark - IBAction methods Started

-(IBAction)doneButtonPressed:(id)sender
{
    if([self isValidated])
    {
        NSString* urlString = [NSString stringWithFormat:@"%@api/venu/createvenu",[SharedAppManager sharedInstance].baseURL];
        [_addressDictionary setObject:_venueName.text forKey:@"name"];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:_addressDictionary];
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        [parameters setObject:jsonString forKey:@"param"];
        [jsonWriter release];
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json ",@"Content-type",nil] forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"POST" forKey:@"callType"];
        [parameters setObject:@"createVenue" forKey:@"idetntifier"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [_networkManager sendRequestforAction:parameters];
    }
}


#pragma mark - IBActions methods Started

#pragma mark - Delegate Methods Starts

#pragma mark - Network Delegates
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    callInProgress=YES;
    if([networkManager.identifier isEqualToString:@"addressFind"])
    {
        if([[object objectForKey:@"status"] isEqualToString:@"OK"] && [[object objectForKey:@"results"] count]>0 )
        {
            [self popuLateAddressDictionary:[object objectForKey:@"results"]];
        }
        else
        {
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No address found, Please try again"];
        }
    }
    else if([networkManager.identifier isEqualToString:@"createVenue"] && [[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        [_addressDictionary setObject:[object valueForKey:@"data"] forKey:@"venuid"];
        if([self.delegate respondsToSelector:@selector(pinDropVc:withVenuDictionary:)])
        {
            
            [self.delegate pinDropVc:self withVenuDictionary:self.addressDictionary];
            [self backAction:nil];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No address found, Please try again!"];
    }
    
   
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


#pragma mark - Location Manager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
        [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"
                                                    message:errorType delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

#pragma mark - UIGestureRecongnizer Delegate Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGesture
{
    return YES;
}

#pragma mark - MKMapView Delegate Methods

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
	
	if (oldState == MKAnnotationViewDragStateDragging) {
		DDAnnotation *annotation = (DDAnnotation *)annotationView.annotation;
        [self getAddredssFromCoordinates:annotation.coordinate];
        }
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
//    if([annotation isKindOfClass:[Annotation class]])
//    {
//        MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
//        annView.pinColor = MKPinAnnotationColorGreen;
//        //annView.frame = CGRectMake(-5, 5, 200, 70);
//        annView.animatesDrop=!panned;
//        annView.canShowCallout = YES;
//        annView.calloutOffset = CGPointMake(-5, 5);
//        
//        _dropPinMap.scrollEnabled=YES;
//        
//        UIPanGestureRecognizer *doubleTap = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pinPanned:)];
//        doubleTap.delegate = self;
//        [annView addGestureRecognizer:doubleTap];
//        
//        return [annView autorelease];
//    }
//  
//    return nil;
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
	}
	
	static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
	MKAnnotationView *draggablePinView = [_dropPinMap dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
	
	if (draggablePinView) {
		draggablePinView.annotation = annotation;
	} else {
		// Use class method to create DDAnnotationView (on iOS 3) or built-in draggble MKPinAnnotationView (on iOS 4).
		draggablePinView = [DDAnnotationView annotationViewWithAnnotation:annotation reuseIdentifier:kPinAnnotationIdentifier mapView:_dropPinMap];
        
		if ([draggablePinView isKindOfClass:[DDAnnotationView class]]) {
			// draggablePinView is DDAnnotationView on iOS 3.
		} else {
			// draggablePinView instance will be built-in draggable MKPinAnnotationView when running on iOS 4.
		}
	}
	
	return draggablePinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
{
	
}

#pragma mark -
#pragma mark DDAnnotationCoordinateDidChangeNotification

// NOTE: DDAnnotationCoordinateDidChangeNotification won't fire in iOS 4, use -mapView:annotationView:didChangeDragState:fromOldState: instead.
- (void)coordinateChanged_:(NSNotification *)notification {
	
    //	DDAnnotation *annotation = notification.object;
    //	annotation.subtitle = [NSString	stringWithFormat:@"%f %f", annotation.coordinate.latitude, annotation.coordinate.longitude];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
