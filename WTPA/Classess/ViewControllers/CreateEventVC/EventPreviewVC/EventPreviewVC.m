//
//  EventPreviewVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 26/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "EventPreviewVC.h"
#import "SuccesEventCreateVC.h"

@interface EventPreviewVC ()<NetworkManagerDelegate>

@property (nonatomic,retain) NSMutableDictionary *eventDictionry;
@property (nonatomic,retain) IBOutlet UILabel *eventName;
@property (nonatomic,retain) IBOutlet UILabel *venueName;
@property (nonatomic,retain) IBOutlet UITextView *eventDetail;
@property (nonatomic,retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic,retain) NetworkManager * networkManager;
@end

@implementation EventPreviewVC


-(void) dealloc
{
    [_eventDictionry release],_eventDictionry=nil;
    [_eventName release],_eventName=nil;
    [_venueName release],_venueName=nil;
    [_eventDetail release],_eventDetail=nil;
    [_scrollView release],_scrollView=nil;
    [_networkManager release],_networkManager.delegate=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andEventData:(NSMutableDictionary *) eventData
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _eventDictionry=[[NSMutableDictionary alloc] initWithDictionary:eventData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    self.scrollView.contentSize=CGSizeMake(320, IS_IPHONE_5?630:680);
    
    [_eventDictionry setObject:@"public" forKey:@"visibility"];
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    [self setUpNavigationBar];
    [self updateUI];
    
    // Do any additional setup after loading the view from its nib.
}


#pragma mark - Custom methods Started

-(void) updateUI
{
    _eventName.text=[NSString stringWithFormat:@"%@",[_eventDictionry objectForKey:@"eventname"]];
    _venueName.text=[NSString stringWithFormat:@"%@",[_eventDictionry objectForKey:@"venuename"]];
    _eventDetail.text=[NSString stringWithFormat:@"%@",[_eventDictionry objectForKey:@"detaillink"]];
}

-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBActions methods Started
- (IBAction)profileToggleBtnPressed:(id)sender
{
    UIButton *b=(UIButton  *) sender;
    b.selected=!b.selected;
    if(!b.selected)
    {
        [_eventDictionry setObject:@"public" forKey:@"visibility"];
    }
    else
    {
        [_eventDictionry setObject:@"private" forKey:@"visibility"];
    }
}

-(IBAction) postEventClicked:(id) sender
{
    NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
    
    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    
    [param setObject:[AppUtils getStrindFromDate:[self.eventDictionry objectForKey:@"startdate"] withFormat:@"yyyy-MM-dd hh:mm:ss"] forKey:@"event_start_time"];
    [param setObject:[self.eventDictionry objectForKey:@"visibility"] forKey:@"visibility"];
    [param setObject:[AppUtils getStrindFromDate:[self.eventDictionry objectForKey:@"enddate"] withFormat:@"yyyy-MM-dd hh:mm:ss"] forKey:@"event_end_time"];
    [param setObject:[self.eventDictionry objectForKey:@"eventname"] forKey:@"event_name"];
    [param setObject:[data objectForKey:@"id"] forKey:@"user_id"];
    [param setObject:[self.eventDictionry objectForKey:@"detaillink"] forKey:@"event_description"];
    [param setObject:[self.eventDictionry objectForKey:@"venueid"] forKey:@"venu_id"];
    [param setObject:@"" forKey:@"archive"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:dictionary forKey:@"headers"];
    
    if([[self.eventDictionry objectForKey:@"update"] isEqualToString:@"n"])
    {
        NSString* urlString = [NSString stringWithFormat:@"%@api/events/create",[SharedAppManager sharedInstance].baseURL];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"POST" forKey:@"callType"];
        [parameters setObject:@"createevent" forKey:@"idetntifier"];
    }
    else
    {
        NSString* urlString = [NSString stringWithFormat:@"%@api/events/update/%@",[SharedAppManager sharedInstance].baseURL,[self.eventDictionry objectForKey:@"eventid"]];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"PUT" forKey:@"callType"];
        [parameters setObject:@"updateEvent" forKey:@"idetntifier"];
    }

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}


#pragma mark -  Delegate Methods starts

#pragma mark - NetworkMangerDelegate Delegate Methods


- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        SuccesEventCreateVC *successEventVC=[[SuccesEventCreateVC alloc] initWithNibName:@"SuccesEventCreateVC" bundle:nil];
        [self.navigationController pushViewController:successEventVC animated:YES];
        [successEventVC release];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
