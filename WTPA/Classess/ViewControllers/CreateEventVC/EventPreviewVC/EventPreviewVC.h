//
//  EventPreviewVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 26/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventPreviewVC : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andEventData:(NSMutableDictionary *) eventData;

@end
