//
//  SuccesEventCreateVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 27/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SuccesEventCreateVC.h"
#import "AMAttributedHighlightLabel.h"

@interface SuccesEventCreateVC ()<AMAttributedHighlightLabelDelegate>

@property (retain, nonatomic) IBOutlet AMAttributedHighlightLabel *eventsLabel;

@end

@implementation SuccesEventCreateVC
-(void) dealloc
{
    [_eventsLabel release],_eventsLabel=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshEvents"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationItem.hidesBackButton=YES;
    _eventsLabel.delegate = self;
    _eventsLabel.userInteractionEnabled = YES;
    _eventsLabel.numberOfLines = 0;
    _eventsLabel.textColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1];
    _eventsLabel.mentionTextColor=[UIColor blueColor];
    _eventsLabel.selectedMentionTextColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1];
    _eventsLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [_eventsLabel setString:@"You can manage all your events from YOUR EVENTS page if you want to edit or delete them."];
    // Do any additional setup after loading the view from its nib.
}

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
    titleView.textColor = [UIColor whiteColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = [@"partify" uppercaseString];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
}


#pragma mark -  Delegate Methods starts

#pragma mark - AMAttributedHighlightLabel Delegate Methods

- (void)selectedMention:(NSString *)string {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)selectedHashtag:(NSString *)string {

}
- (void)selectedLink:(NSString *)string {
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
