//
//  PhotoUploadVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "PhotoUploadVC.h"
#import "TagPickerVC.h"
#import "FaceBookManager.h"
#import "SocialSharingVC.h"
#import "FGalleryViewController.h"
#import "CameraOverlayView.h"
#import "UIImage+Utils.h"

@interface PhotoUploadVC () <IGSessionDelegate,FGalleryViewControllerDelegate,CameraOverlayViewDelegate,NetworkManagerDelegate>

@property (nonatomic,retain) NSMutableArray * imagesWithLink;
@property (nonatomic,retain) UIImageView *profileImage;
@property (nonatomic,retain) UIImagePickerController * picker;
@property (nonatomic) NSInteger selectedOverlay;

@end

@implementation PhotoUploadVC

FGalleryViewController * networkGallery;


- (void)didTapFlashOnWithCameraOverlayView:(CameraOverlayView *)view andTypeOfFlash:(NSMutableDictionary *) flashmodes
{
    _picker.cameraFlashMode=[[flashmodes objectForKey:@"flashmode"] integerValue];
}
- (void)didTapFrontBackWithCameraOverlayView:(CameraOverlayView *)view
{
    if(_picker.cameraDevice==UIImagePickerControllerCameraDeviceRear)
        _picker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
    else
        _picker.cameraDevice=UIImagePickerControllerCameraDeviceRear;
}

- (void)didTapInstaGramButtonWithCameraOverlayView:(CameraOverlayView *)view
{
    [_picker dismissViewControllerAnimated:NO completion:^{ [self instagramBtnPressed];}];
    
}
- (void)didTapPhotoGalleryWithCameraOverlayView:(CameraOverlayView *)view
{
    [_picker dismissViewControllerAnimated:NO completion:^{ [self fromGalleryBtnPressed];}];
}

-(void)didTapCaptureButtonWithCameraOverlayView:(CameraOverlayView *)view
{
    [_picker performSelector:@selector(takePicture) withObject:nil];
}

- (void)didTapCancelyWithCameraOverlayView:(CameraOverlayView *)view
{
    [_picker dismissViewControllerAnimated:NO completion:^{ self.tabBarController.selectedIndex=0;}];
}

#pragma mark - init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setUpNavigationBar];
    
    
//    [self  showImagePickerController: UIImagePickerControllerSourceTypeCamera];
    //self.view = overlayIV;
    //[[FaceBookManager sharedManager]uploadStausWithText:@"http://google.com"];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self  showImagePickerController: UIImagePickerControllerSourceTypeCamera];
}
#pragma mark - Custom methods Started


-(void) setUpNavigationBar

{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods Implementation

-(void) getImagesFromInstagaram
{
    
    NSString* urlString = [NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/media/recent?access_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"accessTokenIns"]];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    [jsonWriter release];
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionary] forKey:@"headers"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getImagesFromInstagram" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}
-(void) cancelGalleryAction
{
    [_picker dismissViewControllerAnimated:NO completion:^{
    
    [self  showImagePickerController:UIImagePickerControllerSourceTypeCamera];}];
//    [_picker performSelector:@selector(takePicture) withObject:nil afterDelay:0.1];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    UINavigationItem *ipcNavBarTopItem;
    
    // add done button to right side of nav bar
    UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom];
    butt.frame=CGRectMake(0, 4, 70, 36);
    [butt setTitle:@"Cancel" forState:UIControlStateNormal];
    butt.titleLabel.textColor=[UIColor whiteColor];
    [butt addTarget:self action:@selector(cancelGalleryAction) forControlEvents:UIControlEventTouchDown];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:butt];
    
    
    UINavigationBar *bar = self.picker.navigationBar;
    [bar setHidden:NO];
    ipcNavBarTopItem = bar.topItem;
//    ipcNavBarTopItem.title = @"c";
    ipcNavBarTopItem.rightBarButtonItem = doneButton;
}

-(void) showImagePickerController:(UIImagePickerControllerSourceType) sourceType
{
    if([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        if(self.picker)
           [_picker release],_picker=nil;
        _picker = [[UIImagePickerController alloc] init];
        self.picker.delegate = self;
        self.picker.sourceType = sourceType;
        self.picker.allowsEditing=NO;
        
       
        
        if(sourceType==UIImagePickerControllerSourceTypeCamera)
        {
            self.picker.showsCameraControls = NO;
            self.picker.cameraFlashMode=0;
            self.picker.cameraDevice=UIImagePickerControllerCameraDeviceRear;
            CameraOverlayView *overlayIV = [[CameraOverlayView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, (IS_IPHONE_5 ? 480 + screenDifference : 480.0))];
            //overlayIV.image = overlayImage;
            //[overlayIV setBackgroundColor:[UIColor redColor]];
            overlayIV.delegate = self;
            self.picker.cameraOverlayView = overlayIV;
            
            if(IS_IPHONE_5)
            {
                CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 71.0); //This slots the preview exactly in the middle of the screen by moving it down 71 points
                self.picker.cameraViewTransform = translate;
                
                CGAffineTransform scale = CGAffineTransformScale(translate, 1.333333, 1.333333);
                self.picker.cameraViewTransform = scale;
            }
        }
        else
        {
//            self.picker.navigationBar.tintColor = [UIColor blackColor];
//            UINavigationItem *ipcNavBarTopItem = nil;
            
//            UIButton *butt=[UIButton buttonWithType:UIButtonTypeCustom];
//            [butt setTitle:@"cancel" forState:UIControlStateNormal];
//            butt.titleLabel.textColor=[UIColor whiteColor];
//
//            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:butt];
//        
//            
//            UINavigationBar *bar = self.picker.navigationBar;
//            [bar setHidden:NO];
//            ipcNavBarTopItem = bar.topItem;
//            ipcNavBarTopItem.title = @"c";
//            ipcNavBarTopItem.rightBarButtonItem = doneButton;

        }
        
        [self presentViewController:self.picker animated:NO completion:^{}];
    }
}


#pragma mark - IBActions Implementation

- (void)fromGalleryBtnPressed
{
    [self showImagePickerController:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)instagramBtnPressed
{
    if( ![[NSUserDefaults standardUserDefaults] valueForKey:@"accessTokenIns"])
    {
        [SharedAppManager sharedInstance].instagram.sessionDelegate = self;
        [[SharedAppManager sharedInstance].instagram authorize:[NSArray arrayWithObjects:@"comments", @"likes", nil]];
    }
    else
    {
        [self getImagesFromInstagaram];
    }
}

#pragma mark - Delegate Methods Implementation Starts
#pragma - IGSessionDelegate

-(void)igDidLogin {
    NSLog(@"Instagram did login");
    // here i can store accessToken

    [[NSUserDefaults standardUserDefaults] setObject:[SharedAppManager sharedInstance].instagram.accessToken forKey:@"accessTokenIns"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    [self getImagesFromInstagaram];

}

-(void)igDidNotLogin:(BOOL)cancelled {
    NSLog(@"Instagram did not login");
    NSString* message = nil;
    if (cancelled) {
        message = @"Access cancelled!";
    } else {
        message = @"Access denied!";
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(void)igDidLogout {
    NSLog(@"Instagram did logout");
    // remove the accessToken
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"accessToken"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)igSessionInvalidated {
    NSLog(@"Instagram session was invalidated");
}

#pragma mark - FGallery Delegates
- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
	return [self.imagesWithLink count];
}


- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
	return FGalleryPhotoSourceTypeNetwork;
}

- (void)photoGallery:(FGalleryViewController*)gallery removePhotoAtIndex:(NSInteger)index
{
    NSMutableDictionary * params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[[self.imagesWithLink objectAtIndex:index]objectForKey:@"standard"],@"imageURL", nil];
    SocialSharingVC * socialSharingVC = [[SocialSharingVC alloc]initWithNibName:@"SocialSharingVC" bundle:nil parameters:params];
    [self.navigationController pushViewController:socialSharingVC animated:YES];
    [socialSharingVC release];
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
	return @"";
}


- (NSString*)photoGallery:(FGalleryViewController*)gallery filePathForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return @"";
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery urlForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    
    return [[self.imagesWithLink objectAtIndex:index] objectForKey:@"thumb"];
}


#pragma mark - UIImagePicker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:NO completion:^{
       
        UIImage *selectedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        if (!selectedImage)
        {
            return;
        }
        
        self.profileImage = [[UIImageView alloc]init];
        self.profileImage.image=[selectedImage fixOrientation];
      
        NSMutableDictionary * params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.profileImage,@"image", nil];
        SocialSharingVC * socialSharingVC = [[SocialSharingVC alloc]initWithNibName:@"SocialSharingVC" bundle:nil parameters:params];
        [self.navigationController pushViewController:socialSharingVC animated:NO];
        [socialSharingVC release];
    }];
    
   
    /*TagPickerVC *tagPickerVC = [[TagPickerVC alloc]initWithNibName:@"TagPickerVC" bundle:nil parameters:params];
    [self.navigationController pushViewController:tagPickerVC animated:YES];
    [tagPickerVC release];*/
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
}
-(void) dealloc
{
    [super dealloc];
}

#pragma mark - Network Delegates
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([networkManager.identifier isEqualToString:@"getImagesFromInstagram"])
    {
        self.imagesWithLink = [[NSMutableArray alloc]init];
        NSArray * data = [[NSArray alloc]initWithArray:[object objectForKey:@"data"]];
        for(int i=0;i<data.count;i++)
        {
             if([[[data objectAtIndex:i]valueForKey:@"type"]isEqualToString:@"image"])
             {
                 NSDictionary * tempDic = [[NSDictionary alloc]initWithObjectsAndKeys:[[[[data objectAtIndex:i]objectForKey:@"images"]objectForKey:@"thumbnail"]valueForKey:@"url"],@"thumb",[[[[data objectAtIndex:i]objectForKey:@"images"]objectForKey:@"standard_resolution"]valueForKey:@"url"],@"standard", nil];
                 [self.imagesWithLink addObject:tempDic];
                 [tempDic release];
             }
        }
        networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
        [self.navigationController pushViewController:networkGallery animated:YES];
        [networkGallery release];
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


@end

