//
//  HomeVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "HomeVC.h"
#import "LogInVC.h"
#import "StartVC.h"
#import "GMGridView.h"
#import "UIImageView+WebCache.h"
#import "ALLEventsVC.h"
#import "AppDelegate.h"
#import "CreateUserVC.h"
#import "UserFeedVC.h"

@interface HomeVC ()<GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewTransformationDelegate, GMGridViewActionDelegate>
{
    __gm_weak GMGridView *_gmGridView;
}

@property (nonatomic,retain) IBOutlet UIButton *findpartyButton;
@property (nonatomic,retain) IBOutlet UIButton *userNameButton;

@property (nonatomic,retain) NSMutableArray *homeDataArray;

@property (nonatomic,retain) NetworkManager * networkManager;

@end

@implementation HomeVC

BOOL configLoaded;

-(void) dealloc
{
    [_findpartyButton release],_findpartyButton=nil;
    [_userNameButton release],_userNameButton=nil;
    
    [_homeDataArray release],_homeDataArray=nil;
    [_networkManager release],_networkManager=nil;
    [_gmGridView release],_gmGridView=nil;
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
                // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    self.findpartyButton.layer.cornerRadius=70;
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    [self initilizeGMGridView];
    [self.view bringSubviewToFront:_findpartyButton];
    [self.view bringSubviewToFront:_userNameButton];

    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.leftBarButtonItem=nil;
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA]);
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
//    if(data.count>0)
//    {
//        [self.userNameButton setTitle:[[data objectForKey:@"username"] capitalizedString] forState:UIControlStateNormal];
//        if(homeImagesLoaded)
//            [self loadHomePageDataForUser];
//    }
    if(!configLoaded)
    {
        StartVC *startVC=[[StartVC alloc] initWithNibName:@"StartVC" bundle:nil];
        startVC.delegate=self;
        startVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:startVC animated:NO];
        [startVC release];
    }
    else
    {
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
        if(data.count>0)
        {
            [self.userNameButton setTitle:[[data objectForKey:@"username"] capitalizedString] forState:UIControlStateNormal];
            if(homeImagesLoaded)
                //[self loadHomePageDataForUser];
            [self authenticateUser];
        }
        else
        {
            homeImagesLoaded=YES;
            [SharedAppManager sharedInstance].addUser = NO;
            [self performSelector:@selector(showCreateUserController) withObject:nil afterDelay:0.1];
        }
            
    }
}
#pragma mark - IBActions Started

-(IBAction)findPartiesClicked:(id)sender
{
    ALLEventsVC *allEventsVC=[[ALLEventsVC alloc] initWithNibName:@"ALLEventsVC" bundle:nil];
    [self.navigationController pushViewController:allEventsVC animated:YES];
    [allEventsVC release];
}

-(IBAction)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

#pragma mark - Custom methods Started 

+ (void)changeConfigBool
{
    configLoaded = NO;
}

-(void) setUpNavigationBar
{    
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
//    [titleView release];
}

-(void) initilizeGMGridView
{
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0, 0, 320, IS_IPHONE_5?449+screenDifference:449)];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style =  GMGridViewStyleNone;
    _gmGridView.itemSpacing = 0;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.sortingDelegate = self;
    _gmGridView.transformDelegate = self;
    _gmGridView.dataSource = self;
    _gmGridView.enableEditOnLongPress=NO;
    _gmGridView.editing=NO;
}

-(void) showCreateUserController
{
    if([SharedAppManager sharedInstance].addUser)
    {
//        CreateUserVC *createUserVC=[[CreateUserVC alloc] initWithNibName:@"CreateUserVC" bundle:nil];
//        createUserVC.hidesBottomBarWhenPushed=YES;
//        [self.navigationController pushViewController:createUserVC animated:YES];
//        //[SharedAppManager sharedInstance].addUser = NO;
//        [createUserVC release];
        LogInVC *logInVC=[[LogInVC alloc] initWithNibName:@"LogInVC" bundle:nil];
        logInVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:logInVC animated:YES];
        [logInVC release];
    }
    else
    {
        LogInVC *logInVC=[[LogInVC alloc] initWithNibName:@"LogInVC" bundle:nil];
        logInVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:logInVC animated:YES];
//        [logInVC release];
    }
    
}

-(void) authenticateUser
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/authenticate",[SharedAppManager sharedInstance].baseURL];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];

    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
   
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:[data objectForKey:@"username"] forKey:@"USERNAME"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PASSWORD] forKey:@"PASSWORD"];

    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"authenticateUser" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_networkManager sendRequestforAction:parameters];
}

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)imageWithImage:(UIImage*)sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height; // 161
    float scaleFactor = i_height / oldHeight; // 51/161 = 0.3167
    
    float newHeight =  oldHeight * scaleFactor;//sourceImage.size.height * scaleFactor; //54.87
    float newWidth =  sourceImage.size.width * scaleFactor;//oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


-(void) loadHomePageDataForUser
{
    if(homeImagesLoaded)
    {
        homeImagesLoaded=NO;
        NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
        NSString* urlString = [NSString stringWithFormat:@"%@api/photos/list/%@",[SharedAppManager sharedInstance].baseURL,[NSString stringWithFormat:@"%@",[data objectForKey:@"id"]]];
    
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
        NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
        [jsonWriter release];
    
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
        [parameters setObject:jsonString forKey:@"param"];
    
        NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
        [dictionary setObject:[data objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    
        [parameters setObject:dictionary forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"GET" forKey:@"callType"];
        [parameters setObject:@"listUser" forKey:@"idetntifier"];
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
        [_networkManager sendRequestforAction:parameters];
    }
}

#pragma mark - Delegate Started

#pragma mark - ConfigDownloaded Delegate Methods

-(void) configDownloadedWithResponse
{
    configLoaded=YES;
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN])
    {
        homeImagesLoaded=YES;
        [self performSelector:@selector(showCreateUserController) withObject:nil afterDelay:0.1];
        
    }
    else
    {        
        [self performSelector:@selector(authenticateUser) withObject:nil afterDelay:0.1];
    }
}

#pragma mark - NetworkMangerDelegate Delegate Methods


- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"authenticateUser"])
        {
            NSLog(@"%@",object);
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            homeImagesLoaded=YES;
            
            [self findPartiesClicked:nil];
           // [self loadHomePageDataForUser];
        }
        else if([networkManager.identifier isEqualToString:@"listUser"])
        {
            homeImagesLoaded = YES;
            self.homeDataArray=[[NSMutableArray alloc] initWithArray:[object objectForKey:@"data" ]];
            [_gmGridView reloadData];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];

    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_homeDataArray count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(170, 135);
        }
        else
        {
            return CGSizeMake(160, 110);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(285, 205);
        }
        else
        {
            return CGSizeMake(230, 175);
        }
    }
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[GMGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        cell.deleteButtonIcon = [UIImage imageNamed:@"close_x.png"];
        cell.deleteButtonOffset = CGPointMake(-15, -15);
        
        cell.backgroundColor=[UIColor clearColor];
        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
//        view.backgroundColor = [UIColor clearColor];
//        view.layer.masksToBounds = NO;
//        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
   
    NSString *imageUrl=[NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[_homeDataArray objectAtIndex:index] objectForKey:@"image_url"]];
    [cell.imageView setContentMode:UIViewContentModeScaleToFill];
    [cell.imageView setImageWithURL:[NSURL URLWithString:imageUrl]
    /*placeholderImage:[UIImage imageNamed:@""]*/];
    /*[cell.imageView setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:nil options:nil
                             success:^(UIImage *image, BOOL cached) {
                                 
                                 
                                 UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToHeight:110.0].CGImage];
                                 
                                 cell.imageView.image = scaledImage;
                                 cell.imageView.clipsToBounds = YES;
                                 [scaledImage release];
                             }
                             failure:^(NSError *error) {
                                 
                             }];*/

    
    
    return cell;
}


- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return YES; //index % 2 == 0;
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    NSLog(@"Did tap at index %d", position);
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want to delete this item?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
//    
//    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
//        [_currentData removeObjectAtIndex:_lastDeleteItemIndexAsked];
//        [_gmGridView removeObjectAtIndex:_lastDeleteItemIndexAsked withAnimation:GMGridViewItemAnimationFade];
    }
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewSortingDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    
//    [UIView animateWithDuration:0.3
//                          delay:0
//                        options:UIViewAnimationOptionAllowUserInteraction
//                     animations:^{
//                         cell.contentView.backgroundColor = [UIColor orangeColor];
//                         cell.contentView.layer.shadowOpacity = 0.7;
//                     }
//                     completion:nil
//     ];
}

- (void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
//    [UIView animateWithDuration:0.3
//                          delay:0
//                        options:UIViewAnimationOptionAllowUserInteraction
//                     animations:^{
//                         cell.contentView.backgroundColor = [UIColor redColor];
//                         cell.contentView.layer.shadowOpacity = 0;
//                     }
//                     completion:nil
//     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return NO;
}

- (void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
}

- (void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
}


//////////////////////////////////////////////////////////////
#pragma mark DraggableGridViewTransformingDelegate
//////////////////////////////////////////////////////////////

- (CGSize)GMGridView:(GMGridView *)gridView sizeInFullSizeForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index inInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(320, 210);
        }
        else
        {
            return CGSizeMake(160, 110);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(700, 530);
        }
        else
        {
            return CGSizeMake(600, 500);
        }
    }
}

- (UIView *)GMGridView:(GMGridView *)gridView fullSizeViewForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    UIView *fullView = [[UIView alloc] init];
    fullView.backgroundColor = [UIColor clearColor];
    fullView.layer.masksToBounds = NO;
    fullView.layer.cornerRadius = 0;
    
    CGSize size = [self GMGridView:gridView sizeInFullSizeForCell:cell atIndex:index inInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    fullView.bounds = CGRectMake(0, 0, size.width, size.height);
    
//    UILabel *label = [[UILabel alloc] initWithFrame:fullView.bounds];
//    label.text = [NSString stringWithFormat:@"Fullscreen View for cell at index %d", index];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.backgroundColor = [UIColor clearColor];
//    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    
//    if (INTERFACE_IS_PHONE)
//    {
//        label.font = [UIFont boldSystemFontOfSize:15];
//    }
//    else
//    {
//        label.font = [UIFont boldSystemFontOfSize:20];
//    }
//    
//    [fullView addSubview:label];
    
    
    return fullView;
}

- (void)GMGridView:(GMGridView *)gridView didStartTransformingCell:(GMGridViewCell *)cell
{
//    [UIView animateWithDuration:0.5
//                          delay:0
//                        options:UIViewAnimationOptionAllowUserInteraction
//                     animations:^{
//                         cell.contentView.backgroundColor = [UIColor blueColor];
//                         cell.contentView.layer.shadowOpacity = 0.7;
//                     }
//                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEndTransformingCell:(GMGridViewCell *)cell
{
//    [UIView animateWithDuration:0.5
//                          delay:0
//                        options:UIViewAnimationOptionAllowUserInteraction
//                     animations:^{
//                         cell.contentView.backgroundColor = [UIColor redColor];
//                         cell.contentView.layer.shadowOpacity = 0;
//                     }
//                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEnterFullSizeForCell:(UIView *)cell
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
