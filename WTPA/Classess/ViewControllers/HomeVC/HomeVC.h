//
//  HomeVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartVC.h"
@interface HomeVC : UIViewController <ConfigResponse,NetworkManagerDelegate>
{
//    BOOL configLoaded;
    BOOL homeImagesLoaded;
}

@property (nonatomic,assign) BOOL configLoaded;

+ (void)changeConfigBool;
+ (void)changeImageLoadedBool;

@end
