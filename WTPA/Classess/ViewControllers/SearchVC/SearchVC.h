//
//  SearchVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 04/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>

@interface SearchVC : UIViewController<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}


@end
