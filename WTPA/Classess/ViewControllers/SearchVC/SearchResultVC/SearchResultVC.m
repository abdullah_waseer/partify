//
//  SearchResultVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SearchResultVC.h"
#import "EventDetailVC.h"

@interface SearchResultVC ()<NetworkManagerDelegate>


@property (nonatomic,retain) NetworkManager * networkManager;
@property (nonatomic,retain) NSMutableArray * resultsArray;

@property (nonatomic,retain) NSMutableDictionary * searchDictionary;

@property (nonatomic,retain) IBOutlet UITableView * resultsTableView;

@property (nonatomic,retain) IBOutlet UILabel * dateLabel;
@property (nonatomic,retain) IBOutlet UILabel * locationLabel;
@property (nonatomic,retain) IBOutlet UILabel * keywordlabel;

@end

@implementation SearchResultVC

-(void) dealloc
{
    [_networkManager release],_networkManager=nil;
    [_resultsArray release],_resultsArray=nil;
    [_searchDictionary release],_searchDictionary=nil;
    [_resultsTableView  release],_resultsTableView=nil;
    
    [_dateLabel  release],_dateLabel=nil;
    [_locationLabel  release],_locationLabel=nil;
    [_keywordlabel  release],_keywordlabel=nil;
    
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andSearchDictionary:(NSMutableDictionary *) dictionary
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.searchDictionary=[[NSMutableDictionary alloc] initWithDictionary:dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setUpNavigationBar];
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    self.locationLabel.text=[NSString stringWithFormat:@"%@",_searchDictionary[@"location"]?_searchDictionary[@"location"]:@""];
    
    self.keywordlabel.text=[NSString stringWithFormat:@"%@",_searchDictionary[@"keyword"]?_searchDictionary[@"keyword"]:@""];
    self.dateLabel.text=[NSString stringWithFormat:@"%@",[[SharedAppManager sharedInstance]  returnDateAfterForamting:_searchDictionary[@"startdate"]]];
    
    if([[_searchDictionary objectForKey:@"event_type"] isEqualToString:@"all"])
        [self performSelector:@selector(makeSearchCallWithSelectedparameters) withObject:nil afterDelay:0.1];
    else if([[_searchDictionary objectForKey:@"event_type"] isEqualToString:@"following"])
        [self performSelector:@selector(makeSearchCallWithSelectedparametersFollowing) withObject:nil afterDelay:0.1];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Custom methods Started

-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) makeSearchCallWithSelectedparameters
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/events/searchall",[SharedAppManager sharedInstance].baseURL];

    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"long"]?_searchDictionary[@"long"]:@""] forKey:@"long"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"lat"]?_searchDictionary[@"lat"]:@""] forKey:@"lat"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"startdate"]?[[SharedAppManager sharedInstance] returnDateAfterForamting: _searchDictionary[@"startdate"]]:@""] forKey:@"date"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"keyword"]?_searchDictionary[@"keyword"]:@""] forKey:@"keyword"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"distance"]?_searchDictionary[@"distance"]:@"100"] forKey:@"distance"];
    //[param setObject:[NSUs] forKey:@"LoginId"];
    [param setObject:@"" forKey:@"page"];
    [param setObject:@"" forKey:@"tag"];
    [param setObject:@"" forKey:@"festivalsonly"];
    
    [param setObject:[[NSString stringWithFormat:@"%@",_searchDictionary[@"location"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"location"];
    
    [param setObject:[NSString stringWithFormat:@"%d",100] forKey:@"limit"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    [dictionary setObject:jsonString forKey:@"param"];
    [dictionary setObject:AUTH_VALUE forKey:AUTH_KEY];
    
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"searchResult" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

-(void) makeSearchCallWithSelectedparametersFollowing
{
    NSString* urlString = [NSString stringWithFormat:@"%@api/events/searchEventsFollowedByUser",[SharedAppManager sharedInstance].baseURL];
    
    NSMutableDictionary *param=[NSMutableDictionary dictionary];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"long"]?_searchDictionary[@"long"]:@""] forKey:@"long"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"lat"]?_searchDictionary[@"lat"]:@""] forKey:@"lat"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"startdate"]?[[SharedAppManager sharedInstance] returnDateAfterForamting: _searchDictionary[@"startdate"]]:@""] forKey:@"date"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"keyword"]?_searchDictionary[@"keyword"]:@""] forKey:@"keyword"];
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"distance"]?_searchDictionary[@"distance"]:@"300"] forKey:@"distance"];
    //[param setObject:[NSUs] forKey:@"LoginId"];
    [param setObject:@"" forKey:@"page"];
    [param setObject:@"" forKey:@"tag"];
    [param setObject:@"" forKey:@"festivalsonly"];
    
    [param setObject:[NSString stringWithFormat:@"%@",_searchDictionary[@"location"]] forKey:@"location"];
    
    [param setObject:[NSString stringWithFormat:@"%d",100] forKey:@"limit"];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonString = [jsonWriter stringWithObject:param];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-type"];
    [dictionary setObject:jsonString forKey:@"param"];
    
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"searchResultFollowing" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}


#pragma mark - Delegates Statrt

#pragma mark - UITableView Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _resultsArray.count;
}

//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//}

-(CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 1;
}

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 44;
}


//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//
//
//    NSMutableArray *tempArray = [[[NSMutableArray alloc] init] autorelease];
//    [tempArray addObject:@"A"];
//    [tempArray addObject:@"B"];
//    [tempArray addObject:@"C"];
//    [tempArray addObject:@"D"];
//    [tempArray addObject:@"E"];
//    [tempArray addObject:@"F"];
//    [tempArray addObject:@"G"];
//    [tempArray addObject:@"H"];
//    [tempArray addObject:@"I"];
//    [tempArray addObject:@"J"];
//    [tempArray addObject:@"K"];
//    [tempArray addObject:@"L"];
//    [tempArray addObject:@"M"];
//    [tempArray addObject:@"N"];
//    [tempArray addObject:@"O"];
//    [tempArray addObject:@"P"];
//    [tempArray addObject:@"Q"];
//    [tempArray addObject:@"R"];
//    [tempArray addObject:@"S"];
//    [tempArray addObject:@"T"];
//    [tempArray addObject:@"U"];
//    [tempArray addObject:@"V"];
//    [tempArray addObject:@"W"];
//    [tempArray addObject:@"X"];
//    [tempArray addObject:@"Y"];
//    [tempArray addObject:@"Z"];
//
//    return tempArray;
//}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *HeaderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    HeaderView.backgroundColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1];
    return HeaderView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSLog(@"%@",[_resultsArray objectAtIndex:indexPath.row]);
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@",[[_resultsArray objectAtIndex:indexPath.row] objectForKey:@"title"]];
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",[[_resultsArray objectAtIndex:indexPath.row] objectForKey:@"startDate"]];
    cell.accessibilityIdentifier = [[[_resultsArray objectAtIndex:indexPath.row] objectForKey:@"venue"] objectForKey:@"venu_id"];
    return cell;
}


#pragma mark UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    EventDetailVC *eventDetailVC=[[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:[_resultsArray objectAtIndex:indexPath.row]];
    eventDetailVC.venueId = cell.accessibilityIdentifier;
    eventDetailVC.eventSource = [[self.resultsArray objectAtIndex:indexPath.row] objectForKey:@"source"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}


#pragma mark - Network Delegates
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"searchResult"])
        {
            _resultsArray = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
            if([_resultsArray count]>0)
            {
                _resultsArray=[[NSMutableArray alloc] initWithArray:[object objectForKey:@"data"]];
                [_resultsTableView reloadData];
            }
            else
            {
                [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No event found!"];
            }
        }
        else if([networkManager.identifier isEqualToString:@"searchResultFollowing"])
        {
            _resultsArray = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
            if([_resultsArray count]>0)
            {
                _resultsArray=[[NSMutableArray alloc] initWithArray:[object objectForKey:@"data"]];
                [_resultsTableView reloadData];
            }
            else
            {
                [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No event found!"];
            }
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"No event found!"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
