//
//  SearchVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 04/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SearchVC.h"
#import "SearchResultVC.h"
#import "SelectCategoryPopupView.h"
#import "NSDate-Utilities.h"
#import "NSString+SBJSON.h"
#import "TagPickerVC.h"
#import "MLPAutoCompleteTextField.h"
#import "DEMOCustomAutoCompleteCell.h"
#import "DEMOCustomAutoCompleteObject.h"

@interface SearchVC ()<SelectCategoryPopupViewDelegate,tagPickerVCDelegate,UITextFieldDelegate, MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate,NetworkManagerDelegate>

@property (nonatomic,retain) SelectCategoryPopupView *datePickerView;
@property (nonatomic,retain) IBOutlet UIButton *startDateBtn;
@property (nonatomic,retain) IBOutlet UIButton *tagLocationBtn;
@property (nonatomic,retain) IBOutlet UIButton *withInBtn;
@property (nonatomic,retain) IBOutlet UIButton *searchBtn;
@property (nonatomic,retain) IBOutlet UIButton *eventType;


@property (nonatomic,retain) IBOutlet UIImageView *locationBackImage;
@property (nonatomic,retain) IBOutlet UIImageView *keyWordbackIamge;


@property (assign) BOOL simulateLatency;
@property (assign) BOOL testWithAutoCompleteObjectsInsteadOfStrings;
@property (strong, nonatomic) NSMutableArray *countryObjects;
@property (strong, nonatomic) NSMutableArray *countryArray;

@property (nonatomic,retain) IBOutlet MLPAutoCompleteTextField *keyWordField;
@property (nonatomic,retain) IBOutlet UITextField *txtKeyWord;

@property (nonatomic,retain) NSMutableDictionary *dataDictionary;

@property (nonatomic,retain) NetworkManager * networkManager;
@property (nonatomic) BOOL cityLoaded;

@end

@implementation SearchVC

-(void) dealloc
{
    [_startDateBtn release],_startDateBtn=nil;
    [_searchBtn release],_searchBtn=nil;
    [_datePickerView release],_datePickerView=nil;
    [_tagLocationBtn release],_tagLocationBtn=nil;
    [_withInBtn release],_withInBtn=nil;
    [_countryObjects release],_countryObjects=nil;
    [_countryArray release],_countryArray=nil;
    [_keyWordField release],_keyWordField=nil;
    [_txtKeyWord release],_txtKeyWord=nil;
    [_dataDictionary release],_dataDictionary=nil;
    [_networkManager release],_networkManager=nil;
    [_locationBackImage release],_locationBackImage=nil;
    [_keyWordbackIamge release],_keyWordbackIamge=nil;
    [_eventType release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    self.cityLoaded=NO;
    self.eventType.selected=NO;
    
//    self.searchBtn.layer.borderColor=[UIColor colorWithRed:71.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1].CGColor;
//    self.searchBtn.layer.borderWidth=1;
//    self.searchBtn.layer.cornerRadius=5;
    
    [self initilizeLocationObject];
//    if ([_txtKeyWord respondsToSelector:@selector(setAttributedPlaceholder:)]) {
//        UIColor *color = [UIColor whiteColor];
//        _txtKeyWord.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Keyword (e.g Hell fire ,Fun)" attributes:@{NSForegroundColorAttributeName: color ,NSFontAttributeName:[UIFont fontWithName:@"Thonburi" size:17]}];
//    }
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    self.dataDictionary=[NSMutableDictionary dictionary];
    
    [self.dataDictionary setObject:[NSDate date] forKey:@"startdate"];
    
    [_startDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:[NSDate date]] forState:UIControlStateNormal];
    [self autoCompleteTextFiled];
    [self setUpNavigationBar];
    
    [self hideShowViews:YES];
    
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - Custom methods Started

-(void) initilizeLocationObject
{
    if(locationManager)
        [locationManager release],locationManager=nil;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please enable loaction services and try again"];
    }
}


-(void) hideShowViews:(BOOL) hide
{
    _txtKeyWord.hidden=hide;
    _keyWordField.hidden=hide;
   // _withInBtn.hidden=hide;
    _startDateBtn.hidden=hide;
    _tagLocationBtn.hidden=hide;
    _searchBtn.hidden=hide;
    _locationBackImage.hidden=hide;
    _keyWordbackIamge.hidden=hide;
    _eventType.hidden = hide;
}

-(void) autoCompleteTextFiled
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShowWithNotification:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHideWithNotification:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.keyWordField setBorderStyle:UITextBorderStyleNone];
    self.keyWordField.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    if ([_keyWordField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        _keyWordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Location" attributes:@{NSForegroundColorAttributeName: color ,NSFontAttributeName:[UIFont fontWithName:@"Thonburi" size:17]}];
    }
    _keyWordField.autoCompleteTableBackgroundColor=[UIColor groupTableViewBackgroundColor];
    _keyWordField.autoCompleteTableCellTextColor=[UIColor blackColor];
    
}

- (void)keyboardDidShowWithNotification:(NSNotification *)aNotification
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         CGPoint adjust;
                         switch (self.interfaceOrientation) {
                             case UIInterfaceOrientationLandscapeLeft:
                                 adjust = CGPointMake(-110, 0);
                                 break;
                             case UIInterfaceOrientationLandscapeRight:
                                 adjust = CGPointMake(110, 0);
                                 break;
                             default:
                                 adjust = CGPointMake(0, -60);
                                 break;
                         }
                         CGPoint newCenter = CGPointMake(self.view.center.x+adjust.x, self.view.center.y+adjust.y);
                         [self.view setCenter:newCenter];
                         
                     }
                     completion:nil];
}


- (void)keyboardDidHideWithNotification:(NSNotification *)aNotification
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         CGPoint adjust;
                         switch (self.interfaceOrientation) {
                             case UIInterfaceOrientationLandscapeLeft:
                                 adjust = CGPointMake(110, 0);
                                 break;
                             case UIInterfaceOrientationLandscapeRight:
                                 adjust = CGPointMake(-110, 0);
                                 break;
                             default:
                                 adjust = CGPointMake(0, 60);
                                 break;
                         }
                         CGPoint newCenter = CGPointMake(self.view.center.x+adjust.x, self.view.center.y+adjust.y);
                         [self.view setCenter:newCenter];
                     }
                     completion:nil];
    
    
    [self.keyWordField setAutoCompleteTableViewHidden:NO];
}


-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
}
-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (BOOL)ifContainsPicker
{
    BOOL containsIntrmdtView = [[UIApplication sharedApplication].keyWindow.subviews  containsObject:_datePickerView];
    return containsIntrmdtView;
}

- (void)showPopover:(id)sender
{
    if ([self ifContainsPicker])
    {
        [_datePickerView setDelegate:nil];
        [_datePickerView removeFromSuperview];
    }
    
    CGRect theFrame = CGRectMake(0,  380 + (IS_IPHONE_5?screenDifference:0.0)-(155), 320, 216+42);
    _datePickerView = [[SelectCategoryPopupView alloc] initWithFrame:theFrame withArray:nil withSelectedCategory:@"" withPickerSelected:YES withOnlyTimeMode:NO andDelegate:self];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_datePickerView];
    [_datePickerView show];
    
}

-(void) resignFirstResponserField
{
    [_keyWordField resignFirstResponder];
    [_txtKeyWord resignFirstResponder];
}

-(void) loadCities
{
    NSString* urlString = [NSString stringWithFormat:@"%@cities.json",[SharedAppManager sharedInstance].baseURL];
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"cities" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [_networkManager sendRequestforAction:parameters];
}

-(BOOL) isValidated
{
    if(![self.dataDictionary objectForKey:@"long"]|| ![self.dataDictionary objectForKey:@"location"])
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please Select Location"];
        return NO;
    }
    else if (self.txtKeyWord.text.length<=0)
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please enter keyword"];
        return NO;
    }
    return YES;
}

- (void)getAddressesWithLatitude:(NSMutableDictionary *)geoData
{
    NSString *latLongCombined = [NSString stringWithFormat:@"%@,%@",[geoData objectForKey:@"lat"],[geoData objectForKey:@"long"]];
    
    NSString *strURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@&sensor=true", latLongCombined];
    strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL* urlAddress = [NSURL URLWithString:strURL];
    NSURLRequest* request = [NSURLRequest requestWithURL:urlAddress];
    
    
    [NSURLConnection  sendAsynchronousRequest:request
                                        queue:[NSOperationQueue mainQueue]
                            completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                
                                NSString* responseString = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease];
                                // //NSLog(@"Response: %@",responseString);
                                NSDictionary* jsonDict = [[responseString JSONValue]copy];
                                
                                if ([[jsonDict objectForKey:@"status"]isEqualToString:@"OK"] && [[jsonDict objectForKey:@"results"] count] >0)
                                {
                                    NSArray *results = [[NSArray alloc] initWithArray:[jsonDict objectForKey:@"results"]];
                                    
                                    BOOL isFound=NO;
                    
                                    for(int i=0;i<results.count;i++)
                                    {
                                      
                                        if([self getAddressFrom:[[results objectAtIndex:i] objectForKey:@"address_components"] afterCompare:@"locality" withGeoData:geoData] )
                                            {
                                                isFound=YES;
                                                [self performSelectorOnMainThread:@selector(loadCities) withObject:nil waitUntilDone:NO];
                
                                                break;
                                            }
                                    }
                                    if(!isFound)
                                    {
                                        for(int i=0;i<results.count;i++)
                                        {
                                        
                                            if([self getAddressFrom:[[results objectAtIndex:i] objectForKey:@"address_components"] afterCompare:@"postal_town" withGeoData:geoData] )
                                            {
                                                isFound=YES;
                                                [self performSelectorOnMainThread:@selector(loadCities) withObject:nil waitUntilDone:NO];
                                            
                                                break;
                                            }
                                        }
                                    }
                                    if(!isFound)
                                    {
                                        for(int i=0;i<results.count;i++)
                                        {
                                        
                                            if([self getAddressFrom:[[results objectAtIndex:i] objectForKey:@"address_components"] afterCompare:@"administrative_area_level_2" withGeoData:geoData] )
                                            {
                                                isFound=YES;
                                                [self performSelectorOnMainThread:@selector(loadCities) withObject:nil waitUntilDone:NO];
                                            
                                                break;
                                            }
                                        }
                                    }
                                    if(!isFound)
                                    {
                                        NSMutableDictionary *geoData=[NSMutableDictionary dictionary];
                                        [geoData setObject:[NSString stringWithFormat:@"Current Location (%@)",
                                                            @" "] forKey:@"city"];
                                        [geoData setObject:@"" forKey:@"country"];
                                        [geoData setObject:@"00" forKey:@"id"];
                                        
                                        self.keyWordField.text = [NSString stringWithFormat:@"%@",
                                                                 @""];
                                        self.countryArray=[[NSMutableArray alloc] initWithObjects:geoData, nil];
                                    }
                                }
                                else
                                {
                                    [self performSelector:@selector(getAddressesWithLatitude:) withObject:geoData];
                                }
                            }];
}

-(BOOL) getAddressFrom :(NSArray *) address afterCompare:(NSString *) compare withGeoData:(NSMutableDictionary*) geoData
{
    BOOL found=NO;
    for (int i=0;i<address.count;i++)
    {
        if([[[[address objectAtIndex:i] objectForKey:@"types"] objectAtIndex:0] isEqualToString:compare]) //city
        {
            found=YES;
            [geoData setObject:[NSString stringWithFormat:@"Current Location (%@)",
                                [[address objectAtIndex:i] objectForKey:@"long_name"]] forKey:@"city"];
            [geoData setObject:@"" forKey:@"country"];
            [geoData setObject:@"00" forKey:@"id"];
            
            self.keyWordField.text = [NSString stringWithFormat:@"%@",
                                      [[address objectAtIndex:i] objectForKey:@"long_name"]];
            
            self.countryArray=[[NSMutableArray alloc] initWithObjects:geoData, nil];
            
            [self.dataDictionary setObject:[[self.countryArray objectAtIndex:0] objectForKey:@"long"] forKey:@"long"];
            
            [self.dataDictionary setObject:[[self.countryArray objectAtIndex:0] objectForKey:@"lat"] forKey:@"lat"];
            [self.dataDictionary setObject:[[address objectAtIndex:i] objectForKey:@"long_name"] forKey:@"location"];
            break;
        }
    }
    return  found;
}


#pragma mark - IBActions Started

- (IBAction)hideKeyboard:(id)sender
{
    [_keyWordField resignFirstResponder];
    [_txtKeyWord resignFirstResponder];
}

- (IBAction)eventTypeClicked:(id)sender
{
    _eventType.selected=!_eventType.selected;
}

-(IBAction)searClicked:(id)sender
{
    [self resignFirstResponserField];
    
    //if([self isValidated])
    //{
        [self.dataDictionary setObject:self.txtKeyWord.text forKey:@"keyword"];
        if(!self.eventType.selected)
           [self.dataDictionary setObject:@"all" forKey:@"event_type"];
        else
           [self.dataDictionary setObject:@"following" forKey:@"event_type"];
        SearchResultVC *searchresultsVC=[[SearchResultVC alloc] initWithNibName:@"SearchResultVC" bundle:nil andSearchDictionary:self.dataDictionary];
        [self.navigationController pushViewController:searchresultsVC animated:YES];
        [searchresultsVC release];
    //}
    //    else
    //    {
    //        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please Select Location"];
    //    }
}
-(IBAction)startDatePickerClicked:(id)sender
{
    [self resignFirstResponserField];
    [self showPopover:sender];
}

-(IBAction)tagLocationClicked:(id)sender
{
    [self resignFirstResponserField];
    
    TagPickerVC * tagPickerVC = [[TagPickerVC alloc]initWithNibName:@"TagPickerVC" bundle:nil];
    
    tagPickerVC.delegate = self;
    
    [self.navigationController pushViewController:tagPickerVC animated:YES];
    
    [tagPickerVC release];
}

-(IBAction)withInKmClicked:(id)sender
{
    [self resignFirstResponserField];
    if ([self ifContainsPicker])
    {
        [_datePickerView setDelegate:nil];
        [_datePickerView removeFromSuperview];
    }
    NSMutableArray *array=[[NSMutableArray alloc] initWithObjects:@"3 km", @"6 km",@"9 km", @"12 km", @"15 km",@"25 km",@"50 km",@"100 km",@"500 km",nil];
    
    CGRect theFrame = CGRectMake(0,  380 + (IS_IPHONE_5?screenDifference:0.0)-(155), 320, 216+42);
    _datePickerView = [[SelectCategoryPopupView alloc] initWithFrame:theFrame withArray:array withSelectedCategory:@"" withPickerSelected:NO withOnlyTimeMode:NO andDelegate:self];
    
    [[UIApplication sharedApplication].keyWindow addSubview:_datePickerView];
    [_datePickerView show];
    
}

#pragma mark - Delegates Statrt

#pragma mark - Network Delegates
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    //    {
    if([networkManager.identifier isEqualToString:@"cities"])
    {
        [self.countryArray addObjectsFromArray:object];
        [self hideShowViews:NO];
    }
    //    }
    //    else
    //    {
    //        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:ALERTVIEW_TITLE message:@"Problem in loading cities" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",nil];
    //
    //        alert.tag=11111;
    //        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    //        [alert release];
    //    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - TagPickerVC Delegate Methods

- (void) tagPickerVC:(TagPickerVC *)tagPickerVc withLocation:(NSMutableDictionary *)data
{
    [self.tagLocationBtn setTitle:[NSString stringWithFormat:@"%@",data[@"name"]] forState:UIControlStateNormal];
    
    [self.dataDictionary setObject:[NSString stringWithFormat:@"%@",data[@"name"]] forKey:@"location"];
}


#pragma mark - SelectCategoryPopupView Delegate Methods

- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedDateDidSave:(NSDate*)value
{
	if(value != nil)
	{
        if(![value isEarlierThanDate:[NSDate date]])
        {
            [_startDateBtn setTitle:[[SharedAppManager sharedInstance] returnDateAfterForamting:value] forState:UIControlStateNormal];
            [self.dataDictionary setObject:value forKey:@"startdate"];
        }
        else
        {
            [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Please select a valid date"];
        }
    }
    
    self.navigationController.hidesBottomBarWhenPushed = NO;
    [self.datePickerView hide];
}

- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedValueDidSave:(NSString*)value withIndex:(NSInteger)selectedIndex
{
    if(value)
    {
        [self.withInBtn setTitle:value forState:UIControlStateNormal];
        [self.dataDictionary setObject:value forKey:@"within"];
    }
    self.navigationController.hidesBottomBarWhenPushed = NO;
    [self.datePickerView hide];
    
}

#pragma mark - Location Manager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSMutableDictionary *latLong=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],@"lat",[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude],@"long",nil];
    if(!_cityLoaded)
    {
        _cityLoaded=YES;
        [self getAddressesWithLatitude:latLong];
    }
    
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSString *errorType = (error.code == kCLErrorDenied) ? @"Please enable location services to use this feature" : @"Unknown Error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error getting Location"
                                                    message:errorType delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.tag=22222;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    [alert release];
}


#pragma mark - UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - MLPAutoCompleteTextField DataSource


//example of asynchronous fetch:
- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        if(self.simulateLatency){
            CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution©
            sleep(seconds);
        }
        
        NSArray *completions;
        if(self.testWithAutoCompleteObjectsInsteadOfStrings){
            completions = [self allCountryObjects];
        } else {
            completions = [self allCountryObjects];
        }
        
        handler(completions);
    });
}
- (NSArray *)allCountryObjects
{
    if(!self.countryObjects){
        NSMutableArray *mutableCountries = [NSMutableArray new];
        for (int i=0;i<self.countryArray.count ;i++)
        {
            NSString *countryName=[NSString stringWithFormat:@"%@, %@",[[_countryArray objectAtIndex:i] objectForKey:@"city"],[[_countryArray objectAtIndex:i] objectForKey:@"country"]];
            DEMOCustomAutoCompleteObject *country = [[DEMOCustomAutoCompleteObject alloc] initWithCountry:i==0?[[_countryArray objectAtIndex:i] objectForKey:@"city"]:countryName];
            [mutableCountries addObject:country];
        }
        [self setCountryObjects:[NSMutableArray arrayWithArray:mutableCountries]];
    }
    
    return self.countryObjects;
}


- (NSArray *)allCountries
{
    NSArray *countries =
    @[
      @"Abkhazia",
      @"Afghanistan",
      @"Aland",
      @"Albania",
      @"Algeria",
      @"American Samoa",
      @"Andorra",
      @"Angola",
      @"Anguilla",
      @"Antarctica",
      @"Antigua & Barbuda",
      @"Argentina",
      @"Armenia",
      @"Aruba",
      @"Australia",
      @"Austria",
      @"Azerbaijan",
      @"Bahamas",
      @"Bahrain",
      @"Bangladesh",
      @"Barbados",
      @"Belarus",
      @"Belgium",
      @"Belize",
      @"Benin",
      @"Bermuda",
      @"Bhutan",
      @"Bolivia",
      @"Bosnia & Herzegovina",
      @"Botswana",
      @"Brazil",
      @"British Antarctic Territory",
      @"British Virgin Islands",
      @"Brunei",
      @"Bulgaria",
      @"Burkina Faso",
      @"Burundi",
      @"Cambodia",
      @"Cameroon",
      @"Canada",
      @"Cape Verde",
      @"Cayman Islands",
      @"Central African Republic",
      @"Chad",
      @"Chile",
      @"China",
      @"Christmas Island",
      @"Cocos Keeling Islands",
      @"Colombia",
      @"Commonwealth",
      @"Comoros",
      @"Cook Islands",
      @"Costa Rica",
      @"Cote d'Ivoire",
      @"Croatia",
      @"Cuba",
      @"Cyprus",
      @"Czech Republic",
      @"Democratic Republic of the Congo",
      @"Denmark",
      @"Djibouti",
      @"Dominica",
      @"Dominican Republic",
      @"East Timor",
      @"Ecuador",
      @"Egypt",
      @"El Salvador",
      @"England",
      @"Equatorial Guinea",
      @"Eritrea",
      @"Estonia",
      @"Ethiopia",
      @"European Union",
      @"Falkland Islands",
      @"Faroes",
      @"Fiji",
      @"Finland",
      @"France",
      @"Gabon",
      @"Gambia",
      @"Georgia",
      @"Germany",
      @"Ghana",
      @"Gibraltar",
      @"GoSquared",
      @"Greece",
      @"Greenland",
      @"Grenada",
      @"Guam",
      @"Guatemala",
      @"Guernsey",
      @"Guinea Bissau",
      @"Guinea",
      @"Guyana",
      @"Haiti",
      @"Honduras",
      @"Hong Kong",
      @"Hungary",
      @"Iceland",
      @"India",
      @"Indonesia",
      @"Iran",
      @"Iraq",
      @"Ireland",
      @"Isle of Man",
      @"Israel",
      @"Italy",
      @"Jamaica",
      @"Japan",
      @"Jersey",
      @"Jordan",
      @"Kazakhstan",
      @"Kenya",
      @"Kiribati",
      @"Kosovo",
      @"Kuwait",
      @"Kyrgyzstan",
      @"Laos",
      @"Latvia",
      @"Lebanon",
      @"Lesotho",
      @"Liberia",
      @"Libya",
      @"Liechtenstein",
      @"Lithuania",
      @"Luxembourg",
      @"Macau",
      @"Macedonia",
      @"Madagascar",
      @"Malawi",
      @"Malaysia",
      @"Maldives",
      @"Mali",
      @"Malta",
      @"Mars",
      @"Marshall Islands",
      @"Mauritania",
      @"Mauritius",
      @"Mayotte",
      @"Mexico",
      @"Micronesia",
      @"Moldova",
      @"Monaco",
      @"Mongolia",
      @"Montenegro",
      @"Montserrat",
      @"Morocco",
      @"Mozambique",
      @"Myanmar",
      @"Nagorno Karabakh",
      @"Namibia",
      @"NATO",
      @"Nauru",
      @"Nepal",
      @"Netherlands Antilles",
      @"Netherlands",
      @"New Caledonia",
      @"New Zealand",
      @"Nicaragua",
      @"Niger",
      @"Nigeria",
      @"Niue",
      @"Norfolk Island",
      @"North Korea",
      @"Northern Cyprus",
      @"Northern Mariana Islands",
      @"Norway",
      @"Olympics",
      @"Oman",
      @"Pakistan",
      @"Palau",
      @"Palestine",
      @"Panama",
      @"Papua New Guinea",
      @"Paraguay",
      @"Peru",
      @"Philippines",
      @"Pitcairn Islands",
      @"Poland",
      @"Portugal",
      @"Puerto Rico",
      @"Qatar",
      @"Red Cross",
      @"Republic of the Congo",
      @"Romania",
      @"Russia",
      @"Rwanda",
      @"Saint Barthelemy",
      @"Saint Helena",
      @"Saint Kitts & Nevis",
      @"Saint Lucia",
      @"Saint Vincent & the Grenadines",
      @"Samoa",
      @"San Marino",
      @"Sao Tome & Principe",
      @"Saudi Arabia",
      @"Scotland",
      @"Senegal",
      @"Serbia",
      @"Seychelles",
      @"Sierra Leone",
      @"Singapore",
      @"Slovakia",
      @"Slovenia",
      @"Solomon Islands",
      @"Somalia",
      @"Somaliland",
      @"South Africa",
      @"South Georgia & the South Sandwich Islands",
      @"South Korea",
      @"South Ossetia",
      @"South Sudan",
      @"Spain",
      @"Sri Lanka",
      @"Sudan",
      @"Suriname",
      @"Swaziland",
      @"Sweden",
      @"Switzerland",
      @"Syria",
      @"Taiwan",
      @"Tajikistan",
      @"Tanzania",
      @"Thailand",
      @"Togo",
      @"Tonga",
      @"Trinidad & Tobago",
      @"Tunisia",
      @"Turkey",
      @"Turkmenistan",
      @"Turks & Caicos Islands",
      @"Tuvalu",
      @"Uganda",
      @"Ukraine",
      @"United Arab Emirates",
      @"United Kingdom",
      @"United Nations",
      @"United States",
      @"Uruguay",
      @"US Virgin Islands",
      @"Uzbekistan",
      @"Vanuatu",
      @"Vatican City",
      @"Venezuela",
      @"Vietnam",
      @"Wales",
      @"Western Sahara",
      @"Yemen",
      @"Zambia",
      @"Zimbabwe"
      ];
    
    return countries;
}


/*
 - (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
 {
 
 if(self.simulateLatency){
 CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution
 NSLog(@"sleeping fetch of completions for %f", seconds);
 sleep(seconds);
 }
 
 NSArray *completions;
 if(self.testWithAutoCompleteObjectsInsteadOfStrings){
 completions = [self allCountryObjects];
 } else {
 completions = [self allCountries];
 }
 
 return completions;
 }
 */
- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    //    NSString *filename = [autocompleteString stringByAppendingString:@".png"];
    //    filename = [filename stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    //    filename = [filename stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    //    [cell.imageView setImage:[UIImage imageNamed:filename]];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array= [[selectedObject autocompleteString] componentsSeparatedByString:@","];
    
    for (int i=0;i<self.countryArray.count;i++)
    {
        if(i==0 && [AppUtils ifString:@"Current Location" isFoundInString:[array objectAtIndex:0]])
        {
            NSArray *arrayText=[[array objectAtIndex:0] componentsSeparatedByString:@"("];
            
            textField.text=[NSString stringWithFormat:@"%@",[[ arrayText objectAtIndex:1] stringByReplacingOccurrencesOfString:@")" withString:@""]];
            
            [self.dataDictionary setObject:[[self.countryArray objectAtIndex:i] objectForKey:@"long"] forKey:@"long"];
            
            [self.dataDictionary setObject:[[self.countryArray objectAtIndex:i] objectForKey:@"lat"] forKey:@"lat"];
            [self.dataDictionary setObject:@"" forKey:@"location"];
            
            break;
        }
        else if([AppUtils ifString:[array objectAtIndex:0] isFoundInString:[[self.countryArray objectAtIndex:i] objectForKey:@"city"]] )
        {
            [self.dataDictionary setObject:[[self.countryArray objectAtIndex:i] objectForKey:@"city"] forKey:@"location"];
            [self.dataDictionary setObject:@"" forKey:@"long"];
            
            [self.dataDictionary setObject:@"" forKey:@"lat"];
            break;
        }
    }
}

#pragma mark - UIAlertView Delegate Methods

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    if(alertView.tag==11111)
    //    {
    //        if(buttonIndex==1)
    //        {
    //            [self performSelector:@selector(loadCities)];
    //        }
    //    }
    //    else if(alertView.tag==22222)
    //    {
    //        if(buttonIndex==1)
    //        {
    //            [self performSelector:@selector(initilizeLocationObject)];
    //        }
    //    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
