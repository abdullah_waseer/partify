//
//  PeopleTagPickerVC.m
//  WTPA
//
//  Created by Admin on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "PeopleTagPickerVC.h"
#import "PeopleTagPickerCustomCellVC.h"
#import "UIImageView+WebCache.h"

@interface PeopleTagPickerVC ()<NetworkManagerDelegate>

@property (nonatomic , retain) NSMutableDictionary * localData;
@property (nonatomic , retain) NSMutableArray * usersData;
@property (nonatomic , retain) IBOutlet UIImageView *imageToTag;
@property (nonatomic , retain) NetworkManager * networkManager;
@property (nonatomic , retain) UIView *popUpView;
@property (nonatomic,assign) BOOL isShowPopUp;
@property (nonatomic,assign) CGPoint pointTapped;

@end

@implementation PeopleTagPickerVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withdataDictionary:(NSMutableDictionary *) data
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.localData=[[NSMutableDictionary alloc] initWithDictionary:data];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scroll.contentSize = CGSizeMake(320, 568);
    if(!_localData)
        self.localData=[[NSMutableDictionary alloc] init];
    
    [_localData setObject:[NSMutableArray array] forKey:@"taggedarray"];
    [self setUpNavigationBar];
    _networkManager = [[NetworkManager alloc]init];
    _networkManager.delegate = self;
    
    if([self.localData objectForKey:@"image"])
    {
        UIImage *image=[UIImage imageWithData:[self.localData objectForKey:@"image"]];
        _imageToTag.image=image;
    }
    else
    {
        [_imageToTag setImageWithURL:[NSURL URLWithString:[self.localData objectForKey:@"imageurl"]]];
    }
    
    _isShowPopUp=YES;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    [_imageToTag addGestureRecognizer:tapRecognizer];

    
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - IBActions

- (IBAction)doneBtnPressed:(UIButton * )sender
{
//    NSMutableArray * datatoPass = [[NSMutableArray alloc]init];
//    for(int i=0;i<self.usersData.count;i++)
//        if([[[self.usersData objectAtIndex:i] objectForKey:@"selected"] isEqualToString:@"Y"])
//           [datatoPass addObject:[self.usersData objectAtIndex:i]];
    if([self.delegate respondsToSelector:@selector(peopleTagPickerVC:withTagList:)])
    {
        [self.delegate peopleTagPickerVC:self withTagList:[self.localData objectForKey:@"taggedarray"]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)cancelBtnPressed:(UIButton * )sender
{
    
}
- (IBAction)HideKeyBoard:(UIButton * )sender
{
    [_searchBar resignFirstResponder];
    [self.scroll sendSubviewToBack:_table];
    [self.scroll bringSubviewToFront:_imageToTag];
}
- (IBAction)tagPeopleBtnPressed:(UIButton * )sender
{
    if([[[self.usersData objectAtIndex:sender.tag] objectForKey:@"selected"]isEqualToString:@"Y"])
        [[self.usersData objectAtIndex:sender.tag] setObject:@"N" forKey:@"selected"];
    else
        [[self.usersData objectAtIndex:sender.tag] setObject:@"Y" forKey:@"selected"];
    [self.table reloadData];
}

#pragma mark - Custom Methods

-(void)showPopUP:(CGPoint) point
{
    if([_imageToTag.subviews containsObject: _popUpView])
        [_popUpView removeFromSuperview];
    
    if(point.x>224)
    {
        point.x=224;
        _pointTapped.x=224;
    }
    
    _popUpView=[[UIView alloc] initWithFrame:CGRectMake(point.x, point.y, 76, 33)];
    _popUpView.backgroundColor=[UIColor clearColor];
    
    UIImageView *popImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 76, 19)];
    popImage.image=[UIImage imageNamed:@"alert_popup.png"];
    popImage.backgroundColor=[UIColor blackColor];
    [_popUpView addSubview:popImage];
    
    UIButton *btntag=[UIButton buttonWithType:UIButtonTypeCustom];
    btntag.frame=CGRectMake(0, 8, 62, 19);
    btntag.backgroundColor=[UIColor clearColor];
    [btntag addTarget:self  action:@selector(showListForsearch) forControlEvents:UIControlEventTouchDown];
    [_popUpView addSubview:btntag];
    
    UIButton *popUpCross=[UIButton buttonWithType:UIButtonTypeCustom];
    popUpCross.frame=CGRectMake(64, 13, 11, 9);
    popUpCross.backgroundColor=[UIColor clearColor];
    [popUpCross setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    [popUpCross addTarget:self  action:@selector(hidePopUP) forControlEvents:UIControlEventTouchDown];
    [_popUpView addSubview:popUpCross];
    
    [_imageToTag addSubview:_popUpView];
    [self fadeInAnimation:_popUpView];
    [_popUpView release];
}

-(void) showListForsearch
{
    [_searchBar becomeFirstResponder];
    [self.scroll sendSubviewToBack:_imageToTag];
    [self.scroll bringSubviewToFront:_table];
}

-(void)hidePopUP
{
//    [_imageToTag  setUserInteractionEnabled:YES];
    
    if([self.view.subviews containsObject:_popUpView])
    {
//        [self fadeInAnimation:_popUpView];
        [_popUpView removeFromSuperview];
    }
}

-(void)fadeInAnimation:(UIView *)aView {
    
    CATransition *transition = [CATransition animation];
    transition.type =kCATransitionFade;
    transition.duration = 0.5f;
    transition.delegate = self;
    [aView.layer addAnimation:transition forKey:nil];
}

- (void)imageTapped:(UITapGestureRecognizer *)recognizer
{
    _pointTapped = [recognizer locationInView:_imageToTag];
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            /* equivalent to touchesBegan:withEvent: */
            break;
            
        case UIGestureRecognizerStateChanged:
            /* equivalent to touchesMoved:withEvent: */
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self showPopUP:_pointTapped];
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
            /* equivalent to touchesCancelled:withEvent: */
            break;
            
        default:
            break;
    }
}



-(void)getFollowerandFollowing
{
    
}

-(void) setUpNavigationBar
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    
    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, 240, 44)];
    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
    titleView.textColor = [UIColor whiteColor];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = [[data objectForKey:@"username"] uppercaseString];
    [self.navigationItem setTitleView:titleView];
    [titleView release];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}

- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) searchUser
{
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/searchuser/%@",[SharedAppManager sharedInstance].baseURL,self.searchBar.text];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    //[dictionary setObject:[data objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [dictionary setObject:[[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0] objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"searchUser" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
    [_networkManager sendRequestforAction:parameters];
}


#pragma mark - Delegate Methods
#pragma mark - UISearchBar Delegate Methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self searchUser];
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [_searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.scroll sendSubviewToBack:_imageToTag];
    [self.scroll bringSubviewToFront:_table];
    searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length==0 )
    {
        [searchBar resignFirstResponder];
    }
}

#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.usersData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    PeopleTagPickerCustomCellVC *cell = (PeopleTagPickerCustomCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"PeopleTagPickerCustomCellVC"] owner:self options:nil];
        
        for(id currentObject in objects)
        {
            
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (PeopleTagPickerCustomCellVC *)currentObject;
                break;
            }
        }
    }
    
    cell.nameLbl.text = [[self.usersData objectAtIndex:indexPath.row] objectForKey:@"username"];
    if([[[self.usersData objectAtIndex:indexPath.row] objectForKey:@"selected"]isEqualToString:@"Y"])
    {
        [cell.tagPeopeleBtn setImage:[UIImage imageNamed:@"selected.png"] forState:UIControlStateNormal];
    }
    else
        [cell.tagPeopeleBtn setImage:[UIImage imageNamed:@"unselected.png"] forState:UIControlStateNormal];
    cell.tagPeopeleBtn.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor=[UIColor whiteColor];
    
    cell.alpha=0.5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    [dic setObject:[NSString stringWithFormat:@"%@",[[self.usersData objectAtIndex:indexPath.row] objectForKey:@"username"]] forKey:@"tagged_username"];
    
    [dic setObject:[NSString stringWithFormat:@"%@",[[self.usersData objectAtIndex:indexPath.row] objectForKey:@"id"]] forKey:@"tagged_user_id"];
    [dic setObject:[NSString stringWithFormat:@"%f",_pointTapped.x] forKey:@"x"];
    [dic setObject:[NSString stringWithFormat:@"%f",_pointTapped.y/2] forKey:@"y"];
    
    [dic setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"index"];

    [[self.localData objectForKey:@"taggedarray"] addObject:dic];
    
    [self.scroll sendSubviewToBack:_table];
    [self.scroll bringSubviewToFront:_imageToTag];
    [self hidePopUP];
    
//    taggedarray
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"tagUser"])
    {
        
    }
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"searchUser"])
    {
        self.usersData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
        for(int i = 0; i < self.usersData.count; i++)
            [[self.usersData objectAtIndex:i] setObject:@"N" forKey:@"selected"];
        [self.table reloadData];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_localData release],_localData=nil;
    [_searchBar release];
    [_table release];
    [_scroll release];
    [super dealloc];
}

@end
