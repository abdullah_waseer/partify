//
//  PeoleTagPickerCustomCellVC.m
//  WTPA
//
//  Created by Admin on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "PeopleTagPickerCustomCellVC.h"

@implementation PeopleTagPickerCustomCellVC

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_nameLbl release];
    [_tagPeopeleBtn release];
    [super dealloc];
}
@end
