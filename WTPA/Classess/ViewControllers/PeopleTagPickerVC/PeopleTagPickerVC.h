//
//  PeopleTagPickerVC.h
//  WTPA
//
//  Created by Admin on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PeopleTagPickerVC;

@protocol peopleTagPickerVCDelegate <NSObject>

- (void) peopleTagPickerVC:(PeopleTagPickerVC*) peopleTagPickerVc withTagList:(NSMutableArray *)data;

@end

@interface PeopleTagPickerVC : UIViewController


@property (retain, nonatomic) IBOutlet UITableView *table;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property (assign, nonatomic) id<peopleTagPickerVCDelegate> delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withdataDictionary:(NSMutableDictionary *) data;
- (IBAction)doneBtnPressed:(UIButton *)sender;
- (IBAction)cancelBtnPressed:(UIButton *)sender;
- (IBAction)tagPeopleBtnPressed:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIScrollView *scroll;

@end
