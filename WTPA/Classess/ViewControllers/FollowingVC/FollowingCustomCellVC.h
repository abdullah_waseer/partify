//
//  FollowingCustomCellVC.h
//  WTPA
//
//  Created by Admin on 22/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowingCustomCellVC : UITableViewCell

@property (nonatomic , retain) IBOutlet UILabel * nameLbl;
@property (nonatomic , retain) IBOutlet UIButton * blockBtn;
@property (nonatomic , retain) IBOutlet UIImageView * userImage;

@end
