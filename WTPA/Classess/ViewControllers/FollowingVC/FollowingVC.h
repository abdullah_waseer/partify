//
//  FollowingVC.h
//  WTPA
//
//  Created by Admin on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowingVC : UIViewController
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)username andRequester:(NSString *)reqId;

@property (retain, nonatomic) IBOutlet UITableView *table;
- (IBAction)dropBtnPressed:(UIButton *)sender;

@end
