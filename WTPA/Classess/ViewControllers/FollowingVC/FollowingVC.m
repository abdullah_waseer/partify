//
//  FollowingVC.m
//  WTPA
//
//  Created by Admin on 20/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "FollowingVC.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "UserProfileVC.h"
#import "FollowingCustomCellVC.h"

@interface FollowingVC () <NetworkManagerDelegate>

@property (nonatomic , retain) NSString * reqId;
@property (nonatomic , retain) NSString * userId;
@property (nonatomic , retain) NSString * username;
@property (nonatomic , retain) NSMutableArray * followingData;
@property (nonatomic , retain) NSString * idToDrop;

@end

@implementation FollowingVC

NetworkManager * networkManager;

int clickIndex;

#pragma mark - View init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)username andRequester:(NSString *)reqId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.username = [[NSString alloc]initWithString: username];
        self.userId = [[NSString alloc]initWithString: reqId];
    }
    return self;
}
#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavigationBar];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self followingDetail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods

-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
    
//    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(40, 0, 240, 44)];
//    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
//    titleView.textColor = [UIColor whiteColor];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.text = [self.username uppercaseString];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.titleLabel.font = [UIFont fontWithName:@"Thonburi" size:36.0];
    titleButton.titleLabel.textColor = [UIColor whiteColor];
    titleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleButton setTitle:[self.username uppercaseString] forState:UIControlStateNormal];
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
    
}

-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

- (void)backAction:(id)sender
{
     networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) followingDetail
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/userfollowing/%@",[SharedAppManager sharedInstance].baseURL,self.userId];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"followingDetail" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

- (void) dropFollowing
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/dropfollowing/%@",[SharedAppManager sharedInstance].baseURL,self.idToDrop];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:[data objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"DELETE" forKey:@"callType"];
    [parameters setObject:@"dropFollowing" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [networkManager sendRequestforAction:parameters];
}

- (void) searchUser
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/searchuser/%@",[SharedAppManager sharedInstance].baseURL,self.searchBar.text];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    //[dictionary setObject:[data objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [dictionary setObject:[[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0] objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"searchUser" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [networkManager sendRequestforAction:parameters];
}
- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)imageWithImage:(UIImage*)sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height; // 161
    float scaleFactor = i_height / oldHeight; // 51/161 = 0.3167
    
    float newHeight =  oldHeight * scaleFactor;//sourceImage.size.height * scaleFactor; //54.87
    float newWidth =  sourceImage.size.width * scaleFactor;//oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)followUser
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/followuser/%@",[SharedAppManager sharedInstance].baseURL,[[self.followingData objectAtIndex:clickIndex] objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSDictionary * params = [[NSDictionary alloc]initWithObjectsAndKeys:[data objectForKey:@"id"],@"follower_id", nil];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:params] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"POST" forKey:@"callType"];
    [parameters setObject:@"followUser" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [networkManager sendRequestforAction:parameters];
}
#pragma mark - IBAction Implementation
- (IBAction)dropBtnPressed:(UIButton *)sender
{
    NSLog(@"%d",sender.tag);
    if(sender.tag > -1)
    {
        clickIndex = sender.tag - 1;
        self.idToDrop = [NSString stringWithFormat:@"%@",[[self.followingData objectAtIndex:clickIndex] valueForKey:@"id"]];
        NSString * message = [NSString stringWithFormat:@"Are you sure, you want to block %@?",[[self.followingData objectAtIndex:clickIndex] objectForKey:@"username"]];
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Partify"
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"Ok",nil] autorelease];
        alert.tag = 0;
        [alert show];
    }
    else
    {
        clickIndex = (sender.tag * -1) - 1;
        self.idToDrop = [NSString stringWithFormat:@"%@",[[self.followingData objectAtIndex:clickIndex] valueForKey:@"id"]];
        [self followUser];
    }
}
#pragma mark - Delegate Methods
#pragma mark - UISearchBar Delegate Methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self searchUser];
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [_searchBar resignFirstResponder];
   
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
   
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length==0 )
    {
        [searchBar resignFirstResponder];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self dropFollowing];
    }
    else
    {
        
    }
}

#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.followingData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    FollowingCustomCellVC *cell = (FollowingCustomCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"FollowingCustomCellVC"] owner:self options:nil];
        
        for(id currentObject in objects)
        {
            
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (FollowingCustomCellVC *)currentObject;
                break;
            }
        }
    }
    NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[self.followingData objectAtIndex:indexPath.row] objectForKey:@"profile_picture"]];
    //[[self.followingData objectAtIndex:0]setObject:@"0" forKey:@"following"];
    if([[[self.followingData objectAtIndex:indexPath.row] objectForKey:@"following"] isEqualToString:@"1"])
    {
        cell.blockBtn.tag = indexPath.row+1;
        [cell.blockBtn setImage:[UIImage imageNamed:@"drop.png" ] forState:UIControlStateNormal];
    }
    else
    {
        cell.blockBtn.tag = -1 * (indexPath.row+1);
        [cell.blockBtn setImage:[UIImage imageNamed:@"follow_1.png" ] forState:UIControlStateNormal];
    }
    
    cell.userImage.clipsToBounds=YES;
    [cell.userImage setImageWithURL:[NSURL URLWithString:url]
              placeholderImage:nil options:nil
                       success:^(UIImage *image, BOOL cached) {
                           
                           
                           UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:221.0].CGImage];
                           
                           cell.userImage.image = scaledImage;
                           cell.userImage.clipsToBounds = YES;
                           [scaledImage release];
                       }
                       failure:^(NSError *error) {
                           
                       }];

    cell.nameLbl.text = [[self.followingData objectAtIndex:indexPath.row] objectForKey:@"username"];
    
     NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    if(![[data objectForKey:@"id"] isEqualToString:self.userId])
    {
        cell.blockBtn.hidden=YES;
        
        CGRect si=cell.nameLbl.frame;
        si.size.width=320;
        
        [cell.nameLbl setFrame:si];
    }
    
    [cell setNeedsLayout];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[self.followingData objectAtIndex:indexPath.row] objectForKey:@"id"] status:YES];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];

}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"followingDetail"])
    {
        self.followingData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
        [self.table reloadData];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"error"] && [networkManager.identifier isEqualToString:@"followingDetail"])
    {
        [_followingData release],_followingData = nil;
        [self.table reloadData];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"dropFollowing"])
    {
        [[self.followingData objectAtIndex:clickIndex] setObject:@"0" forKey:@"following"];
        [self.table reloadData];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"searchUser"])
    {
        if([[object objectForKey:@"data"] count] > 0)
        {
            self.followingData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
            [self.table reloadData];
        }
        else
        {
            [[SharedAppManager sharedInstance] initWithTitle:@"Partify" message:@"No users found."];
        }
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"followUser"])
    {
        [[self.followingData objectAtIndex:clickIndex] setObject:@"1" forKey:@"following"];
        [self.table reloadData];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }    }
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - View Dealloc
- (void)dealloc {
    [_table release];
    [_searchBar release];
    [super dealloc];
}

@end
