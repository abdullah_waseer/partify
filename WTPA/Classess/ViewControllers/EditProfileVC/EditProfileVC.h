//
//  EditProfileVC.h
//  WTPA
//
//  Created by Ishaq Shafiq on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<NetworkManagerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
