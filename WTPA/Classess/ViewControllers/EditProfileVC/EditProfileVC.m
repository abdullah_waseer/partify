//
//  EditProfileVC.m
//  WTPA
//
//  Created by Ishaq Shafiq on 09/12/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "EditProfileVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "GCPlaceholderTextView.h"
#import "LinkToOtherNetworkVC.h"
#import "UIImageView+WebCache.h"

@interface EditProfileVC ()


@property (nonatomic,retain) IBOutlet UITextField *txtConfirmPassword;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;
@property (nonatomic,retain) IBOutlet GCPlaceholderTextView *txtBio;
@property (nonatomic,retain) IBOutlet UIImageView *profileImage;
@property (nonatomic,retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic,retain) NSString * error;
@property (nonatomic,retain) NetworkManager * networkManager;


@end

@implementation EditProfileVC

-(void) dealloc
{
    [_txtBio release],_txtBio=nil;
    [_txtEmail release],_txtEmail=nil;
    
    [_txtPassword release],_txtPassword=nil;
    
    [_txtConfirmPassword release],_txtConfirmPassword=nil;
    
    [_profileImage release],_profileImage=nil;
    
    [_networkManager release],_networkManager=nil;
    
    [_scrollView release],_scrollView=nil;
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setUpNavigationBar];
    
    self.networkManager = [[NetworkManager alloc] init];
    _networkManager.delegate = self;
    
    self.profileImage.layer.borderWidth=2;
    self.profileImage.layer.borderColor=[UIColor blackColor].CGColor;
    
    self.scrollView.contentSize=CGSizeMake(320, IS_IPHONE_5?630:670);
    
    self.txtBio.placeholder=@"Bio";
    
    NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
    
    self.txtEmail.text=[data objectForKey:@"email"];
    self.txtBio.text=[data objectForKey:@"bio"];
    
    NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"profile_picture"]];
    
    [self.profileImage setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:nil options:nil
                               success:^(UIImage *image, BOOL cached) {
                                   
                                   self.profileImage.image = image;
                               }
                               failure:^(NSError *error) {
                                   
                               }];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Custom methods Started

-(void) setUpNavigationBar
{
//    UIImageView *titleView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.image=[UIImage imageNamed:@"logo.png"];
//    titleView.backgroundColor=[UIColor clearColor];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton setImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
    UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame=BACK_FRAME;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    [barButtonItem release];
}
-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}


- (void)backAction:(id)sender
{
    _networkManager.delegate=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) uploadImageToserver
{
    NSData *param = UIImageJPEGRepresentation(self.profileImage.image, 0.4);
    
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/uploadprofilephoto/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:param forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"image/png",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"POST" forKey:@"callType"];
    [parameters setObject:@"uploadImage" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_networkManager sendRequestforAction:parameters];
    
}

-(IBAction)updateUserClicked:(id)sender
{
    if([self validate])
    {
        NSMutableDictionary *param=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    self.txtEmail.text,@"email",
                                    self.txtPassword.text,@"password",
                                    self.txtBio.text,@"bio",
                                    @"public",@"visibility",
                                    nil];
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        
        NSString *jsonString = [jsonWriter stringWithObject:param];
        
        [jsonWriter release];
        
        NSDictionary *data= [[[NSUserDefaults standardUserDefaults] valueForKey:USER_DATA] objectAtIndex:0];
        
        NSString* urlString = [NSString stringWithFormat:@"%@api/users/updateuser/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
        
        NSMutableDictionary  *parameters=[NSMutableDictionary dictionary];
        [parameters setObject:jsonString forKey:@"param"];
        [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
        [parameters setObject:urlString forKey:@"url"];
        [parameters setObject:@"PUT" forKey:@"callType"];
        [parameters setObject:@"updateuser" forKey:@"idetntifier"];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [_networkManager sendRequestforAction:parameters];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:self.error];
    }
}

#pragma mark - Custom Methods Statrt

- (BOOL)validate
{
   
    if(![AppUtils validateEmail:self.txtEmail.text])
    {
        self.error = [NSString stringWithFormat:@"Please enter valid e-mail"];
        return NO;
    }
    else if(self.txtPassword.text.length == 0)
    {
        self.error = [NSString stringWithFormat:@"Please enter password"];
        return NO;
    }
    else  if(![self.txtConfirmPassword.text isEqualToString:self.txtPassword.text])
    {
        self.error = [NSString stringWithFormat:@"Passwords do not match, Please try again"];
        return NO;
    }
    return YES;
}


#pragma mark - IBActions Methods Started
-(IBAction)hidekeyBorad:(id)sender
{
    [_txtBio resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    [_txtConfirmPassword resignFirstResponder];
}

-(IBAction)addImageClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Select image"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Capture with camera", @"From PhotoLibrary", nil];
    [actionSheet showInView:self.view];
    [actionSheet release];
}

#pragma mark - NetworkMangerDelegate Delegate Methods

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"])
    {
        if([networkManager.identifier isEqualToString:@"updateuser"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_AUTHENTICATE];
            
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            [[NSUserDefaults standardUserDefaults] setObject:self.txtPassword.text  forKey:PASSWORD];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if(self.profileImage.image)
            {
                [self uploadImageToserver];
            }
            else
            {
                [self backAction:nil];
            }
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"]  forKey:USER_DATA];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self backAction:nil];
        }
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}


#pragma mark - UIACtionSheet Delegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0:
        {
            [self showImagePickerController: UIImagePickerControllerSourceTypeCamera];
        }
            break;
        case 1:
        {
            [self showImagePickerController: UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        }
        default:
            // Do Nothing.........
            break;
    }
}
-(void) showImagePickerController:(UIImagePickerControllerSourceType) sourceType
{
    if([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        UIImagePickerController * picker = [[[UIImagePickerController alloc] init] autorelease];
        picker.delegate = self;
        picker.allowsEditing=YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:^{}];
    }
}
#pragma mark - UIImagePIckerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *selectedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (!selectedImage)
    {
        return;
    }
    
    self.profileImage.image=selectedImage;
    
    //    // Adjusting Image Orientation
    //    NSData *data = UIImagePNGRepresentation(selectedImage);
    //    UIImage *tmp = [UIImage imageWithData:data];
    //    UIImage *fixed = [UIImage imageWithCGImage:tmp.CGImage
    //                                         scale:selectedImage.scale
    //                                   orientation:selectedImage.imageOrientation];
    //    self.profileImage.image = fixed;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - UITextFiled Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text=@"";
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextResponder=textField.tag+1;
    if(nextResponder!=3)
    {
        UITextField *txtfield=(UITextField *)[self.view viewWithTag:nextResponder];
        if(txtfield)
        {
            [txtfield becomeFirstResponder];
        }
    }
    else
    {
        UITextView *txtfield=(UITextView *)[self.view viewWithTag:nextResponder];
        if(txtfield)
        {
            [txtfield becomeFirstResponder];
        }
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
