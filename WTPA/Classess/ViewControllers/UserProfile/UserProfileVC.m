//
//  UserProfileVC.m
//  WTPA
//
//  Created by Admin on 19/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "UserProfileVC.h"
#import "UserProfileCustomCellVCCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Resize.h"
#import "FollowersVC.h"
#import "FollowingVC.h"
#import "SettingsVC.h"
#import "PartyMapVC.h"
#import "UIAlertView+Block.h"
#import "CUIButton.h"
#import "TaggedView.h"

#import "UserFeedVC.h"
#import "UserFeedCustomCellVC.h"
#import "UserFeedCustomCellVC3.h"
#import "UserFeedCustomCellVC1.h"
#import "UserFeedCustomCellVC2.h"
#import "NSString+SBJSON.h"
#import "EventDetailVC.h"
#import "VenueDetailVC.h"
#import "NSDate-Utilities.h"
#import "FeedbackVC.h"


@interface UserProfileVC () <NetworkManagerDelegate , UIGestureRecognizerDelegate,TaggedViewDelegate>

@property (nonatomic , retain) NSMutableDictionary * profileData;
@property (nonatomic , retain) NSMutableArray *feedData;
@property (nonatomic , retain) NSMutableArray * imagesArray;
@property (nonatomic, retain) UIImageView * profilePic;
@property (nonatomic, retain) UIImageView * profileBio;
@property (nonatomic, retain) NSString * userid;
@property (nonatomic, retain) NSString * username;
@property BOOL follow;

@property (nonatomic , retain) UIView *popUpView;
@property (nonatomic,assign) BOOL isShowPopUp;

@end

@implementation UserProfileVC



-(void)resetAllProperties
{
    [_profileData release];
    [_imagesArray release];
    [self.userid release];
    [self.username release];
    _profileData = nil;
    _imagesArray = nil;
    self.profilePic.image = nil;
    self.notificationBtn.hidden = YES;
    [self.notificationBtn setTitle:@"" forState:UIControlStateNormal];
    _userid = nil;
    _username = nil;
    [self.navigationItem setTitleView:nil];
    [self.table reloadData];
}
#pragma mark - init View

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)userid status:(BOOL)follow
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.userid = [NSString stringWithFormat:@"%@",userid];
        self.follow = follow;
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad  
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.leftBarButtonItem=nil;
   
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.notificationBtn.hidden = YES;
    if(![SharedAppManager sharedInstance].addUser)
    {
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    if([self.userid isEqualToString:@"(null)"] || self.userid == nil)
        self.userid = [NSString stringWithFormat:@"%@",[data objectForKey:@"id"]];
//    if([self.userid isEqualToString:[data objectForKey:@"id"]])
//    {
//        
//    }
//    else
//    {
//        if(self.follow)
//            [self.settingButton setTitle:@"Unfollow" forState:UIControlStateNormal];
//        [self.settingButton setTitle:@"Follow" forState:UIControlStateNormal];
//    }
    self.profilePic = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 305, 151)] autorelease];
        //self.profilePic.backgroundColor = [UIColor whiteColor];
    self.profilePic.contentMode = UIViewContentModeScaleAspectFill;
    self.profileBio = [[[UIImageView alloc]initWithFrame:CGRectMake(320, 0, 320, 151)] autorelease];
    self.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.profilePic.layer.borderWidth = 1.0;
    [self.profileScrollView addSubview:self.profilePic];
    [self.profileScrollView addSubview:self.profileBio];
    self.profileScrollView.contentSize = CGSizeMake(640, 151);
    //if(![self.userid isEqualToString:@"(null)"])
    //{
    [self performSelector:@selector(userDetailsCall) withObject:nil afterDelay:0.2];
   
    [self performSelector:@selector(getNewFollower) withObject:nil afterDelay:0.9];
    }
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
    //}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods

#pragma mark - IBactions
- (IBAction)venueDetailBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}

- (IBAction)userAttendingDetailBtnPressed:(UIButton *)sender
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"user_detail"]objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}

- (IBAction)eventAttendingBtnPressed:(UIButton *)sender
{
    NSMutableDictionary * eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"event_detail"]objectForKey:@"id"],@"id",@"YES",@"type",@"DB",@"source",nil];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventDetail];
    eventDetailVC.venueId = [[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetail release];
}
- (IBAction)venueAttendingBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}

- (IBAction)userCreatedBtnPressed:(UIButton *)sender
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"user_detail"]objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}

- (IBAction)eventCreatedBtnPressed:(UIButton *)sender
{
    NSMutableDictionary * eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"YES",@"type",@"DB",@"source",[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"]objectForKey:@"event_detail"]objectForKey:@"id"],@"id",nil];
    EventDetailVC * eventDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil andEventDetail:eventDetail];
//    eventDetailVC.venueId = [[[_eventsArray objectAtIndex:button.index] objectForKey:@"venue"] objectForKey:@"venu_id"];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetail release];
}

- (IBAction)venueCreatedBtnPressed:(UIButton *)sender
{
    VenueDetailVC * venueDetail = [[VenueDetailVC alloc]initWithNibName:@"VenueDetailVC" bundle:nil withVenueId:[[[[self.feedData objectAtIndex:sender.tag] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"id"]];
    [self.navigationController pushViewController:venueDetail animated:YES];
    [venueDetail release];
}


-(void) setUpNavigationBar
{
//    self.view.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
//    self.view.layer.borderWidth=3;
//    
//    UILabel * titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 157, 27)];
//    titleView.font = [UIFont fontWithName:@"Thonburi" size:36.0];
//    titleView.textColor = [UIColor whiteColor];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.text = [[self.profileData objectForKey:@"username"] uppercaseString];
//    [self.navigationItem setTitleView:titleView];
//    [titleView release];
    
    UIButton *titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(0, 8, 157, 27);
    titleButton.titleLabel.font = [UIFont fontWithName:@"Thonburi" size:28.0];
    titleButton.titleLabel.textColor = [UIColor whiteColor];
    titleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleButton setTitle:[[self.profileData objectForKey:@"username"] uppercaseString] forState:UIControlStateNormal];
    titleButton.backgroundColor=[UIColor clearColor];
    [titleButton addTarget:self action:@selector(userNameClicked:) forControlEvents:UIControlEventTouchDown];
    [self.navigationItem setTitleView:titleButton];
    
     if(![self.settingButton.titleLabel.text isEqualToString:@"Settings"])
     {
         UIButton* backButton= [UIButton buttonWithType:UIButtonTypeCustom];
         [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
         backButton.frame=BACK_FRAME;
         [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
         self.navigationItem.leftBarButtonItem = barButtonItem;
         [barButtonItem release];
     }
     else
     {
         self.navigationItem.backBarButtonItem=nil;
     }
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)userNameClicked:(id)sender
{
    UserFeedVC * userFeedVC = [[UserFeedVC alloc]init];
    [self.navigationController pushViewController:userFeedVC animated:YES];
    [userFeedVC release];
}

- (void) userDetailsCall
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/userdetail/%@",[SharedAppManager sharedInstance].baseURL,self.userid];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",[data objectForKey:@"id"],@"REQUESTER_ID",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getUserDetail" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

- (void) userPhotos
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/feed/%@",[SharedAppManager sharedInstance].baseURL,self.userid];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getUserFeed" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   NetworkManager *networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

-(void)getNewFollower
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/newfollowers/%@",[SharedAppManager sharedInstance].baseURL,[data objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"GET" forKey:@"callType"];
    [parameters setObject:@"getNewFollower" forKey:@"idetntifier"];
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

- (void) followUser
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/followuser/%@",[SharedAppManager sharedInstance].baseURL,[self.profileData objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSDictionary * params = [[NSDictionary alloc]initWithObjectsAndKeys:[data objectForKey:@"id"],@"follower_id", nil];
    
    NSString *jsonString = [jsonWriter stringWithObject:params];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-type",[data objectForKey:@"id"],@"REQUESTER_ID",nil] forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"POST" forKey:@"callType"];
    [parameters setObject:@"followUser" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)imageWithImage:(UIImage*)sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height; // 161
    float scaleFactor = i_height / oldHeight; // 51/161 = 0.3167
    
    float newHeight =  oldHeight * scaleFactor;//sourceImage.size.height * scaleFactor; //54.87
    float newWidth =  sourceImage.size.width * scaleFactor;//oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void) dropFollowing
{
    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    
    NSString* urlString = [NSString stringWithFormat:@"%@api/users/dropfollowing/%@",[SharedAppManager sharedInstance].baseURL,self.userid];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:[jsonWriter stringWithObject:[NSMutableDictionary dictionary]] forKey:@"param"];
    
    [jsonWriter release];
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
    [dictionary setObject:[data objectForKey:@"id"] forKey:@"REQUESTER_ID"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [dictionary setObject:@"application/json" forKey:@"Content-Type"];
    [parameters setObject:dictionary forKey:@"headers"];
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"DELETE" forKey:@"callType"];
    [parameters setObject:@"dropFollowing" forKey:@"idetntifier"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
}


- (void)imageTapped:(UITapGestureRecognizer *)recognizer
{
    UIImageView *TappedImage = (UIImageView*)recognizer.view;
    CGPoint pointTapped = [recognizer locationInView:TappedImage];
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            /* equivalent to touchesBegan:withEvent: */
            break;
            
        case UIGestureRecognizerStateChanged:
            /* equivalent to touchesMoved:withEvent: */
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self showPopUP:pointTapped withImage:TappedImage];
            break;
        }
            
        case UIGestureRecognizerStateCancelled:
            /* equivalent to touchesCancelled:withEvent: */
            break;
            
        default:
            break;
    }
}

-(void)showPopUP:(CGPoint) point withImage:(UIImageView *) tappedImage
{
    if(_popUpView)
        [self hidePopUP];
    
    NSDictionary *isFound=[self isTappedInTaggedArea:tappedImage.tag withPoints:point];
    if([[isFound objectForKey:@"found"] isEqualToString:@"y"])
    {
        self.popUpView=[[UIView alloc] initWithFrame:CGRectMake(point.x, point.y, 76, 33)];
        _popUpView.backgroundColor=[UIColor clearColor];
        
        UIImageView *popImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 76, 19)];
        popImage.image=[UIImage imageNamed:@"alert_tag_name"];
        popImage.backgroundColor=[UIColor blackColor];
        [_popUpView addSubview:popImage];
        
        UIButton *btntag=[UIButton buttonWithType:UIButtonTypeCustom];
        btntag.frame=CGRectMake(0, 8, 62, 19);
        [btntag setTitle:[isFound objectForKey:@"username"] forState:UIControlStateNormal];
        btntag.titleLabel.textColor=[UIColor whiteColor];
        btntag.titleLabel.font=[UIFont fontWithName:@"Thonburi" size:12.0];
        btntag.backgroundColor=[UIColor clearColor];
        [btntag addTarget:self  action:@selector(hidePopUP) forControlEvents:UIControlEventTouchDown];
        [_popUpView addSubview:btntag];
        
        CUIButton *popUpCross=[CUIButton buttonWithType:UIButtonTypeCustom];
        popUpCross.frame=CGRectMake(64, 13, 11, 9);
        popUpCross.backgroundColor=[UIColor clearColor];
        popUpCross.tag=tappedImage.tag;
        popUpCross.titleName=[NSString stringWithFormat:@"%@",[isFound objectForKey:@"username"]];
        [popUpCross setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
        [popUpCross addTarget:self  action:@selector(doSomething:) forControlEvents:UIControlEventTouchDown];
        [_popUpView addSubview:popUpCross];
        
        [tappedImage addSubview:_popUpView];
        [self fadeInAnimation:_popUpView];
        [_popUpView release];
    }
}

-(void)bringFeedbackVC:(UIButton*)senderBtn
{
    FeedbackVC *feedbackVC = [[FeedbackVC alloc]initWithNibName:@"FeedbackVC" bundle:nil];
    feedbackVC.feedbackIssueId = senderBtn.accessibilityIdentifier;
    feedbackVC.issueAbout = @"photo";
    [self.navigationController pushViewController:feedbackVC animated:YES];
}

-(void) doSomething :(id) sender
{
    [UIAlertView alertViewWithTitle:ALERTVIEW_TITLE
                            message:@"Are you sure to remove this tag"
                  cancelButtonTitle:@"Cancel"
                  otherButtonTitles:[NSArray arrayWithObjects:@"YES", nil]
                          onDismiss:^(int buttonIndex, NSString *buttonTitle)
                            {
                                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                [self removeTagFromImage:sender];
                            }
                           onCancel:^(void)
                            {
                               [self hidePopUP];
                           }
     ];

}

-(void) removeTagFromImage:(id) sender
{
    CUIButton *tppedTag=(CUIButton *) sender;

    NSString* urlString = [NSString stringWithFormat:@"%@api/photos/deletetaguser/%@",[SharedAppManager sharedInstance].baseURL,[[self.imagesArray objectAtIndex:[tppedTag tag]] objectForKey:@"id"]];
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject:[NSDictionary dictionary]];
    
    [jsonWriter release];
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:jsonString forKey:@"param"];
    NSMutableArray *userData=[[self.imagesArray objectAtIndex:[tppedTag tag]] objectForKey:@"taged_user_data"];
    for(int i=0;i<userData.count;i++)
    {
        NSDictionary *Taggeddict=[userData objectAtIndex:i];
        if([[Taggeddict objectForKey:@"name"] isEqualToString:tppedTag.titleName])
        {
            [parameters setObject:[NSDictionary dictionaryWithObjectsAndKeys:[Taggeddict objectForKey:@"id"] ,@"TAGED_ID",nil] forKey:@"headers"];
            [userData removeObjectAtIndex:i];
            break;
        }
    }
    [self hidePopUP];
    
    [parameters setObject:urlString forKey:@"url"];
    [parameters setObject:@"DELETE" forKey:@"callType"];
    [parameters setObject:@"removetag" forKey:@"idetntifier"];
    
    NetworkManager * networkManager = [[NetworkManager alloc] init];
    networkManager.delegate = self;
    [networkManager sendRequestforAction:parameters];
   
}

-(void)hidePopUP
{
    [_popUpView removeFromSuperview];
    [self fadeInAnimation:_popUpView];
}

-(NSDictionary *) isTappedInTaggedArea:(NSInteger) tag withPoints:(CGPoint) point
{
    NSArray *userData=[[self.imagesArray objectAtIndex:tag] objectForKey:@"taged_user_data"];
    for(int i=0;i<userData.count;i++)
    {
        NSDictionary *Taggeddict=[userData objectAtIndex:i];
        CGPoint pointCompare=CGPointMake([[Taggeddict objectForKey:@"x_position"] floatValue], [[Taggeddict objectForKey:@"y_position"] floatValue]);
        
        if(point.x>=pointCompare.x && point.x <
           pointCompare.x+30 && point.y>=pointCompare.y && point.y < pointCompare.y+20)
        {
            
            return [NSDictionary dictionaryWithObjectsAndKeys:@"y",@"found",[Taggeddict objectForKey:@"name"],@"username", nil];
            break;
        }
    }
    return [NSDictionary dictionaryWithObjectsAndKeys:@"n",@"found",@"",@"username", nil];
}

-(void)fadeInAnimation:(UIView *)aView {
    
    CATransition *transition = [CATransition animation];
    transition.type =kCATransitionFade;
    transition.duration = 0.5f;
    transition.delegate = self;
    [aView.layer addAnimation:transition forKey:nil];
}



#pragma mark - IBAction Implementation

- (IBAction)userTagedPoopUPBtnPressed:(UIButton *)sender
{
    TaggedView *tagedView=[[TaggedView alloc] initWithFrame:CGRectMake(35, IS_IPHONE_5?134:90, 250, 300) withArray:[[[self.feedData objectAtIndex:[sender tag]] objectForKey:@"data"] objectForKey:@"taged_user_detail"]];
    tagedView.delegate=self;
    [self.view addSubview:tagedView];
    
}

- (IBAction)userTagedAttendingDetailBtnPressed:(UIButton *)sender
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:self.userid status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
}


- (IBAction)followingBtnPressed:(id)sender
{
    FollowingVC * followingVC = [[FollowingVC alloc]initWithNibName:@"FollowingVC" bundle:nil with:[self.profileData objectForKey:@"username"] andRequester:[self.profileData objectForKey:@"id"] ];
    [self.navigationController pushViewController:followingVC animated:YES];
    [followingVC release];
}


- (IBAction)partyMapPressed:(id)sender
{
    PartyMapVC * partyMapVC = [[PartyMapVC alloc]initWithNibName:@"PartyMapVC" bundle:nil];
    [self.navigationController pushViewController:partyMapVC animated:YES];
    [partyMapVC release];
}

- (IBAction)followersBtnPressed:(id)sender
{
    FollowersVC * followersVC = [[FollowersVC alloc]initWithNibName:@"FollowersVC" bundle:nil with:[self.profileData objectForKey:@"username"] andRequester:self.userid];
    [self.navigationController pushViewController:followersVC animated:YES];
    [followersVC release];
}

- (IBAction)settingsBtnPressed:(id)sender
{
    UIButton * button = (UIButton *)sender;
    //api/users/followuser/USERID
    if([[button titleLabel].text isEqualToString:@"Settings"])
    {
        SettingsVC * settingsVC = [[SettingsVC alloc]initWithNibName:@"SettingsVC" bundle:nil with:[self.profileData objectForKey:@"username"]];
        [self.navigationController pushViewController:settingsVC animated:YES];
        [settingsVC release];
    }
    else if([[button titleLabel].text isEqualToString:@"Follow"])
    {
        [self followUser];
    }
    else if([[button titleLabel].text isEqualToString:@"Unfollow"])
    {
        [self dropFollowing];
    }

}

- (IBAction)notificationBtnPressed:(id)sender
{
    FollowersVC * followersVC = [[FollowersVC alloc]initWithNibName:@"FollowersVC" bundle:nil with:[self.profileData objectForKey:@"username"] andRequester:self.userid];
    [self.navigationController pushViewController:followersVC animated:YES];
    [followersVC release];
}

#pragma mark - Delegate Methods

#pragma mark - Table View Delegates

#pragma mark - Delegates
#pragma mark - Table Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    int padding = 5;
    if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"image_upload"])
    {
        UserFeedCustomCellVC *cell = (UserFeedCustomCellVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC *)currentObject;
                    [cell.feedbackBtn addTarget:self action:@selector(bringFeedbackVC:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                }
            }
        }
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"image_url"]];
        [cell.image setImageWithURL:[NSURL URLWithString:url]];
        cell.image.clipsToBounds=YES;
        //    cell.venueBtn.frame = CGRectMake(cell.venueBtn.frame.origin.x, cell.venueBtn.frame.origin.y, expectedSize.width+padding+20, 20);
        [cell.venueBtn addTarget:self action:@selector(venueDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        [cell.venueBtn setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        
        cell.feedbackBtn.accessibilityIdentifier = [[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"id"];
        
        /*[cell.image setImageWithURL:[NSURL URLWithString:url]
         placeholderImage:nil options:nil
         success:^(UIImage *image, BOOL cached) {
         
         
         UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:305.0].CGImage];
         
         cell.feedImage.image = scaledImage;
         cell.feedImage.clipsToBounds = YES;
         [scaledImage release];
         }
         failure:^(NSError *error) {
         
         }];*/
        cell.venueBtn.tag = indexPath.row;
        
        NSDate * actDate = [AppUtils getDateFromString:[[self.feedData objectAtIndex:indexPath.row]objectForKey:@"activity_time"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        [cell.timeBtn setTitle:[AppUtils timeFormatted:[NSDate date] toDate:actDate] forState:UIControlStateNormal];
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"event_follow"])
    {
        UserFeedCustomCellVC1 *cell = (UserFeedCustomCellVC1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC1"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC1 *)currentObject;
                    break;
                }
            }
        }

        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.userName.frame = CGRectMake(cell.userName.frame.origin.x, cell.userName.frame.origin.y, expectedSize.width+padding+5, 30);
        [cell.userName addTarget:self action:@selector(userAttendingDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.attending.frame = CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.attending.frame.origin.y, cell.attending.frame.size.width, cell.attending.frame.size.height);
        cell.eventName.frame = CGRectMake(cell.attending.frame.origin.x+cell.attending.frame.size.width, cell.eventName.frame.origin.y, expectedSize.width+padding+5, 30);
        
        [cell.eventName addTarget:self action:@selector(eventAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.at.frame = CGRectMake(cell.eventName.frame.origin.x+cell.eventName.frame.size.width, cell.at.frame.origin.y, cell.at.frame.size.width, cell.at.frame.size.height);
        cell.venueName.frame = CGRectMake(cell.at.frame.origin.x+cell.at.frame.size.width, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
        
        [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        [cell.userName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] forState:UIControlStateNormal];
        [cell.eventName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        cell.userName.tag = indexPath.row;
        cell.eventName.tag = indexPath.row;
        cell.venueName.tag = indexPath.row;
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"event_creation"])
    {
        UserFeedCustomCellVC2 *cell = (UserFeedCustomCellVC2 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC2"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC2 *)currentObject;
                    break;
                }
            }
        }
       
        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.userCreated.frame = CGRectMake(cell.userCreated.frame.origin.x, cell.userCreated.frame.origin.y, expectedSize.width+padding, 30);
        
        [cell.userCreated addTarget:self action:@selector(userCreatedBtnPressed:) forControlEvents:UIControlEventTouchDown];
        cell.created.frame = CGRectMake(cell.userCreated.frame.origin.x+cell.userCreated.frame.size.width, cell.created.frame.origin.y, cell.created.frame.size.width, 30);
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.eventCreated.frame = CGRectMake(cell.created.frame.origin.x+cell.created.frame.size.width, cell.eventCreated.frame.origin.y, expectedSize.width+padding+10, 30);
        [cell.eventCreated addTarget:self action:@selector(eventAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
        cell.at.frame = CGRectMake(cell.eventCreated.frame.origin.x+cell.eventCreated.frame.size.width, cell.at.frame.origin.y, cell.at.frame.size.width, 30);
        cell.venueAt.frame = CGRectMake(cell.at.frame.origin.x+cell.at.frame.size.width, cell.venueAt.frame.origin.y, expectedSize.width+padding+10, 30);
        
        [cell.userCreated setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"user_detail"] objectForKey:@"username"] forState:UIControlStateNormal];
        [cell.eventCreated setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"event_detail"] objectForKey:@"event_name"] forState:UIControlStateNormal];
        [cell.venueAt setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
        [cell.venueAt addTarget:self action:@selector(venueCreatedBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        cell.userCreated.tag = indexPath.row;
        cell.eventCreated.tag = indexPath.row;
        cell.venueAt.tag = indexPath.row;
        return cell;
    }
    else if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"] isEqualToString:@"image_taged"])
    {
        UserFeedCustomCellVC3 *cell = (UserFeedCustomCellVC3 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray* objects = [[NSBundle mainBundle] loadNibNamed:[AppUtils getNibName:@"UserFeedCustomCellVC3"] owner:self options:nil];
            
            for(id currentObject in objects)
            {
                
                if([currentObject isKindOfClass:[UITableViewCell class]])
                {
                    cell = (UserFeedCustomCellVC3 *)currentObject;
                    [cell.feedbackBtn addTarget:self action:@selector(bringFeedbackVC:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                }
            }
        }
        
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"image_url"]];
        [cell.image setImageWithURL:[NSURL URLWithString:url]];
        cell.image.clipsToBounds=YES;
        
        cell.feedbackBtn.accessibilityIdentifier = [[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"photo_detail"] objectForKey:@"id"];
        
        UIFont*font = [UIFont fontWithName:@"Thonburi" size:11.0];
        CGSize expectedSize ;
        float maxSize = 90.0;
        
        expectedSize = [[self.profileData objectForKey:@"username"] sizeWithMyFont:font withMaxSize:maxSize];
        
        cell.userName.frame = CGRectMake(cell.userName.frame.origin.x, cell.userName.frame.origin.y, expectedSize.width+padding+5, 30);
        [cell.userName addTarget:self action:@selector(userTagedAttendingDetailBtnPressed:) forControlEvents:UIControlEventTouchDown];
        [cell.userName setTitle:[self.profileData objectForKey:@"username"] forState:UIControlStateNormal];
        
        NSArray *array=[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"taged_user_detail"];
        
        if(array.count>1)
        {
            cell.andLabel.hidden=NO;
            cell.multipleTag.hidden=NO;
            cell.andLabel.frame=CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.andLabel.frame.origin.y, cell.andLabel.frame.size.width, cell.andLabel.frame.size.height);
            
            expectedSize = [[NSString stringWithFormat:@"%i other%@",array.count-1,array.count-1==1?@"":@"s"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.multipleTag.frame=CGRectMake(cell.andLabel.frame.origin.x+cell.andLabel.frame.size.width, cell.multipleTag.frame.origin.y, expectedSize.width+10, 30);
            [cell.multipleTag addTarget:self action:@selector(userTagedPoopUPBtnPressed:) forControlEvents:UIControlEventTouchDown];
            [cell.multipleTag setTitle:[NSString stringWithFormat:@"%i other%@",array.count-1,array.count-1==1?@"":@"s"] forState:UIControlStateNormal];
            
            cell.wasAtLabel.frame = CGRectMake(cell.multipleTag.frame.origin.x+cell.multipleTag.frame.size.width, cell.wasAtLabel.frame.origin.y, cell.wasAtLabel.frame.size.width, cell.wasAtLabel.frame.size.height);
            
            expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.venueName.frame = CGRectMake(cell.wasAtLabel.frame.origin.x+cell.wasAtLabel.frame.size.width+5, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
            
            [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
            
            [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
            
        }
        else
        {
            cell.andLabel.hidden=YES;
            cell.multipleTag.hidden=YES;
            
            cell.wasAtLabel.text=@"was at";
            
            cell.wasAtLabel.frame = CGRectMake(cell.userName.frame.origin.x+cell.userName.frame.size.width, cell.wasAtLabel.frame.origin.y, cell.wasAtLabel.frame.size.width, cell.wasAtLabel.frame.size.height);
            
            expectedSize = [[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] sizeWithMyFont:font withMaxSize:maxSize];
            
            cell.venueName.frame = CGRectMake(cell.wasAtLabel.frame.origin.x+cell.wasAtLabel.frame.size.width+5, cell.venueName.frame.origin.y, expectedSize.width+padding+10,30);
            
            [cell.venueName addTarget:self action:@selector(venueAttendingBtnPressed:) forControlEvents:UIControlEventTouchDown];
            
            [cell.venueName setTitle:[[[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"data"] objectForKey:@"venu_detail"] objectForKey:@"name"] forState:UIControlStateNormal];
            
        }
        
        cell.multipleTag.tag=indexPath.row;
        cell.userName.tag = indexPath.row;
        cell.venueName.tag = indexPath.row;
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"]isEqualToString:@"image_upload"] || [[[self.feedData objectAtIndex:indexPath.row] objectForKey:@"activity_type"]isEqualToString:@"image_taged"])
        return 151;
    else
        return 44;
}

# pragma mark - TaggedView Delegates

- (void)TagedViewcrossTapped:(TaggedView *)view
{
    [view removeFromSuperview];
}

- (void)TagedView:(TaggedView *)view withTaggedDictionary:(NSMutableDictionary *) tagged
{
    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[tagged objectForKey:@"id"] status:NO];
    [self.navigationController pushViewController:userProfileVC animated:YES];
    [userProfileVC release];
    
    [view removeFromSuperview];
}


# pragma mark - Network Delegates

- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"getUserDetail"])
    {
        self.profileData = [[NSMutableDictionary alloc]initWithDictionary:[object objectForKey:@"data"]];
        NSString * url = [NSString stringWithFormat:@"%@%@",[SharedAppManager sharedInstance].baseURL,[self.profileData objectForKey:@"profile_picture"]];
        NSLog(@"%@",url);
        [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                       placeholderImage:nil options:nil
                                success:^(UIImage *image, BOOL cached) {
                                    
                                    
                                    UIImage *scaledImage = [[UIImage alloc] initWithCGImage:[self imageWithImage:image scaledToWidth:300.0].CGImage];
                                    
                                    self.profilePic.image = scaledImage;
                                    self.profilePic.clipsToBounds = YES;
                                    [scaledImage release];
                                }
                                failure:^(NSError *error) {
                                    
                                }];

        self.followersLbl.text = [NSString stringWithFormat:@"%@ Followers", [self.profileData objectForKey:@"user_followers"]];
        self.followingLbl.text = [NSString stringWithFormat:@"%@ Following", [self.profileData objectForKey:@"user_following"]];
        self.partyRatingLbl.text = [NSString stringWithFormat:@"%@%% Party Rating", [self.profileData objectForKey:@"party_rating"]];
        self.descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 305, 128)];
        self.descriptionLabel.numberOfLines = 5;
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.descriptionLabel.textColor = [UIColor whiteColor];
        self.descriptionLabel.font = [UIFont fontWithName:@"Thonburi" size:12];
        self.descriptionLabel.text = [self.profileData objectForKey:@"bio"];
        [self.profileBio setImage:[UIImage imageNamed:@"description"]];
        [self.profileBio addSubview:self.descriptionLabel];
        NSDictionary * data = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0];
        if([[[self.profileData objectForKey:@"follower"] stringValue] isEqualToString:@"0"] && [[[self.profileData objectForKey:@"following"] stringValue] isEqualToString:@"0"])
        {
           if([self.userid isEqualToString:[data objectForKey:@"id"]])
           {
               [self.settingButton setTitle:@"Settings" forState:UIControlStateNormal];
           }
           else
           {
               [self.settingButton setTitle:@"Follow" forState:UIControlStateNormal];
           }
        }
        
        else if([[[self.profileData objectForKey:@"follower"] stringValue] isEqualToString:@"1"] && [[[self.profileData objectForKey:@"following"] stringValue] isEqualToString:@"0"])
            [self.settingButton setTitle:@"Follow" forState:UIControlStateNormal];
       else if([[[self.profileData objectForKey:@"follower"] stringValue] isEqualToString:@"0"] && [[[self.profileData objectForKey:@"following"] stringValue] isEqualToString:@"1"])
            [self.settingButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        else if([[[self.profileData objectForKey:@"follower"] stringValue] isEqualToString:@"1"] && [[[self.profileData objectForKey:@"following"] stringValue] isEqualToString:@"1"])
            [self.settingButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        
        if([self.settingButton.titleLabel.text isEqualToString:@"Settings"])
        {
             [self.settingButton setBackgroundImage:[UIImage imageNamed:@"settingsbutton.png"] forState:UIControlStateNormal];
        }
        else
        {
            [self.settingButton setBackgroundImage:[UIImage imageNamed:@"settingsbutton_withouttext.png"] forState:UIControlStateNormal];
        }
            
        [self setUpNavigationBar];
        
        [self performSelector:@selector(userPhotos) withObject:nil afterDelay:0.1];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"getUserFeed"])
    {
        self.feedData = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"data"]];
        [self.table reloadData];
    }
//    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"followUser"])
//    {
//        [self.settingButton setTitle:@"Unfollow" forState:UIControlStateNormal];
//    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"getNewFollower"])
    {
        if([[object objectForKey:@"data"] count] > 0)
        {
            self.notificationBtn.hidden = NO;
        [self.notificationBtn setTitle:[NSString stringWithFormat:@"+%d",[[object objectForKey:@"data"] count]] forState:UIControlStateNormal];
        }
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"followUser"])
    {
        [self.settingButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        [self userDetailsCall];
    }

    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"dropFollowing"])
    {
        [self.settingButton setTitle:@"Follow" forState:UIControlStateNormal];
        [self userDetailsCall];
    }
    else if([[object objectForKey:@"status"] isEqualToString:@"success"] && [networkManager.identifier isEqualToString:@"removetag"])
    {
        [self.table reloadData];
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:@"Tag removed successfully"];
    }
    else
    {
        [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:[object valueForKey:@"message"]];
    }
}

- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[SharedAppManager sharedInstance] initWithTitle:ALERTVIEW_TITLE message:message];
}

#pragma mark - View Dealloc

- (void)dealloc {
    [_table release];
    [_profilePic release];
    [_settingButton release];
    [_followersLbl release];
    [_followingLbl release];
    [_descriptionLabel release];
    [_descriptionBackground release];
    [_profileView release];
    [_profileScrollView release];
    [_notificationBtn release];
    [_partyRatingLbl release];
    [super dealloc];
}

@end
