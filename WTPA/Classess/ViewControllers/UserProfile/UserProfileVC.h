//
//  UserProfileVC.h
//  WTPA
//
//  Created by Admin on 19/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileVC : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil with:(NSString *)userid status:(BOOL)follow;
-(void)resetAllProperties;

@property (retain, nonatomic) IBOutlet UITableView *table;
@property (retain, nonatomic) IBOutlet UIScrollView *profileScrollView;
@property (retain, nonatomic) IBOutlet UIButton *settingButton;
@property (retain, nonatomic) IBOutlet UIView *profileView;
@property (retain, nonatomic) IBOutlet UILabel *followersLbl;
@property (retain, nonatomic) IBOutlet UILabel *followingLbl;
@property (retain, nonatomic) IBOutlet UIImageView *descriptionBackground;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIButton *notificationBtn;
- (IBAction)followingBtnPressed:(id)sender;
- (IBAction)followersBtnPressed:(id)sender;
- (IBAction)settingsBtnPressed:(id)sender;
- (IBAction)notificationBtnPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *partyRatingLbl;

@end
