//
//  FaceBookManager.h
//  ICB_SmarterTableViewCell
//
//  Created by OSM LLC on 1/16/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookEngine.h"
#import "Facebook.h"

typedef enum {
    RequestGetFriends,
    RequestPostImage,
    RequestPostStatus,
    RequestPostFeed
} RequestType;

@interface FaceBookManager : NSObject <FacebookEngineDelegate>
{
    RequestType requestType;
    FacebookEngine *engine;
}

@property (nonatomic, readwrite) RequestType requestType;
@property (nonatomic, assign) BOOL justLoginFlag;

+ (FaceBookManager*)sharedManager;
- (void)initializeFacebookEngine;

- (Facebook*)faceBookInstance;

- (void)checkIfFacebookIsLoggedIn;
- (void)loginFacebook;
- (void)logOutFacebook;

- (void)publishImageWithLocation:(NSString *)locationId withImage:(UIImage *)image;
- (void)uploadStausWithText:(NSString*)text;
- (void)postFeed;
- (void)postImageWithImage:(UIImage*)image;
- (void)publishWithImageURL:(NSString *)imageURL withImageRef:(NSString *)imageRef withItemName:(NSString *)itemName withDescription:(NSString *)desc withItemLink:(NSString *) itemLink;

@end
