//
//  FaceBookManager.m
//  ICB_SmarterTableViewCell
//
//  Created by OSM LLC on 1/16/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import "FaceBookManager.h"
#import "AppUtils.h"
#import "AJNotificationView.h"
#import "AppDelegate.h"

@interface FaceBookManager () {
@private
}

@property (nonatomic, retain) NSString *statusText;
@property (nonatomic, retain) NSDictionary *feedDictionary;
@property (nonatomic, retain) UIImage *albumImage;

- (void)checkIfFacebookIsLoggedIn;
@end



@implementation FaceBookManager
@synthesize requestType;
@synthesize justLoginFlag;
@synthesize statusText;
@synthesize albumImage;

static FaceBookManager* mySharedInstance = nil;

+ (FaceBookManager*)sharedManager
{
    @synchronized(self) 
	{ 
		if (mySharedInstance == nil) { 
			//[[self alloc] init]; 
			mySharedInstance = [[FaceBookManager alloc] init];
			
		} 
		return mySharedInstance;
	} 
	return nil; 
}

- (id)init
{
    if (self = [super init]) 
    {
        [self initializeFacebookEngine];
    }
    return self;
}

- (void)initializeFacebookEngine
{
    engine = [[FacebookEngine alloc]init];
    engine.delegate = self;
}

- (Facebook*)faceBookInstance
{
    return [engine faceBook];
}

- (void)loginFacebook
{
    if(![engine isLoggedIn])
        [engine loginFacebook];
}
- (void)logOutFacebook
{
    if([engine isLoggedIn])
        [engine logoutFacebook];
}

- (void)checkIfFacebookIsLoggedIn
{
    if(![engine isLoggedIn])
    {
        [engine loginFacebook];
        return;
    }
}


- (void)uploadStausWithText:(NSString*)text 
{	
    self.statusText = text;
    
    [self checkIfFacebookIsLoggedIn];
	[engine publishStream:text];
}

- (void)postFeed
{
    [self checkIfFacebookIsLoggedIn];
}

- (void)postImageWithImage:(UIImage*)image
{
    [self checkIfFacebookIsLoggedIn];
}

- (void)publishWithImageURL:(NSString *)imageURL withImageRef:(NSString *)imageRef withItemName:(NSString *)itemName withDescription:(NSString *)desc withItemLink:(NSString *)itemLink
{
    self.feedDictionary = [NSDictionary dictionaryWithObjectsAndKeys:imageURL,@"imageURL",imageRef,@"imageRef",itemName,@"itemName",desc,@"desc",itemLink,@"itemLink", nil];
    
    [self checkIfFacebookIsLoggedIn];
    [engine publishStreamWithImageURL:imageURL withImageRef:imageRef withItemName:itemName withDescription:desc withItemLink:itemLink];
}

- (void)publishImageWithLocation:(NSString *)locationId withImage:(UIImage *)image
{    
    [self checkIfFacebookIsLoggedIn];
    [engine publishStreamWithLocation:locationId withAlbumImage:image];
}



#pragma mark -
#pragma mark Facebook Engine Delegate Methods
- (void)fbEngine:(FacebookEngine*)_engine facebookDidLogin:(BOOL)status {
	
    if(status) {
        ////NSLog(@"facebookDidLogin in appDelagte");
        if(self.requestType == RequestPostStatus)
            [self uploadStausWithText:self.statusText];
        
        else if(self.requestType == RequestPostFeed)
            [self publishWithImageURL:[self.feedDictionary objectForKey:@"imageURL"] withImageRef:[self.feedDictionary objectForKey:@"imageRef"] withItemName:[self.feedDictionary objectForKey:@"itemName"] withDescription:[self.feedDictionary objectForKey:@"desc"] withItemLink:[self.feedDictionary objectForKey:@"itemLink"]];
    }
    else
    {
        NSLog(@"facebookDidNOTLogin in FaceBookManager");
        
        [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                        type:AJNotificationTypeRed
                                       title:@"Did not login, Try again!"
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
        
    }
}
- (void)fbEngine:(FacebookEngine*)_engine facebookDidLogout:(BOOL)status {
	NSLog(@"facebookDidLogout in FaceBookManager");
}
- (void)fbEngine:(FacebookEngine*)_engine facebookDidUpdate:(BOOL)status {
	NSLog(@"facebookDidUpdate in FaceBookManager");
}
- (void)fbEngine:(FacebookEngine*)_engine facebookDidReceivedError:(NSError*)error {
    
	NSLog(@"facebookDidReceivedError in FaceBookManager: %@",error.localizedDescription);
    NSLog(@"Err details: %@", [error description]);
    
    if([AppUtils ifString:@"offline" isFoundInString:error.localizedDescription])
    {
        [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                        type:AJNotificationTypeRed
                                       title:@"Item could not be posted, Please check your internet connection!"
                             linedBackground:AJLinedBackgroundTypeDisabled
                                   hideAfter:2.5f];
    }
}

- (void)fbEngine:(FacebookEngine*)_engine dialodLoadingDidFailWithError:(NSError*)error {
	NSLog(@"dialodLoadingDidFailWithError in FaceBookManager");
}
- (void)fbEngine:(FacebookEngine*)_engine dialogDidSucceed:(BOOL)status {
	NSLog(@"dialogDidSucceed in FaceBookManager");
}
- (void)fbEngine:(FacebookEngine*)_engine dialogWillSucceed:(BOOL)status withURL:(NSURL*)url {
	NSLog(@"dialogWillSucceed in FaceBookManager");
}
- (BOOL)fbEngine:(FacebookEngine*)_engine externalBrowserShouldOpenURL:(NSURL*)url {
	NSLog(@"externalBrowserShouldOpenURL in FaceBookManager");
    
    return YES;
}


- (void)fbEngine:(FacebookEngine*)engine photoDidPostedWithMessage:(NSString*)message
{
    //Show alert with message
    [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                    type:AJNotificationTypeGreen
                                   title:@"Image has been successfully posted to your facebook account"
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
}
- (void)fbEngine:(FacebookEngine*)engine feedDidPostedWithMessage:(NSString*)message
{
    //Show alert with message
    [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
                                    type:AJNotificationTypeGreen
                                   title:@"Item has been successfully posted to your facebook account"
                         linedBackground:AJLinedBackgroundTypeDisabled
                               hideAfter:2.5f];
}


@end
