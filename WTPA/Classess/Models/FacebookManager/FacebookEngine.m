//
//  FacebookEngine.m
//  TestProject
//
//  Created by OSM LLC on 4/11/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import "FacebookEngine.h"
#import "AJNotificationView.h"
#import "SBJSON.h"
#import "AppDelegate.h"

//#define kApplicationID		@"103657569843797"
#define kApplicationID		@"170559459812342"
#define kApplicationSecret	@"4ed375ed62b2fc3271b930d5a62e621b"

#define kTitleName			NSLocalizedString(@"WTPA", @"Application Title.")
#define kCaption			NSLocalizedString(@"Reference Documentation", @"Application Title.")
#define kDescription		NSLocalizedString(@"Dialogs provide a simple, consistent interface for apps to interact with users.", @"Application Title.")

@interface FacebookEngine (Private)
- (void)getUserInfo;
@end



@implementation FacebookEngine

@synthesize delegate, isStatus;


- (id)init {
	if (self = [super init]) 
    {
		isLoggedIn = NO;
		
		permissions = [[NSArray alloc] initWithObjects:@"read_stream",@"user_about_me",@"user_photos",@"publish_stream",@"email",@"offline_access",nil];
		facebook = [[Facebook alloc] initWithAppId:kApplicationID];
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"] != nil)	{
			facebook.accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"];
            [[NSUserDefaults standardUserDefaults]synchronize];
		}
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ExpirationDate"] != nil)	{
			facebook.expirationDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"ExpirationDate"];
		}
		
		//[[OfficeAppDelegate sharedDelegate] setFaceBookInstance:facebook];
		
	}
	return self;
}

- (void) dealloc {
	[personalInfo release];
	[facebook release];
	[permissions release];
	[super dealloc];
}

- (Facebook*)faceBook
{
    return facebook;
}

- (BOOL)isLoggedIn	{
	isLoggedIn = [facebook isSessionValid];
	return isLoggedIn;
}

- (void)loginFacebook	{
	[facebook authorize:permissions delegate:self];
}

- (void)logoutFacebook	{
	[facebook logout:self];
}

- (void)getUserInfo	{
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   @"SELECT uid, name, pic FROM user WHERE uid = me() ", @"query", nil];
	
	[facebook requestWithMethodName:@"fql.query"
						  andParams:params
					  andHttpMethod:@"POST"
						andDelegate:self];
}

- (void)publishStreamWithInputDialogBox {
	SBJSON *jsonWriter = [[SBJSON new] autorelease];
	
	NSDictionary *actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
														   @"Always Running", @"text", @"http://itsti.me/", @"href", nil], nil];
	
	NSString *actionLinksString = [jsonWriter stringWithObject:actionLinks];
	NSDictionary *attachment = [NSDictionary dictionaryWithObjectsAndKeys:
								kTitleName, @"name",
								kCaption, @"caption",
								kDescription, @"description",
								@"http://itsti.me/", @"href", nil];
	
	NSString *attachmentString = [jsonWriter stringWithObject:attachment];
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kApplicationID, @"api_key",
								   @"Share on Facebook",  @"user_message_prompt",
								   actionLinksString, @"action_links",
								   attachmentString, @"attachment", nil];
	
	[facebook dialog:@"feed" andParams:params andDelegate:self];
}

- (void)publishStream:(NSString*)statusText {
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   statusText, @"message", nil];
	
	[facebook requestWithGraphPath:@"feed" 
						 andParams:params
					 andHttpMethod:@"POST" 
					   andDelegate:self];
}

- (void)publishStream:(NSString*)statusText withLinkURL:(NSString*)urlString {
    
	NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kApplicationID, @"app_id",
								   @"http://developers.facebook.com/docs/reference/dialogs/", @"link",
								   urlString, @"picture",
								   statusText, @"name",
								   kTitleName, @"caption",
								   kDescription, @"description",
                                   @"http://itsti.me/", @"link",
                                   nil];
	
    [facebook requestWithGraphPath:@"feed"
						 andParams:params
					 andHttpMethod:@"POST"
					   andDelegate:self];
}

- (void)publishStream:(NSString*)statusText withAlbumImage:(UIImage*)image; {
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   image, @"picture", statusText, @"message", nil];
	
	[facebook requestWithGraphPath:@"/me/photos" 
						 andParams:params
					 andHttpMethod:@"POST" 
					   andDelegate:self];
}

- (void)publishStreamWithLocation:(NSString*)locationId withAlbumImage:(UIImage*)image
{
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   image, @"picture", locationId, @"place", nil];
	
	[facebook requestWithGraphPath:@"/me/photos"
						 andParams:params
					 andHttpMethod:@"POST"
					   andDelegate:self];
}


- (void)publishStream:(NSString*)statusText withAlbumImageFromURL:(NSString*)urlString {
    
	NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
	UIImage *image  = [[UIImage alloc] initWithData:imageData];
    
	[self publishStream:statusText withAlbumImage:image];
	[image release];
}

- (void)publishStreamWithImageURL:(NSString *)imageURL withImageRef:(NSString *)imageRef withItemName:(NSString *)itemName withDescription:(NSString *)desc withItemLink:(NSString *)itemLink
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kApplicationID, @"app_id",
								   imageURL, @"picture",
								   itemName, @"name",
								   kTitleName, @"caption",
								   desc, @"description",
                                   itemLink, @"link",
                                   nil];
	
    [facebook requestWithGraphPath:@"feed"
						 andParams:params
					 andHttpMethod:@"POST"
					   andDelegate:self];
}

- (void)publishLink:(NSString*)urlString {
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   @"http://developers.facebook.com/docs/reference/dialogs/", @"link",
								   urlString, @"picture",
								   kTitleName, @"name",
								   kCaption, @"caption",
								   kDescription, @"description", nil];
	
	[facebook requestWithGraphPath:@"feed" 
						 andParams:params
					 andHttpMethod:@"POST" 
					   andDelegate:self];
}

- (void)publishAlbumImage:(UIImage*)image {
	NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:image, @"picture", nil];
	
	[facebook requestWithMethodName:@"photos.upload"
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];
}

- (void)publishAlbumImageFromURL:(NSString*)urlString {
	NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
	UIImage *image  = [[UIImage alloc] initWithData:imageData];
	[self publishAlbumImage:image];
	[image release];
}


#pragma mark -
#pragma mark FBSessionDelegate

- (void)fbDidLogin	{
	[[NSUserDefaults standardUserDefaults] setObject:facebook.accessToken forKey:@"AccessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:facebook.expirationDate forKey:@"ExpirationDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
	[self getUserInfo];
//    [AJNotificationView showNoticeInView:[[AppDelegate sharedDelegate] window]
//                                    type:AJNotificationTypeGreen
//                                   title:@" Successfully."
//                         linedBackground:AJLinedBackgroundTypeDisabled
//                               hideAfter:2.5f];
	if ([self.delegate respondsToSelector:@selector(fbEngine:facebookDidLogin:)])
		[self.delegate fbEngine:self facebookDidLogin:YES];
}

- (void)fbDidNotLogin:(BOOL)cancelled	
{
	[[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AccessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"ExpirationDate"];
    
	if ([self.delegate respondsToSelector:@selector(fbEngine:facebookDidLogin:)])
		[self.delegate fbEngine:self facebookDidLogin:NO];
}

- (void)fbDidLogout	
{
	[[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AccessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"ExpirationDate"];
    
	if ([self.delegate respondsToSelector:@selector(fbEngine:facebookDidLogout:)])
		[self.delegate fbEngine:self facebookDidLogout:YES];
}

#pragma mark FBRequestDelegate
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
	////NSLog(@"request:didReceiveResponse: %@",[[response URL] absoluteString]);
	NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
	////NSLog(@"statusCode: %i",statusCode);
    if (statusCode == 200) {
        NSString *url = [[response URL] absoluteString];
		//NSLog(@"url: %@",url);
        if ([url rangeOfString: @"me/photos"].location != NSNotFound) 
        {
            ////NSLog(@"Request Params: %@", [request params]);
            
            if ([self.delegate respondsToSelector:@selector(fbEngine:photoDidPostedWithMessage:)])
                [self.delegate fbEngine:self photoDidPostedWithMessage:@"message"];
        }
		else if ([url rangeOfString: @"feed"].location != NSNotFound) {
            ////NSLog(@"Request Params: %@", [request params]);
            if ([self.delegate respondsToSelector:@selector(fbEngine:feedDidPostedWithMessage:)])
                [self.delegate fbEngine:self feedDidPostedWithMessage:@"message"];
        }
    }
}

- (void)request:(FBRequest *)request didLoad:(id)result {
	if ([result isKindOfClass:[NSArray class]]) {
		result = [result objectAtIndex:0];
	}
	personalInfo = [[NSDictionary alloc] initWithDictionary:result];
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
	if ([self.delegate respondsToSelector:@selector(fbEngine:facebookDidReceivedError:)])
		[self.delegate fbEngine:self facebookDidReceivedError:error];
}


#pragma mark FBDialogDelegate
// Called when the dialog succeeds and is about to be dismissed.
- (void)dialogDidComplete:(FBDialog *)dialog {
	////NSLog(@"dialogDidComplete:"); // after call dialogCompleteWithUrl delegate method
	if ([self.delegate respondsToSelector:@selector(fbEngine:dialogDidSucceed:)])
		[self.delegate fbEngine:self dialogDidSucceed:YES];
}
// Called when the dialog succeeds with a returning url.
- (void)dialogCompleteWithUrl:(NSURL *)url {
	////NSLog(@"dialogCompleteWithUrl:"); // direct call after publish stream.
	if ([self.delegate respondsToSelector:@selector(fbEngine:dialogWillSucceed:withURL:)])
		[self.delegate fbEngine:self dialogWillSucceed:YES withURL:url];
}
// Called when the dialog get canceled by the user.
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url {
	////NSLog(@"dialogDidNotCompleteWithUrl:");
	if ([self.delegate respondsToSelector:@selector(fbEngine:dialogWillSucceed:withURL:)])
		[self.delegate fbEngine:self dialogWillSucceed:NO withURL:url];
}

// Called when the dialog is cancelled and is about to be dismissed.
- (void)dialogDidNotComplete:(FBDialog *)dialog {
	////NSLog(@"dialogDidNotComplete:");
	if ([self.delegate respondsToSelector:@selector(fbEngine:dialogDidSucceed:)])
		[self.delegate fbEngine:self dialogDidSucceed:NO];
}
// Called when dialog failed to load due to an error.
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error {
	////NSLog(@"dialog:didFailWithError:");
	if ([self.delegate respondsToSelector:@selector(fbEngine:dialodLoadingDidFailWithError:)])
		[self.delegate fbEngine:self dialodLoadingDidFailWithError:error];
}
/**
 * Asks if a link touched by a user should be opened in an external browser.
 * If a user touches a link, the default behavior is to open the link in the Safari browser, which will cause your app to quit.  You may want to prevent this from happening, open the link in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you should hold onto the URL and once you have received their acknowledgement open the URL yourself using [[UIApplication sharedApplication] openURL:].
 **/
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL *)url {
	////NSLog(@"dialog:shouldOpenURLInExternalBrowser:");
	if ([self.delegate respondsToSelector:@selector(fbEngine:externalBrowserShouldOpenURL:)])
		return [self.delegate fbEngine:self externalBrowserShouldOpenURL:url];
	else
		return YES;
}


@end

