//
//  FacebookEngine.h
//  TestProject
//
//  Created by OSM LLC on 4/11/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facebook.h"


@protocol FacebookEngineDelegate;

@interface FacebookEngine : NSObject <FBSessionDelegate, FBRequestDelegate, FBDialogDelegate>
{
    id <FacebookEngineDelegate> delegate;
	
	Facebook *facebook;
	NSArray *permissions;
	NSDictionary *personalInfo;
	
	BOOL isLoggedIn;
	BOOL isStatus;
}

@property (nonatomic, assign) id <FacebookEngineDelegate> delegate;
@property (nonatomic, readonly) BOOL isLoggedIn;
@property BOOL isStatus;

- (void)loginFacebook;
- (void)logoutFacebook;

- (Facebook*)faceBook;

- (void)publishStreamWithInputDialogBox;
- (void)publishStream:(NSString*)statusText;
- (void)publishStream:(NSString*)statusText withLinkURL:(NSString*)urlString;
- (void)publishStream:(NSString*)statusText withAlbumImage:(UIImage*)image;
- (void)publishStream:(NSString*)statusText withAlbumImageFromURL:(NSString*)urlString;
- (void)publishStreamWithImageURL:(NSString *)imageURL withImageRef:(NSString *)imageRef withItemName:(NSString *)itemName withDescription:(NSString *)desc withItemLink:(NSString *)itemLink;
- (void)publishLink:(NSString*)urlString;
- (void)publishAlbumImage:(UIImage*)image;
- (void)publishAlbumImageFromURL:(NSString*)urlString;
- (void)publishStreamWithLocation:(NSString*)locationId withAlbumImage:(UIImage*)image;
@end

@protocol FacebookEngineDelegate <NSObject>
@optional
- (void)fbEngine:(FacebookEngine*)engine facebookDidLogin:(BOOL)status;
- (void)fbEngine:(FacebookEngine*)engine facebookDidLogout:(BOOL)status;
- (void)fbEngine:(FacebookEngine*)engine facebookDidUpdate:(BOOL)status;
- (void)fbEngine:(FacebookEngine*)engine facebookDidReceivedError:(NSError*)error;

- (void)fbEngine:(FacebookEngine*)engine dialodLoadingDidFailWithError:(NSError*)error;
- (void)fbEngine:(FacebookEngine*)engine dialogDidSucceed:(BOOL)status;
- (void)fbEngine:(FacebookEngine*)engine dialogWillSucceed:(BOOL)status withURL:(NSURL*)url;
- (BOOL)fbEngine:(FacebookEngine*)engine externalBrowserShouldOpenURL:(NSURL*)url;

- (void)fbEngine:(FacebookEngine*)engine photoDidPostedWithMessage:(NSString*)message;
- (void)fbEngine:(FacebookEngine*)engine feedDidPostedWithMessage:(NSString*)message;

@end
