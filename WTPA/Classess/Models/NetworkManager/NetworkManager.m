//
//  NetworkManager.m
//  virgin holidays
//
//  Created by Admin on 19/06/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager ()

@property (nonatomic,retain) NSMutableData* receivedData;
@end


@implementation NetworkManager

- (void)sendRequestforAction:(NSMutableDictionary *) parameters
{
    self.identifier=[NSString stringWithFormat:@"%@",[parameters objectForKey:@"idetntifier"]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:
                                [NSURL URLWithString:[parameters objectForKey:@"url"]]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                          timeoutInterval:150.0];
    NSDictionary *header=[parameters objectForKey:@"headers"];
    
    for (id key in header.allKeys) {
        [theRequest setValue:[header objectForKey:key] forHTTPHeaderField:[NSString stringWithFormat:@"%@",key]];
    }

    NSDictionary * data = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN])
    {
        [theRequest setValue:AUTH_VALUE forHTTPHeaderField:AUTH_KEY];
    }
    else
    {
        [theRequest setValue:[data objectForKey:@"key"] forHTTPHeaderField:AUTH_KEY];
    }
    
    [theRequest setValue:@"300" forHTTPHeaderField:@"limit"];
    
    id parameterData=[parameters objectForKey:@"param"];

    if([parameterData isKindOfClass:[NSString class]])
    {
        if(![parameterData isEqualToString:@"{}"])
        {
            [theRequest setHTTPBody:[parameterData dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    else if([parameterData isKindOfClass:[NSData class]])
    {
        [theRequest setHTTPBody:parameterData];
    }
    
    [theRequest setHTTPMethod:[parameters objectForKey:@"callType"]];
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        self.receivedData = [[NSMutableData alloc] init];
    } else {
        // Inform the user that the connection failed.
        [self requestFailedWithError:@"The Internet connection appears to be offline"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    if ([response isKindOfClass: [NSHTTPURLResponse class]])
    {
        [self.receivedData setLength:0];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    [connection release];
    // receivedData is declared as a method instance elsewhere
    [self.receivedData release];
    
    // inform the user
    
    [self requestFailedWithError:error.localizedDescription];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
//    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
//    NSLog(@"Data: %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);    // release the connection, and the data object
    [connection release];
    NSString *string=[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    
    if ([self.identifier isEqualToString:@"feedback"])
    {
        if ([_delegate respondsToSelector:@selector(networkManager:didFinishDownload:)])
        {
            [_delegate networkManager:self didFinishDownload:string];
            return;
        }
    }
    
    NSError* error=nil;
    id json = [NSJSONSerialization
                          JSONObjectWithData:self.receivedData //1
                          options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves
                          error:&error];
    
    
    if (error)
    {
        NSLog(@"JSON Error:%@",[error localizedDescription]);
        [self requestFailedWithError:[error localizedDescription]];
    }
    else
    {
        [self requestCompletedWithData:json];
    }
}

- (void)requestCompletedWithData:(id)data
{
    if ([_delegate respondsToSelector:@selector(networkManager:didFinishDownload:)])
    {
        [_delegate networkManager:self didFinishDownload:data];
    }
}

- (void)requestFailedWithError:(NSString*)errorMessage
{
    if ([_delegate respondsToSelector:@selector(networkManager:didFailedWithErrorMessage:)])
    {
        [_delegate networkManager:self didFailedWithErrorMessage:errorMessage];
    }
}
-(void) dealloc
{
    [self.receivedData release],self.receivedData=nil;
    self.delegate=nil;
    [super dealloc];
}

@end
