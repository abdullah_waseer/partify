//
//  NetworkManager.h
//  virgin holidays
//
//  Created by Admin on 19/06/2013.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>



@class  NetworkManager;
@protocol NetworkManagerDelegate <NSObject>

@optional
- (void)networkManager:(NetworkManager*)networkManager didFinishDownload:(id)object;
- (void)networkManager:(NetworkManager*)networkManager didFailedWithErrorMessage:(NSString*)message;

@end


@interface NetworkManager : NSObject
{
//    NSMutableData* receivedData;
}

@property (nonatomic, assign) id<NetworkManagerDelegate> delegate;
@property (nonatomic, retain) NSString *identifier;

- (void)sendRequestforAction:(NSMutableDictionary *) parameters;



@end
