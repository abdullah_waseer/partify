//
//  UITextField+UITextField_Utitlities.m
//  WTPA
//
//  Created by Ishaq Shafiq on 25/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "UITextField+UITextField_Utitlities.h"

@implementation UITextField (UITextField_Utitlities)

- (void) drawPlaceholderColorInRect:(CGRect)rect {
    //search field placeholder color
    UIColor* color = [UIColor whiteColor];
    
    [color setFill];
    [self.placeholder drawInRect:rect withFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
}

@end
