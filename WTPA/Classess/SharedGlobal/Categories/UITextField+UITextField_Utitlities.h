//
//  UITextField+UITextField_Utitlities.h
//  WTPA
//
//  Created by Ishaq Shafiq on 25/11/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (UITextField_Utitlities)

- (void) drawPlaceholderColorInRect:(CGRect)rect ;
@end
