//
//  CUIButton.h
//  HEO
//
//  Created by Smonte on 4/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CUIButton : UIButton {

	NSInteger index;
    NSString *titleName;;
}

@property (nonatomic) NSInteger index;
@property (nonatomic,retain) NSString *titleName;

@end
