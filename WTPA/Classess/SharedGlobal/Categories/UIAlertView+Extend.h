/*
 *  UIAlertView+Extend.h
 *  myFriends
 *
 *  Created by Will Strafach on 8/10/09.
 *  Copyright 2009 Chronic Dev. All rights reserved.
 *
 */

#define allTrim(object) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

@interface UIAlertView (extended)

- (NSString*)tfText;
- (void)addTFieldWithValue:(NSString *)value label:(NSString *)label;
@end

@implementation UIAlertView (extended)
- (void)addTFieldWithValue:(NSString *)value label:(NSString *)label {
    	
	UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	tf.backgroundColor = [UIColor whiteColor];
	tf.clearButtonMode = UITextFieldViewModeNever;
	tf.keyboardType = UIKeyboardTypeAlphabet;
	tf.placeholder = label;
	tf.autocapitalizationType = UITextAutocapitalizationTypeNone;
	tf.autocorrectionType = UITextAutocorrectionTypeNo;
	tf.clearsOnBeginEditing = NO;
	tf.enablesReturnKeyAutomatically = YES;
	tf.returnKeyType = UIReturnKeySend;
	[self addSubview:tf];
	[tf release];
}

- (NSString*)tfText 
{
    NSString *text = nil;
    
    NSArray *allSubViews = [self subviews];
    for(UITextField *tField in allSubViews)
    {
        if([tField isKindOfClass:[UITextField class]])
            text = tField.text;
    }
    
    if([allTrim(text) length] == 0)
        text = @"";
    
	return text;
}

@end