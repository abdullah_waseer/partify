//
//  AppUtils.h
//  BetWithFriends
//
//  Created by OSM LLC on 1/2/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppUtils : NSObject

+ (NSString *)timeFormatted: (NSDate *)fdate toDate:(NSDate *)tdate;
+ (NSString*)getNibName:(NSString*)nibName;

+ (NSInteger)totalNumberOfSecondsWithTheseSettings:(NSDictionary*)settings;
+ (NSDictionary*)timeSettings;


+ (NSString*)getStrindFromDate:(NSDate*)date;
+ (NSString*)getStrindFromDate:(NSDate*)date withFormat:(NSString*)format;
+ (NSDate*)getDateFromString:(NSString*)stirng withFormat:(NSString*)format;
+ (NSString *)firstThreeStringLiteralsInString:(NSString*)string;
+ (NSString*)decodeEncode:(NSString*)url;
+ (NSString*)formatURL:(NSString*)urlToFormat;
+ (NSString*)formattedURL:(NSString*)urlToFormat;
+ (NSString*)getiPhoneNibName:(NSString*)nibName;

+ (BOOL)ifString:(NSString*)string isFoundInString:(NSString*)completeString;
+ (BOOL)ifEmptyString:(NSString*)string;
+ (BOOL)valueAlreadyExistsWithValue:(int)value withElements:(NSArray*)array;
+ (BOOL)validateEmail:(NSString *)emailString;
+ (BOOL)validateUKpostalCode:(NSString *)postalCode;
+ (BOOL)date:(NSDate*)currentDay isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;
+ (BOOL)ifView:(UIView*)view isFoundInView:(UIView*)container;

+ (NSInteger)indexForThisDay:(NSString*)theDay;
+ (NSDate *)dateByAddingSystemTimeZoneToThisDate:(NSDate *)sourceDate;
+ (NSDate *)currentDateByLocalTimeZone;
+ (NSDate *)nearestDateFromDay:(NSString*)theDay;
+ (NSDate *)nearestDateForWeekDay:(NSInteger)weekDay withReferenceDate:(NSDate*)refDate;
+ (NSDate *)lastDateForWeekDay:(NSInteger)weekDay withReferenceDate:(NSDate*)refDate;
+ (NSDate *)dateByAddingDays:(NSUInteger)days;
+ (NSDate *)dateByAddingDays:(NSUInteger)days toDate:(NSDate*)date;
+ (NSDate *)dateByAddingMinutes:(NSUInteger)minutes toDate:(NSDate*)date;
+ (NSDate *)dateByAddingSeconds:(NSUInteger)seconds toDate:(NSDate*)date;
+ (NSDate *)dateByAddingSystemTimeZoneToDate:(NSDate*)date;
+ (NSDate *)dateWithOutTimeWithDate:(NSDate*)date;
+ (NSDate *)dateWithWeekNumber:(NSInteger)weekNmber withYear:(NSInteger)year;

+ (NSDictionary*)updateTimerWithNumSeconds:(NSInteger)seconds;

+ (NSInteger)hoursBetweenDate:(NSDate*)d;
+ (NSInteger)minutesBetweenDate:(NSDate*)d;
+ (NSInteger)minutesBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2;
+ (NSInteger)secondsBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2;
+ (NSInteger)daysBetweenDate:(NSDate*)date;
+ (NSInteger)daysBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2;
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

+ (void) loadFonts;
+ (NSString *)globallyUniqueString;

@end
