//
//  AppUtils.m
//  BetWithFriends
//
//  Created by OSM LLC on 1/2/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import "AppUtils.h"
#import "NSDate-Utilities.h"
#import <dlfcn.h>


#define INTERFACE_IS_PAD     ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define INTERFACE_IS_PHONE   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)

#define DAYS [NSArray arrayWithObjects: @"-1",@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday", nil]

#define DAY_TYPES [NSArray arrayWithObjects: @"Saturday",@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",nil]

#define WEEK_DAYS [NSArray arrayWithObjects:@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday", nil]


@implementation AppUtils

+ (NSInteger)totalNumberOfSecondsWithTheseSettings:(NSDictionary*)settings
{
    
    NSInteger hours = [[settings objectForKey:@"hour"] integerValue];
    NSInteger minutes = [[settings objectForKey:@"minute"] integerValue];
    NSInteger seconds = [[settings objectForKey:@"second"] integerValue];
    
    NSInteger totalSeconds = (hours * 60 * 60) + (minutes * 60) + seconds;
    
    ////NSLog(@"totalSeconds: %i",totalSeconds);
    
    return totalSeconds;
    
}
+ (NSString*)getiPhoneNibName:(NSString*)nibName
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height > 480.0f)
    {
        return [NSString stringWithFormat:@"%@iPhone",nibName];
    }
    else
    {
        return nibName;
    }
}


+ (NSString*)getNibName:(NSString*)nibName
{
    if(INTERFACE_IS_PAD)
    {
        return [NSString stringWithFormat:@"%@~iPad",nibName];
    }
    else
    {
        return nibName;
    }

}

+ (NSDictionary*)timeSettings
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary *settings = nil;
	
	if([standardUserDefaults objectForKey:@"timeSettings"])
	{
		settings = [standardUserDefaults objectForKey:@"timeSettings"];
	}
    else
    {
        settings = [NSDictionary dictionaryWithObjectsAndKeys:@"01",@"hour",@"00",@"minute",@"00",@"second", nil];
        
        [standardUserDefaults setObject:settings forKey:@"timeSettings"];
        [standardUserDefaults synchronize];
    }
    
    return settings;
}


+ (NSDate*)dateByAddingSystemTimeZoneToThisDate:(NSDate *)sourceDate
{
    /*
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [NSDate dateWithTimeInterval:interval sinceDate:sourceDate];
    */
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM.dd G 'at' HH:mm:ss zzz"];
    //[formatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString *string = [formatter stringFromDate:sourceDate];
    //NSLog(@"dateString: %@",string);
    NSDate *newDate = [AppUtils getDateFromString:string withFormat:@"yyyy.MM.dd G 'at' HH:mm:ss zzz"];
    //NSLog(@"newDate: %@",newDate);
    return newDate;
}

+ (NSString*)getStrindFromDate:(NSDate*)date 
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd-MM-yyyy"];//yyyy
	NSString *string = [formatter stringFromDate:date];
	[formatter release];
	
	return string;
    
    
}


+(NSString*)getStrindFromDate:(NSDate*)date withFormat:(NSString*)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:format];//yyyy
	NSString *string = [formatter stringFromDate:date];
	[formatter release];
	
	return string;
}


+ (NSDate*)getDateFromString:(NSString*)stirng withFormat:(NSString*)format
{
    //////NSLog(@"string %@",stirng);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:format];
    NSDate *date = [dateFormat dateFromString:stirng];
    [dateFormat release];
    
    return date;
}

+ (NSString *)firstThreeStringLiteralsInString:(NSString*)string
{
    NSString *str = [string substringWithRange:NSMakeRange(0, 3)];
    //NSLog(@"str: %@",str);
    
    return str;
}

+ (NSString*)decodeEncode:(NSString*)url
{
    url = [url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];                                  //Decoding
    url = (NSString *)CFURLCreateStringByAddingPercentEscapes( NULL,
                                                              (CFStringRef)url,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8);                   //Encoding
    
    return url;
}

+ (NSString*)formatURL:(NSString*)urlToFormat
{
    
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@"\n" withString:@""];    //newline
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@"\t" withString:@""];    //Tab
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@" " withString:@""];     //Single space
    
    return urlToFormat;
}

+ (NSString*)formattedURL:(NSString*)urlToFormat
{
    urlToFormat = [urlToFormat stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@"\n" withString:@""];    //newline
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@"\t" withString:@""];    //Tab
    urlToFormat=[urlToFormat stringByReplacingOccurrencesOfString:@" " withString:@"%20"];     //Single space
    
    return urlToFormat;
}

//check if a string literal is found in a string component
+ (BOOL)ifString:(NSString*)string isFoundInString:(NSString*)completeString
{
	NSRange range = [completeString rangeOfString:string];
	if (range.length > 0) 
	{
		return YES;
	}
	
	return NO;
}

+(BOOL)ifEmptyString:(NSString*)string
{
    NSString *tempString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([tempString length] == 0)
        return YES;
    
    return NO;
}

+ (BOOL)valueAlreadyExistsWithValue:(int)value withElements:(NSArray*)array
{
    BOOL found = NO;
	for(int i=0;i<[array count];i++)
	{
		if (value == [[array objectAtIndex:i] intValue]) 
		{
			found = YES;
			break;
		}
	}
	
	return found;
}

+ (BOOL)validateEmail:(NSString *)emailString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}

+ (BOOL)validateUKpostalCode:(NSString *)postalCode
{
    NSString *postalCodeRegex = @"^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$";
    NSPredicate *postalCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", postalCodeRegex];
    return [postalCodeTest evaluateWithObject:postalCode];
}

+ (BOOL)date:(NSDate*)currentDay isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    NSDate *curDate = [AppUtils dateWithOutTimeWithDate:currentDay];
    NSDate *begDate = [AppUtils dateWithOutTimeWithDate:beginDate];
    NSDate *enDate = [AppUtils dateWithOutTimeWithDate:endDate];
    
//    //NSLog(@"\n curDate: %@",curDate);
//    //NSLog(@"beginDate: %@",beginDate);
//    //NSLog(@"endDate: %@",endDate);
    
    
    if ([curDate compare:begDate] == NSOrderedAscending)
        return NO;
    
    if ([curDate compare:enDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

+ (BOOL)ifView:(UIView*)view isFoundInView:(UIView*)container
{
    return [view.subviews containsObject:view];
}

+ (NSDate*)currentDateByLocalTimeZone
{
    return [AppUtils dateByAddingSystemTimeZoneToDate:[NSDate date]];
}

+ (NSDate*)nearestDateFromDay:(NSString*)theDay
{
    
    NSString *currentDay = [AppUtils getStrindFromDate:[NSDate date] withFormat:@"EEEE"];
    
    if([theDay isEqualToString:currentDay])
        return nil;
    
    
    NSInteger counter = 1;
    BOOL found = NO;
    
    NSInteger currentDayIndex = [AppUtils indexForThisDay:currentDay]+1;
    
    for(int i=currentDayIndex; i<[DAY_TYPES count] && !found; i++)
    {
        ////NSLog(@"i: %d",i);
        
        if([theDay isEqualToString:[DAY_TYPES objectAtIndex:i]])
        {
            found = YES;
            break;
        }
        
        
        counter ++;
        
        if((i == [DAY_TYPES count]-1) && !found)
        {
            i=-1;
        }
        
        
    }
    
    ////NSLog(@"counter: %i",counter);
    
    NSDate *theDate = [self dateByAddingDays:counter];
    //////NSLog(@"theDate: %@",theDate);
    
    return theDate;
    
}

+ (NSDate *)nearestDateForWeekDay:(NSInteger)weekDay withReferenceDate:(NSDate*)referenceDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit | NSWeekCalendarUnit |NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:referenceDate];
    
    int targetWeekday = weekDay;
    
    if (dateComponents.weekday > targetWeekday)
    {
        dateComponents.week++;
    }
    
    dateComponents.weekday = targetWeekday;
    NSDate *followingTargetDay = [calendar dateFromComponents:dateComponents];
    
    return followingTargetDay;
}

+ (NSDate *)lastDateForWeekDay:(NSInteger)weekDay withReferenceDate:(NSDate*)refDate
{
    NSString *theDay = [WEEK_DAYS objectAtIndex:weekDay-1];
    NSString *currentDay = [AppUtils getStrindFromDate:refDate withFormat:@"EEEE"];
    
    if([theDay isEqualToString:currentDay])
    {
        NSDate *theDate = [refDate dateBySubtractingDays:0];
        return theDate;
    }
    
    
    NSInteger counter = 0;
    BOOL found = NO;
    
    NSInteger currentDayIndex = [refDate weekday] - 1;
    ////NSLog(@"currentDayIndex: %i",currentDayIndex);
    
    for(int i=currentDayIndex; i>=0 && !found; i--)
    {
        ////NSLog(@"i: %d",i);
        
        ////NSLog(@"WEEK_DAYS: %@",[WEEK_DAYS objectAtIndex:i]);
        if([theDay isEqualToString:[WEEK_DAYS objectAtIndex:i]])
        {
            found = YES;
            break;
        }
        
        
        counter ++;
        
        if((i <= 0) && !found)
        {
            i = 7;
            ////NSLog(@"LOOP and counter: %i",counter);
        }
        
        
    }
    
    ////NSLog(@"counter: %i",counter);
    NSDate *theDate = [refDate dateBySubtractingDays:counter];
    
    return theDate;
}

+ (NSInteger)indexForThisDay:(NSString*)theDay
{
    NSInteger counter = 0;
    for(int i=0; i<[DAY_TYPES count];i++)
    {
        if([theDay isEqualToString:[DAY_TYPES objectAtIndex:i]])
        {
            break;
        }
        counter ++;
    }
    
    return counter;
}

+ (NSDate *)dateByAddingDays:(NSUInteger)days {
	NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease];
	c.day = days;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:[NSDate date] options:0];
}

+ (NSDate *)dateByAddingDays:(NSUInteger)days toDate:(NSDate*)date
{
	NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease];
	c.day = days;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:date options:0];
}

+ (NSDate *)dateByAddingMinutes:(NSUInteger)minutes toDate:(NSDate*)date
{
	NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease];
	c.minute = minutes;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:date options:0];
}

+ (NSDate *)dateByAddingSeconds:(NSUInteger)seconds toDate:(NSDate*)date
{
    NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease];
	c.second = seconds;
	return [[NSCalendar currentCalendar] dateByAddingComponents:c toDate:date options:0];
}

+ (NSDate *)dateByAddingSystemTimeZoneToDate:(NSDate*)sourceDate
{
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [NSDate dateWithTimeInterval:interval sinceDate:sourceDate];
    
    
    return destinationDate;
}

+ (NSDate *)dateWithOutTimeWithDate:(NSDate*)date
{
    if( date == nil ) {
        date = [NSDate date];
    }
    
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

+ (NSDate *)dateWithWeekNumber:(NSInteger)week withYear:(NSInteger)year
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *date = nil;
    NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
    
    [components setYear:year];
    [components setWeek:week];
    date = [cal dateFromComponents:components];
    
    return date;
}

+ (NSInteger)hoursBetweenDate:(NSDate*)d
{
	NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:d];
	return abs(time / 60 / 60);
}
+ (NSInteger)hoursBetweenDate:(NSDate*)d andthisDate:(NSDate *)d1
{
	NSTimeInterval time = [d1 timeIntervalSinceDate:d];
	return abs(time / 60 / 60);
}

+ (NSInteger)minutesBetweenDate:(NSDate*)d
{
    NSTimeInterval time = [d timeIntervalSinceDate:[NSDate date]];
	return (NSInteger)time / 60;
}

+ (NSInteger)minutesBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2
{
    NSTimeInterval time = [d1 timeIntervalSinceDate:d2];
	return (NSInteger)time / 60;
}

+ (NSInteger)secondsBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2
{
    NSTimeInterval time = [d1 timeIntervalSinceDate:d2];
	return (NSInteger)time;
}

+ (NSInteger)daysBetweenDate:(NSDate*)date
{
    NSTimeInterval time = [date timeIntervalSince1970];
	return abs(time / 60 / 60 / 24);
}

+ (NSInteger)daysBetweenThisDate:(NSDate*)d1 andDate:(NSDate*)d2
{
    NSTimeInterval time = [d1 timeIntervalSinceDate:d2];
	return (NSInteger)(time / 60 / 60 / 24);
}

+ (NSString *)timeFormatted: (NSDate *)fdate toDate:(NSDate *)tdate

{
    int seconds = [AppUtils secondsBetweenThisDate:fdate andDate:tdate];
    
    int minutes = [AppUtils minutesBetweenThisDate:fdate andDate:tdate];
    
    int hours = [AppUtils hoursBetweenDate:fdate andthisDate:tdate];
    
    int days=[AppUtils daysBetweenThisDate:fdate andDate:tdate];
    
    
    
    NSString *returnString;
    
    if(days >=1)
        
    {
        
        returnString= [NSString stringWithFormat:@"%i days ago",days];
        
    }
    
    else if (hours>=1)
        
    {
        
        returnString= [NSString stringWithFormat:@"%i Hours ago",hours];
        
    }
    
    else if (minutes>=1)
        
    {
        
        returnString= [NSString stringWithFormat:@"%i minutes ago",minutes];
        
    }
    
    else
        
    {
        
        returnString= [NSString stringWithFormat:@"%i seconds ago",seconds];
        
    }
    
    
    
    return returnString;
    
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

+ (NSDictionary*)updateTimerWithNumSeconds:(NSInteger)seconds
{    
	NSInteger hours = (NSInteger)seconds / 3600;
	NSInteger remainder = (NSInteger)seconds % 3600;
	NSInteger minutes = (NSInteger)(remainder / 60);
	NSInteger _seconds = (NSInteger)(remainder % 60);
	
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setValue:[NSNumber numberWithInteger:hours] forKey:@"hours"];
    [dictionary setValue:[NSNumber numberWithInteger:minutes] forKey:@"minutes"];
    [dictionary setValue:[NSNumber numberWithInteger:_seconds] forKey:@"seconds"];
    
    return dictionary;
}

+ (void) loadFonts
{
     NSUInteger newFontCount = 0;
     NSBundle *frameworkBundle = [NSBundle bundleWithIdentifier:@"com.apple.GraphicsServices"];
     const char *frameworkPath = [[frameworkBundle executablePath] UTF8String];
     if (frameworkPath)
     {
     void *graphicsServices = dlopen(frameworkPath, RTLD_NOLOAD | RTLD_LAZY);
     if (graphicsServices) {
     BOOL (*GSFontAddFromFile)(const char *) = dlsym(graphicsServices, "GSFontAddFromFile");
     if (GSFontAddFromFile)
     for (NSString *fontFile in [[NSBundle mainBundle] pathsForResourcesOfType:@"otf" inDirectory:nil])
     newFontCount += GSFontAddFromFile([fontFile UTF8String]);
     }
     }
     
//     NSString *family, *font;
//     for (family in [UIFont familyNames])
//     {
//         //NSLog(@"\nFamily: %@", family);
//     
//     for (font in [UIFont fontNamesForFamilyName:family])
//         //NSLog(@"\tFont: %@\n", font);
//     }
}

+ (NSString *)globallyUniqueString
{
    NSString *uniqueNumberString = [[NSProcessInfo processInfo] globallyUniqueString];
    NSArray *components = [uniqueNumberString componentsSeparatedByString:@"-"];
    uniqueNumberString = [components lastObject];
    //NSLog(@"uniqueNumberString: %@",uniqueNumberString);
    
    return uniqueNumberString;
}

@end
