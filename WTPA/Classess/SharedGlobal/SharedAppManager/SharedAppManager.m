//
//  SharedAppManager.m
//  BlueInc
//
//  Created by Admin on 3/29/13.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "SharedAppManager.h"
#import "NSData+AES.h"
#import "NSData+Base64.h"
#import "JSON.h"
#import "Instagram.h"

#define APP_ID @"02853be9dd2a4123ac54d20582e80c32"

@interface SharedAppManager ()

@end




@implementation SharedAppManager
static SharedAppManager* mySharedInstance = nil;

@synthesize delegate = _delegate;

+ (SharedAppManager*)sharedInstance
{
    @synchronized(self)
	{
		if (mySharedInstance == nil) {
			//[[self alloc] init];
			mySharedInstance = [[SharedAppManager alloc] init];
            
		}
		return mySharedInstance;
	}
	return nil;
}

- (id)init {
	self = [super init];
	if (self != nil) {

        [self makeConfigCall];
	}
	return self;
}

- (void)initWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}
                                //...... Config Download Call And responses...
- (void)makeConfigCall
{
//    [[NSURLCache sharedURLCache] removeAllCachedResponses];
//    NSURL* url=[NSURL URLWithString:CONFIG_URL];
//    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:url];
//    request.timeoutInterval=1000;
//    
//    NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    self.Instagram = [[Instagram alloc] initWithClientId:APP_ID
                                                delegate:nil];
    self.facebookToggle = YES;
    self.twitterToggle = YES;
    self.instagramToggle = YES;
    self.pushToggle = YES;
    self.baseURL=@"http://app.partifyapp.com/wtpa/";
    self.appStatus=[[NSArray alloc]initWithObjects:@"Active",@"",nil];;
//    if (data)
//    {
//        NSString* responseString = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease];
//        NSMutableDictionary* configDic = [[responseString JSONValue]copy];
//        
//        self.baseURL=configDic[@"baseUrl"];
//        self.appStatus=configDic[@"app_status"];
//    }
//    else
//    {
//         self.appStatus=[[NSArray alloc]initWithObjects:@"inActive",@"Error while downloading data",nil];
//    }
}

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width; // 161
    float scaleFactor = i_width / oldWidth; // 51/161 = 0.3167
    float newHeight = sourceImage.size.height * scaleFactor; //54.87
    float newWidth = oldWidth * scaleFactor;
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height; // 161
    float scaleFactor = i_height / oldHeight; // 51/161 = 0.3167
    float newHeight =  oldHeight * scaleFactor;//sourceImage.size.height * scaleFactor; //54.87
    float newWidth =  sourceImage.size.width * scaleFactor;//oldWidth * scaleFactor;
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSString *) returnDateAfterForamting:(NSDate *) value
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"eee dd MMM yyyy hh:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:value];
    
    return dateString;
}

-(NSDate *) returnDateAfterForamtingString:(NSString *) value
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"eee dd MMM yyyy hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:value];
    
    return date;
}

@end
