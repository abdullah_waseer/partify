//
//  SharedAppManager.h
//  BlueInc
//
//  Created by Admin on 3/29/13.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IGConnect.h"

@protocol SharedAppManagerDelegate <NSObject>

- (void)requestDidSuccededWithData:(NSMutableDictionary*)dictionary;
- (void)requestDidFailed;

@end


@interface SharedAppManager : NSObject

@property (nonatomic, assign) id<SharedAppManagerDelegate> delegate;
@property (nonatomic,retain) NSString *baseURL;
@property (nonatomic,retain) NSArray *appStatus;
@property (strong, nonatomic) Instagram *instagram;
@property BOOL addUser;
@property BOOL facebookToggle;
@property BOOL twitterToggle;
@property BOOL instagramToggle;
@property BOOL pushToggle;

+ (SharedAppManager*)sharedInstance;

- (void)initWithTitle:(NSString *)title message:(NSString *)message;
- (NSString *) returnDateAfterForamting:(NSDate *) value;
- (NSDate *) returnDateAfterForamtingString:(NSString *) value;

- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToWidth: (float) i_width;
- (UIImage *)imageWithImage: (UIImage*)sourceImage scaledToHeight: (float) i_height;

@end

