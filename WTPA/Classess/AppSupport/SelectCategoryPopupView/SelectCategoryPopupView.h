//
//  FrequencyOptionsVC.m
//  MyOmBody
//
//  Created by Abdullah on 11/2/11.
//  Copyright 2011 Organic Spread Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectCategoryPopupView;
@protocol SelectCategoryPopupViewDelegate <NSObject>

@optional

- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedValueDidSave:(NSString*)value withIndex:(NSInteger)selectedIndex;
- (void)selectCategoryPopupView:(SelectCategoryPopupView*)sPopupView selectedDateDidSave:(NSDate*)date;

@end

@interface SelectCategoryPopupView : UIView<UIPickerViewDelegate, UIPickerViewDataSource> {

	UIPickerView *pickerView;
	UIDatePicker *datePicker;
	
    NSString *identifier;
	NSMutableArray *array;
	NSString *selectedCategory;
	
	BOOL isDatePickerSelected;
    
    CGRect frameSize;
}

@property (assign) id<SelectCategoryPopupViewDelegate> delegate;
@property (nonatomic, assign) BOOL isCurrentlyVisible;
@property (nonatomic, assign) BOOL isOnlyTimeDisplayed;


+(id)selectCategoryPopupViewWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate;
+(id)selectSizePopupViewWithFrame:(CGRect)frame withArray:(NSArray*)theArray withidentifier:(NSString *)identfier withSelectedCategory:(NSString*)value  andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate;
- (id)initWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate;
- (id)initWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withIdentifier:(NSString *) ident andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate;
- (id)initWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag withOnlyTimeMode:(BOOL)isOnlyTimeMode andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate;

- (void)show;
- (void)hide;

@end
