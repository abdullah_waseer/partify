//
//  FrequencyOptionsVC.m
//  MyOmBody
//
//  Created by Abdullah on 11/2/11.
//  Copyright 2011 Organic Spread Media, LLC. All rights reserved.
//

#import "SelectCategoryPopupView.h"
#import "AppUtils.h"


@interface SelectCategoryPopupView ()

- (void)setCurrentPickerValue;
- (void)setCurrentDate;
- (void)setUpNavigationBar;
- (void)initializeUIcomponents;
- (void)cancelButtonAction;
- (void)selectButtonAction;
- (void)datePickerValueChanged:(UIDatePicker*)sender;

@property BOOL isDatePickerSelected;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, retain) NSDate *selectedDate;

@end




@implementation SelectCategoryPopupView
@synthesize isDatePickerSelected;
@synthesize delegate;
@synthesize selectedDate;
@synthesize isCurrentlyVisible;
@synthesize isOnlyTimeDisplayed;
@synthesize selectedIndex;


+(id)selectCategoryPopupViewWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate
{
    SelectCategoryPopupView *selectCategoryView = [[SelectCategoryPopupView alloc]initWithFrame:frame withArray:theArray withSelectedCategory:value withPickerSelected:flag andDelegate:aDelegate];
    
    return [selectCategoryView autorelease];
}

+(id)selectSizePopupViewWithFrame:(CGRect)frame withArray:(NSMutableArray*)theArray withidentifier:(NSString *)identfier withSelectedCategory:(NSString*)value  andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate
{
    
    SelectCategoryPopupView *selectCategoryView = [[SelectCategoryPopupView alloc]initWithFrame:frame withArray:theArray withSelectedCategory:value withIdentifier:identfier andDelegate:aDelegate];
    return [selectCategoryView autorelease];
}
- (id)initWithFrame:(CGRect)frame withArray:(NSMutableArray*)theArray withSelectedCategory:(NSString*)value withIdentifier:(NSString *) ident andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate
{
    frameSize=frame;
    self = [super initWithFrame:frame];
    if (self) {
		
		[self setUpNavigationBar];
        
        self.delegate = aDelegate;
        identifier=ident;
//        NSMutableArray *tempArray=[[NSMutableArray alloc]init ];
//        for(int i=0;i<theArray.count;i++)
//        {
//            [tempArray addObject:[theArray objectAtIndex:i]objectForKey:[NSString stringWithFormat:@"id%@",identifier] ]];
//        }
		
		array = [[NSMutableArray alloc]initWithArray:theArray];
		
        if(value)
            selectedCategory = [[NSString alloc]initWithString:value];
        else if(!value)
            selectedCategory = [[NSString alloc]initWithString:@"-1"];
        
		[self initializeUIcomponents];
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate {
    
    
    frameSize=frame;
    self = [super initWithFrame:frame];
    if (self) {
		
		[self setUpNavigationBar];
        
        self.delegate = aDelegate;
		
		array = [[NSMutableArray alloc]initWithArray:theArray];
		
        
        if(value)
            selectedCategory = [[NSString alloc]initWithString:value];
        else if(!value)
            selectedCategory = [[NSString alloc]initWithString:@"-1"];
        else if(flag)
            selectedCategory = [[NSString alloc]initWithString:[AppUtils getStrindFromDate:[NSDate date]]];
        
        
        isDatePickerSelected = flag;
        
		[self initializeUIcomponents];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withArray:(NSArray*)theArray withSelectedCategory:(NSString*)value withPickerSelected:(BOOL)flag withOnlyTimeMode:(BOOL)isOnlyTimeMode andDelegate:(id<SelectCategoryPopupViewDelegate>)aDelegate
{
    frameSize=frame;
    self = [super initWithFrame:frame];
    if (self) {
		
		[self setUpNavigationBar];
        
        self.delegate = aDelegate;
		
		array = [[NSMutableArray alloc]initWithArray:theArray];
		
        
        if(value && !flag)
            selectedCategory = [NSString stringWithFormat:@"%@",value];
        else if(value)
            selectedCategory = [NSString stringWithFormat:@"%@",value];
        
        
        isDatePickerSelected = flag;
        self.isOnlyTimeDisplayed = isOnlyTimeMode;
        
		[self initializeUIcomponents];
    }
    return self;
}

//setting up navigation bar for the view
- (void)setUpNavigationBar
{
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStylePlain target:self action:@selector(selectButtonAction)];
    [rightButton setTitleTextAttributes:@{
                                         UITextAttributeFont: [UIFont fontWithName:@"Thonburi" size:15.0],
                                         UITextAttributeTextColor: [UIColor whiteColor]
                                         } forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction)];
    
    [leftButton setTitleTextAttributes:@{
                                          UITextAttributeFont: [UIFont fontWithName:@"Thonburi" size:15.0],
                                          UITextAttributeTextColor: [UIColor whiteColor]
                                          } forState:UIControlStateNormal];
    
    
    UILabel *navBarLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 42)];
	navBarLabel.text = @"Select a Value";
	navBarLabel.font = [UIFont fontWithName:@"Thonburi" size:18.0];
	navBarLabel.textColor = [UIColor whiteColor];
	navBarLabel.textAlignment = NSTextAlignmentCenter;
	navBarLabel.backgroundColor = [UIColor clearColor];
    
                                   
    UINavigationItem *navitem = [[UINavigationItem alloc] initWithTitle:@""];
    navitem.rightBarButtonItem = rightButton;
    navitem.leftBarButtonItem = leftButton;
    navitem.hidesBackButton = YES;
    navitem.titleView = navBarLabel;
    
    
    UINavigationBar *bottomDetailBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 42)];

    NSString *imagePath = [[NSBundle mainBundle]pathForResource:@"nav_bar" ofType:@"png"];
	UIImage *navBackImg = [[UIImage alloc]initWithContentsOfFile:imagePath];
	UIImageView *navBackImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [navBackImgView setImage:navBackImg];
    
    [bottomDetailBar setBackgroundImage:[UIImage imageNamed:@"nav_bar.png"] forBarMetrics:UIBarMetricsDefault];
    [bottomDetailBar setTintColor:[UIColor whiteColor]];
    
//    [bottomDetailBar insertSubview:navBackImgView atIndex:0];
    
    [navBackImgView release];
	[navBackImg release];
   
    bottomDetailBar.barStyle = UIBarStyleBlackOpaque;
    [self addSubview:bottomDetailBar];
    
    [bottomDetailBar pushNavigationItem:navitem animated:NO];
    
    [rightButton release];
    [leftButton release];
    [navitem release];
    [bottomDetailBar release];
    [navBarLabel release];
}

//setting up UI components for the view, which can be either DatePicker or UIPickerView.
- (void)initializeUIcomponents
{
    if(!isDatePickerSelected)
    {
        pickerView = [[UIPickerView alloc] init];
        pickerView.frame = CGRectMake(0, 42, 320 ,216);
        pickerView.backgroundColor=[UIColor whiteColor];
        pickerView.delegate = self;
        pickerView.dataSource = self;
        pickerView.showsSelectionIndicator = YES;
        [self addSubview:pickerView];
        [pickerView release];
        
        [self setCurrentPickerValue];
    }
    else 
    {
        datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 42, 320, 216)];
        datePicker.backgroundColor=[UIColor whiteColor];
        if(self.isOnlyTimeDisplayed)
            datePicker.datePickerMode = UIDatePickerModeTime;
        else
            datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        
        [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:datePicker];
        [datePicker release];
        
        [self setCurrentDate];
//        [datePicker setDate:[NSDate date]];
    }
}

- (void)setCurrentPickerValue
{
    
	NSInteger index = 0;
	BOOL found = NO;
	
	for(int i=0; i<[array count];i++)
	{
		if([selectedCategory isEqualToString:[array objectAtIndex:i]])
		{
			index = i;
			found = YES;
			break;
		}
	}
    
    self.selectedIndex = 0;
    
    /*
	if(!found)
	{
		[selectedCategory release];
		selectedCategory = [[NSString alloc]initWithString:[array objectAtIndex:0]];
	}
    */
}


- (void)setCurrentDate
{
    //withFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"]
	NSDate *date = [[SharedAppManager sharedInstance] returnDateAfterForamtingString:selectedCategory];//[AppUtils getDateFromString:selectedCategory withFormat:@"dd-MM-yyyy"];
    //[BWFUtils getDateFromString:selectedCategory withFormat:@"dd-MM-yyyy"];
	
	if (selectedCategory && selectedCategory.length > 0)
    {
		[datePicker setDate:date];
	}
	else 
    {
		[datePicker setDate:[NSDate date]];
	}
}


#pragma mark
#pragma mark Pickerview Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pv
{
    return 1;
}

/*
- (NSString *)pickerView:(UIPickerView *)aPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [array objectAtIndex:row];
}
*/

- (void)pickerView:(UIPickerView *)aPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component 
{
	//[selectedCategory release];
	//selectedCategory = [[NSString alloc]initWithString:[array objectAtIndex:row]];
    
    self.selectedIndex = row;
}


- (NSInteger)pickerView:(UIPickerView*)pv numberOfRowsInComponent:(NSInteger)component
{
    return [array count];
}


- (CGFloat)pickerView:(UIPickerView *)aPickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (UIView*)pickerView:(UIPickerView *)pView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIView *retval = (id)view;
    if (!retval) {
        retval= [[[UIView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, [pView rowSizeForComponent:component].width, [pView rowSizeForComponent:component].height)] autorelease];
        
        CGRect cellRectangle = CGRectMake(45, 0.0f, [pView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height);
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:cellRectangle];
        lblTitle.font = [UIFont fontWithName:@"Thonburi" size:17];
        lblTitle.text = [NSString stringWithFormat:@" %@",[[array objectAtIndex:row] capitalizedString]];
        lblTitle.textColor = [UIColor colorWithRed:71.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1];
        lblTitle.backgroundColor = [UIColor clearColor];
        [retval addSubview:lblTitle];
        [lblTitle release];
        
        NSString *imgPath = [[NSBundle mainBundle]pathForResource:@"Tick" ofType:@"png"];
        UIImage *img = [[UIImage alloc]initWithContentsOfFile:imgPath];
        NSString *imgPathUnSelected = [[NSBundle mainBundle]pathForResource:@"Tick2" ofType:@"png"];
        UIImage *imgUnSelected = [[UIImage alloc]initWithContentsOfFile:imgPathUnSelected];
        
        float yCord = ([pView rowSizeForComponent:component].height - img.size.height)/2;
        cellRectangle = CGRectMake(10 ,yCord ,img.size.width ,img.size.height);
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:cellRectangle];
        if([ [[array objectAtIndex:row] lowercaseString] isEqualToString:[selectedCategory lowercaseString]])
        {
            [imgView setImage:imgUnSelected];
        }
        else {
            [imgView setImage:imgUnSelected];
        }
        [retval addSubview:imgView];
        [imgView release];
        [img release];
        [imgUnSelected release];
    }
    
    UILabel *label = (UILabel *)[retval.subviews objectAtIndex:0];
    label.text = [NSString stringWithFormat:@" %@",[[array objectAtIndex:row] capitalizedString]];
    
    NSString *imgPath = [[NSBundle mainBundle]pathForResource:@"Tick" ofType:@"png"];
    UIImage *img = [[UIImage alloc]initWithContentsOfFile:imgPath];
    NSString *imgPathUnSelected = [[NSBundle mainBundle]pathForResource:@"Tick2" ofType:@"png"];
    UIImage *imgUnSelected = [[UIImage alloc]initWithContentsOfFile:imgPathUnSelected];
    
    UIImageView *imgView = (UIImageView *)[retval.subviews objectAtIndex:1];
    if([ [[array objectAtIndex:row] lowercaseString] isEqualToString:[selectedCategory lowercaseString] ])
    {
        [imgView setImage:img];
    }
    else {
        [imgView setImage:imgUnSelected];
    }
    [img release];
    [imgUnSelected release];
    
    return retval;
}

//Delegate is fired upon clicking either Cancel or Select Button
-(void) cancelButtonAction
{
	if(!isDatePickerSelected)
	{
		if ([self.delegate respondsToSelector:@selector(selectCategoryPopupView:selectedValueDidSave:withIndex:)])
			[self.delegate selectCategoryPopupView:self selectedValueDidSave:nil withIndex:0];
	}
	else
	{
		if ([self.delegate respondsToSelector:@selector(selectCategoryPopupView:selectedDateDidSave:)])
			[self.delegate selectCategoryPopupView:self selectedDateDidSave:nil];
	}
}


- (void)selectButtonAction
{

	if(!isDatePickerSelected)
	{
        [selectedCategory release];
        selectedCategory = [[NSString alloc]initWithString:[array objectAtIndex:self.selectedIndex]];
        
		if ([self.delegate respondsToSelector:@selector(selectCategoryPopupView:selectedValueDidSave:withIndex:)])
			[self.delegate selectCategoryPopupView:self selectedValueDidSave:selectedCategory withIndex:self.selectedIndex];
	}
	else
	{
        if(!self.selectedDate)
            self.selectedDate = [NSDate date];
        
		if ([self.delegate respondsToSelector:@selector(selectCategoryPopupView:selectedDateDidSave:)])
			[self.delegate selectCategoryPopupView:self selectedDateDidSave:self.selectedDate];
	}
    
}


- (void)datePickerValueChanged:(UIDatePicker*)sender
{
    
	NSDate *newDate = [sender date];
    self.selectedDate = newDate;
	
}

//Show and Hide methods which will show and hide the view with animating effect (bringing up / sending down)
- (void)show
{
    self.isCurrentlyVisible = YES;
    
    
    [self setFrame:CGRectMake(0, 480+(IS_IPHONE_5 ? screenDifference:0.0), 320, 202)];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelay:0.0];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self cache:YES];
	
    if(isDatePickerSelected)
    
        [self setFrame:frameSize];
         //CGRectMake(0, 416-258, 320, 216+42)];
    else
        [self setFrame:frameSize];
         //CGRectMake(0, 416-204, 320, 204)];
	
	[UIView commitAnimations];
    
    
}

- (void)hide
{
    self.isCurrentlyVisible = NO;
    
    ///frameSize
    if(isDatePickerSelected)
        [self setFrame:self.frame];
         //CGRectMake(0, 416-258, 320, 216+42)];
    else
        [self setFrame:self.frame];
         //CGRectMake(0, 416-204, 320, 204)];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelay:0.0];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self cache:YES];
	
	[self setFrame:CGRectMake(0, 480+(IS_IPHONE_5 ? screenDifference:0.0), 320, 202)];
	
	[UIView commitAnimations];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    
	[array release];
	array = nil;
    
	[selectedCategory release];
	selectedCategory = nil;
    
    [super dealloc];
}


@end
