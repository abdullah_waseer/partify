//
//  CameraOverlayView.h
//  cloggs
//
//  Created by Sovoia on 03/12/2013.
//  Copyright (c) 2013 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CameraOverlayView;
@protocol CameraOverlayViewDelegate  <NSObject>

- (void)didTapCaptureButtonWithCameraOverlayView:(CameraOverlayView *)view;
- (void)didTapInstaGramButtonWithCameraOverlayView:(CameraOverlayView *)view;
- (void)didTapPhotoGalleryWithCameraOverlayView:(CameraOverlayView *)view;
- (void)didTapCancelyWithCameraOverlayView:(CameraOverlayView *)view;
- (void)didTapFlashOnWithCameraOverlayView:(CameraOverlayView *)view andTypeOfFlash:(NSMutableDictionary *) flashmodes;
- (void)didTapFrontBackWithCameraOverlayView:(CameraOverlayView *)view;

@end


@interface CameraOverlayView : UIView

@property (nonatomic, assign) id <CameraOverlayViewDelegate> delegate;

@end
