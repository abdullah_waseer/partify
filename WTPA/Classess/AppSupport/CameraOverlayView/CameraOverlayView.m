//
//  CameraOverlayView.m
//  cloggs
//
//  Created by Sovoia on 03/12/2013.
//  Copyright (c) 2013 Shahid. All rights reserved.
//

#import "CameraOverlayView.h"

@interface CameraOverlayView ()

@property (nonatomic, retain) IBOutlet UIView *UperView;
@property (nonatomic, retain) IBOutlet UIView *LowerView;


@end



@implementation CameraOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSArray *screen =[[NSBundle mainBundle]loadNibNamed:@"CameraOverlayView" owner:self options:nil];
        [self addSubview:[screen objectAtIndex:0]];
        
        
        [self resetFrames];
    }
    return self;
}

- (void)resetFrames
{
    if(IS_IPHONE_5)
    {
        self.frame=CGRectMake(0, 0, 320, 568);
    }
    else
    {
        self.frame=CGRectMake(0, 0, 320, 480);
    }
    self.UperView.frame=CGRectMake(0, 0, 320,41 );
    self.LowerView.frame=CGRectMake(0, self.frame.size.height-75, 320, 75);
}

- (IBAction)captureButtonPressed:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didTapCaptureButtonWithCameraOverlayView:)])
        [self.delegate didTapCaptureButtonWithCameraOverlayView:self];
}

- (IBAction)cancelButtonPressed:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didTapCancelyWithCameraOverlayView:)])
        [self.delegate didTapCancelyWithCameraOverlayView:self];
}

- (IBAction)fronBackCamPressed:(id)sender
{
    UIButton *but=(UIButton *) sender;
    but.selected=!but.selected;
    if([self.delegate respondsToSelector:@selector(didTapFrontBackWithCameraOverlayView:)])
        [self.delegate didTapFrontBackWithCameraOverlayView:self];
}
- (IBAction)flashOnOFPressed:(id)sender
{
    UIButton *but=(UIButton *) sender;
    switch (but.tag) {
        case -1:
        {
            but.tag=0;
            [but setImage:[UIImage imageNamed:@"cameraauto.png"] forState:UIControlStateNormal];
            break;
        }
        case 0:
        {
            but.tag=1;
            [but setImage:[UIImage imageNamed:@"cameraon.png"] forState:UIControlStateNormal];
            break;
        }

        case 1:
        {
            but.tag=-1;
            [but setImage:[UIImage imageNamed:@"cameraoff.png"] forState:UIControlStateNormal];
            break;
        }

        default:
            break;
    }
    
    NSMutableDictionary *dictionary=[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%i",but.tag],@"flashmode", nil];
    if([self.delegate respondsToSelector:@selector(didTapFlashOnWithCameraOverlayView:andTypeOfFlash:)])
        [self.delegate didTapFlashOnWithCameraOverlayView:self andTypeOfFlash:dictionary];
}


- (IBAction)instagramButtonPressed:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didTapInstaGramButtonWithCameraOverlayView:)])
        [self.delegate didTapInstaGramButtonWithCameraOverlayView:self];
}

- (IBAction)GalleryButtonPressed:(id)sender
{
    if([self.delegate respondsToSelector:@selector(didTapPhotoGalleryWithCameraOverlayView:)])
        [self.delegate didTapPhotoGalleryWithCameraOverlayView:self];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
