//
//  CustomTabBarItem.m
//  Modell
//
//  Created by Abdullah Waseer on 12/30/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import "CustomTabBarItem.h"

@implementation CustomTabBarItem

@synthesize customHighlightedImage;  
@synthesize customStdImage;  


- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        
    }
    return self;
}


- (void) dealloc  
{  
    [customHighlightedImage release]; customHighlightedImage=nil;  
    [customStdImage release]; customStdImage=nil; 
    
    [super dealloc];  
}  

- (UIImage *) selectedImage  
{  
    return self.customHighlightedImage;  
    //return nil;  
}  

- (UIImage *) unselectedImage  
{  
    return self.customStdImage;  
    
    //return nil;  
}

@end
