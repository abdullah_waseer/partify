//
//  CustomTabBarItem.h
//  Modell
//
//  Created by Abdullah Waseer on 12/30/12.
//  Copyright (c) 2012 Organic Spread Media, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarItem : UITabBarItem

@property (nonatomic, retain) UIImage *customHighlightedImage;  
@property (nonatomic, retain) UIImage *customStdImage; 

@end
