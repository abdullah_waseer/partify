    //
//  ProgramMapViewController.m
//  College
//
//  Created by Smonte Technologies on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UUITabBarController.h"
#import "CustomTabBarItem.h"
#import "ALLEventsVC.h"
#import "PhotoUploadVC.h"
#import "SearchVC.h"
#import "CreateEventVC.h"
#import "UserProfileVC.h"
@interface UUITabBarController ()

- (void)setUpTabBarItemforViewController:(UIViewController*)viewController withTitle:(NSString*)title selectedImageName:(NSString*)selectedImage unSelectedImageName:(NSString*)unSelectedImage tag:(int)tag;
- (void)setTabbarBackgroundWithImage:(NSString*)imageName;
- (void)setUpNavBarFor_IOS5Above:(UINavigationController*)nav withImageName:(NSString*)imageName;

@end


@implementation UINavigationBar (UINavigationBarCategory)

- (void)drawRect:(CGRect)rect {
    CGRect newRect=CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, self.frame.size.height);
    UIImage *img = [UIImage imageNamed:NAV_BACKGRND_IMG_NAME];
    [img drawInRect:newRect];
}
@end

@implementation UITabBar (UITabBarCategory)
- (void)drawRect:(CGRect)rect {
    //UIImage *img = [UIImage imageNamed:TAB_BACKGRND_IMG_NAME];
    //[img drawInRect:rect];
}
@end


@implementation UUITabBarController

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

-(id) init {
	
	if(self = [super init])
	{
		[self setUpTabBarController];
	}
	return self;
}

- (void)setUpTabBarController
{	
	NSMutableArray *localViewControllersArray = [[NSMutableArray alloc] init];
   
	ALLEventsVC *homeVC  = [[ALLEventsVC alloc] initWithNibName:@"ALLEventsVC" bundle:nil];
	[homeVC.tabBarItem setImage:[UIImage imageNamed:@"house.png"]];
	UINavigationController *tempNavigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    homeVC.title = @"";
    [self setUpTabBarItemforViewController:homeVC withTitle:@"" selectedImageName:@"findparties_selected" unSelectedImageName:@"findparties_unselected" tag:0];
    [self setUpNavBarFor_IOS5Above:tempNavigationController withImageName:NAV_BACKGRND_IMG_NAME];
	[localViewControllersArray addObject:tempNavigationController];
	[tempNavigationController release];
	[homeVC release];
    
    SearchVC *searchVC  = [[SearchVC alloc] initWithNibName:@"SearchVC" bundle:nil];
	[searchVC.tabBarItem setImage:[UIImage imageNamed:@""]];
	searchVC.title = @"";
	tempNavigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
	
    [self setUpTabBarItemforViewController:searchVC withTitle:@"" selectedImageName:@"calender_selected" unSelectedImageName:@"calender_unselected" tag:1];
    [self setUpNavBarFor_IOS5Above:tempNavigationController withImageName:NAV_BACKGRND_IMG_NAME];
	[localViewControllersArray addObject:tempNavigationController];
	[tempNavigationController release];
	[searchVC release];

    PhotoUploadVC *photoUploadVC  = [[PhotoUploadVC alloc] initWithNibName:@"PhotoUploadVC" bundle:nil];
	[photoUploadVC.tabBarItem setImage:[UIImage imageNamed:@""]];
	photoUploadVC.title = @"";
	tempNavigationController = [[UINavigationController alloc] initWithRootViewController:photoUploadVC];
    [self setUpTabBarItemforViewController:photoUploadVC withTitle:@"" selectedImageName:@"camera_selected" unSelectedImageName:@"camera_unselected" tag:2];
    [self setUpNavBarFor_IOS5Above:tempNavigationController withImageName:NAV_BACKGRND_IMG_NAME];
	[localViewControllersArray addObject:tempNavigationController];
	[tempNavigationController release];
	[photoUploadVC release];
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA]);
//    NSDictionary *data=[ NSDictionary dictionaryWithDictionary:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_DATA] objectAtIndex:0]];
    UserProfileVC *profileVC  = [[UserProfileVC alloc] initWithNibName:@"UserProfileVC" bundle:nil with:nil status:NO];
	[profileVC.tabBarItem setImage:[UIImage imageNamed:@""]];
	profileVC.title = @"";
	tempNavigationController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    [self setUpTabBarItemforViewController:profileVC withTitle:@"" selectedImageName:@"account_selected" unSelectedImageName:@"account_unselected" tag:3];
    [self setUpNavBarFor_IOS5Above:tempNavigationController withImageName:NAV_BACKGRND_IMG_NAME];
	[localViewControllersArray addObject:tempNavigationController];
	[tempNavigationController release];
	[profileVC release];
    
    CreateEventVC *createEventVC  = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
	[createEventVC.tabBarItem setImage:[UIImage imageNamed:@""]];
	createEventVC.title = @"";
	tempNavigationController = [[UINavigationController alloc] initWithRootViewController:createEventVC];
    [self setUpTabBarItemforViewController:createEventVC withTitle:@"" selectedImageName:@"balloons_selected" unSelectedImageName:@"balloons_unselected" tag:4];
    [self setUpNavBarFor_IOS5Above:tempNavigationController withImageName:NAV_BACKGRND_IMG_NAME];
	[localViewControllersArray addObject:tempNavigationController];
	[tempNavigationController release];
	[createEventVC release];
    
	self.viewControllers = localViewControllersArray;
	[localViewControllersArray release];
    
    [self setTabbarBackgroundWithImage:TAB_BACKGRND_IMG_NAME];
}




- (void)setUpNavBarFor_IOS5Above:(UINavigationController*)nav withImageName:(NSString*)imageName
{
    if(SYSTEM_VERSION_GREATER_THAN(@"6.3"))
    {
        [[UINavigationBar appearance] setBarTintColor:[UIColor clearColor]];
        nav.navigationBar.barStyle = UIBarStyleDefault;
        nav.navigationBar.translucent=YES;
    }
    else
    {
        [[UINavigationBar appearance] setTintColor:[UIColor clearColor]];
        nav.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    }

    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:imageName] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor clearColor]];
}

- (void)setUpTabBarItemforViewController:(UIViewController*)viewController withTitle:(NSString*)title selectedImageName:(NSString*)selectedImage unSelectedImageName:(NSString*)unSelectedImage tag:(int)tag
{
    if(!SYSTEM_VERSION_LESS_THAN_5_0) {
        
        UITabBarItem *customTabBarItem = [[UITabBarItem alloc] initWithTitle:title image:nil tag:tag];
        
        [customTabBarItem setFinishedSelectedImage:[UIImage imageNamed:selectedImage] withFinishedUnselectedImage:[UIImage imageNamed:unSelectedImage]];
        [[UIBarButtonItem appearance] setTintColor:[UIColor clearColor]];
//        customTabBarItem.title = title;
        
        
        
//        if ([customTabBarItem respondsToSelector:@selector(setTitleTextAttributes:forState:)]) {
//            
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ UITextAttributeTextColor : [UIColor whiteColor], UITextAttributeFont : [UIFont fontWithName:@"Thonburi" size:12.0f] } forState:UIControlStateNormal];
//            
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ UITextAttributeTextColor : [UIColor blackColor], UITextAttributeFont : [UIFont fontWithName:@"Thonburi" size:12.0f] } forState:UIControlStateHighlighted];
//            
//         /*
//            [customTabBarItem setTitleTextAttributes:
//             [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIColor lightGrayColor], nil] forKeys:[NSArray arrayWithObjects:UITextAttributeTextColor, nil]]
//                                            forState:UIControlStateNormal];
//            
//            [customTabBarItem setTitleTextAttributes:
//             [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIColor blackColor], nil] forKeys:[NSArray arrayWithObjects:UITextAttributeTextColor, nil]]
//                                            forState:UIControlStateSelected];
//            */
//            
//        }
        
        viewController.tabBarItem =customTabBarItem;
        [customTabBarItem release];
        
    }
    else {
        
        CustomTabBarItem *customTabBarItem = [[CustomTabBarItem alloc] init];
        customTabBarItem.customHighlightedImage = [UIImage imageNamed:selectedImage];
        customTabBarItem.customStdImage = [UIImage imageNamed:unSelectedImage];
        [viewController setTabBarItem:customTabBarItem];
        [customTabBarItem release];
        
    }
}

- (void)setTabbarBackgroundWithImage:(NSString*)imageName
{
     [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:imageName]];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    
    if(SYSTEM_VERSION_GREATER_THAN(@"6.3"))
    {
            [[UITabBar appearance] setBarTintColor:[UIColor clearColor]];
    }
    else
    {
        [[UITabBar appearance] setTintColor:[UIColor clearColor]];
    }
}


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
    [super dealloc];
}


@end
