//
//  TaggedView.m
//  WTPA
//
//  Created by Ishaq Shafiq on 31/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import "TaggedView.h"

@interface TaggedView ()

@property (nonatomic, retain) IBOutlet UITableView *taggedTable;
@property (nonatomic, retain) IBOutlet NSMutableArray *taggedArray;

@end

@implementation TaggedView

- (id)initWithFrame:(CGRect)frame withArray:(NSArray *) array
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *screen =[[NSBundle mainBundle]loadNibNamed:@"TaggedView" owner:self options:nil];
        [self addSubview:[screen objectAtIndex:0]];
        
        self.layer.borderColor=CUSTOM_APP_COLOR.CGColor;
        self.layer.borderWidth=3;
        self.taggedArray=[NSMutableArray arrayWithArray:array];
        
//        [self resetFrames];
    }
    return self;
}

- (void)resetFrames
{
    if(IS_IPHONE_5)
    {
        self.frame=CGRectMake(0, 0, 250, 325);
        self.taggedTable.frame=CGRectMake(0, 0, 250, 325);
    }
    else
    {
        self.frame=CGRectMake(0, 0, 250, 325);
        self.taggedTable.frame=CGRectMake(0, 0, 250, 325);
    }
    [self.taggedTable reloadData];
}

- (IBAction)cancelButtonPressed:(id)sender
{
    if([self.delegate respondsToSelector:@selector(TagedViewcrossTapped:)])
        [self.delegate TagedViewcrossTapped:self];
}

#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.taggedArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CountryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.textLabel.text=[[_taggedArray objectAtIndex:indexPath.row] objectForKey:@"username"];
    cell.textLabel.font=[UIFont fontWithName:@"Thonburi" size:15 ];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UserProfileVC * userProfileVC = [[UserProfileVC alloc]initWithNibName:@"UserProfileVC" bundle:nil with:[[self.followingData objectAtIndex:indexPath.row] objectForKey:@"id"] status:YES];
//    [self.navigationController pushViewController:userProfileVC animated:YES];
//    [userProfileVC release];
    
    if([self.delegate respondsToSelector:@selector(TagedView:withTaggedDictionary:)])
        [self.delegate TagedView:self withTaggedDictionary:[_taggedArray objectAtIndex:indexPath.row]];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
