//
//  TaggedView.h
//  WTPA
//
//  Created by Ishaq Shafiq on 31/01/2014.
//  Copyright (c) 2014 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TaggedView;
@protocol TaggedViewDelegate  <NSObject>

@optional
- (void)TagedViewcrossTapped:(TaggedView *)view ;
- (void)TagedView:(TaggedView *)view withTaggedDictionary:(NSMutableDictionary *) tagged ;

@end

@interface TaggedView : UIView
@property (nonatomic, assign) id <TaggedViewDelegate> delegate;
- (id)initWithFrame:(CGRect)frame withArray:(NSArray *) array;
@end
