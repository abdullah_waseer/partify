//
//  AppDelegate.m
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import "AppDelegate.h"
#import "UUITabBarController.h"
#import "FaceBookManager.h"
#import "SharedAppManager.h"
#import "UITabBarController+HideTabBar.h"
#import "ALLEventsVC.h"
#import "Bugsnag.h"

@interface AppDelegate ()
{
}
- (void)setUpTabBarController;

@end
@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:USER_AUTHENTICATE];
    if(SYSTEM_VERSION_GREATER_THAN (@"6.3"))
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    }
    
    [Bugsnag startBugsnagWithApiKey:@"9a6a6bb782a2ddb2247eea0d667e3581"];
    [Bugsnag addAttribute:@"AppName" withValue:@"Partify" toTabWithName:@"user"];

    // Override point for customization after application launch.
    [self setUpTabBarController];

    [self.window makeKeyAndVisible];
    return YES;
}

+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (void) showTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4];
    for(UIView *view in _tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 499, view.frame.size.width, view.frame.size.height)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
            }
        }
        else
        {
            if ([[UIScreen mainScreen] bounds].size.height == 568)
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 519)];
            }
            else if ([[UIScreen mainScreen] bounds].size.height == 480)
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
            }
        }
    }
    [UIView commitAnimations];
}

- (void) hideTabBar:(UITabBarController *) tabbarcontroller
{
    [_tabBarController setTabBarHidden:YES animated:NO];
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.4];
//    for(UIView *view in _tabBarController.view.subviews)
//    {
//        if([view isKindOfClass:[UITabBar class]])
//        {
//            if ([[UIScreen mainScreen] bounds].size.height == 568)
//            {
//                [view setFrame:CGRectMake(view.frame.origin.x, 568, view.frame.size.width, view.frame.size.height)];
//            }
//            else if ([[UIScreen mainScreen] bounds].size.height == 480)
//            {
//                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
//            }
//        }
//        else
//        {
//            if ([[UIScreen mainScreen] bounds].size.height == 568)
//            {
//                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 568)];
//            }
//            else if ([[UIScreen mainScreen] bounds].size.height == 480)
//            {
//                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
//            }
//        }
//    }
//    
//    [UIView commitAnimations];
}

- (void)setUpTabBarController
{
    self.tabBarController = [[UUITabBarController alloc]init];
    _tabBarController.delegate = self;
    
//    [self.window addSubview:[_tabBarController view]];
    self.window.rootViewController = _tabBarController;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
//    if([tabBarController selectedIndex]==0)
//    {
//        UINavigationController *navController=[[tabBarController viewControllers] objectAtIndex:0];
////        [navController popToRootViewControllerAnimated:NO];
//        [self performSelector:@selector(setUpAllEventsView:) withObject:navController afterDelay:0.6];
//    }
}

-(void)  setUpAllEventsView:(UINavigationController *)navController
{
    ALLEventsVC *allEventsVC=[[ALLEventsVC alloc] initWithNibName:@"ALLEventsVC" bundle:nil];
    [navController pushViewController:allEventsVC animated:NO];
    [allEventsVC release];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    NSLog(@"url: %@",url.absoluteString);
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    return [[[FaceBookManager sharedManager] faceBookInstance] handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"url: %@",url.absoluteString);
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    if ([AppUtils ifString:@"cloggsiphone" isFoundInString:[url.absoluteString lowercaseString]])
    {
        return YES;
    }
    else if ([AppUtils ifString:@"ig" isFoundInString:[url.absoluteString lowercaseString]])
    {
        return [[SharedAppManager sharedInstance].instagram handleOpenURL:url];
    }
    return [[[FaceBookManager sharedManager] faceBookInstance] handleOpenURL:url];
}

@end
