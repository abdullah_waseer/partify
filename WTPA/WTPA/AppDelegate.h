//
//  AppDelegate.h
//  WTPA
//
//  Created by Ishaq Shafiq on 28/10/2013.
//  Copyright (c) 2013 Sovoia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UUITabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) UUITabBarController *tabBarController;

+ (AppDelegate *)sharedDelegate;

- (void) hideTabBar:(UITabBarController *) tabbarcontroller;
- (void) showTabBar:(UITabBarController *) tabbarcontroller;



@end
